ALTER TABLE layers ADD COLUMN gfi_theme_params text;
UPDATE layers SET gfi_theme_params=subquery.gfi_theme_params FROM (SELECT layer_id, gfi_theme_params FROM layers_type_sensorthings) AS subquery WHERE layers.layer_id=subquery.layer_id;
ALTER TABLE layers_type_sensorthings DROP COLUMN gfi_theme_params CASCADE;

ALTER TABLE public.layers_type_ows
    ADD COLUMN notsupportedfor3d boolean
    DEFAULT false;

ALTER TABLE public.layers_type_ows
    RENAME gfi_complex TO gfi_complex_alt;

ALTER TABLE public.layers_type_ows
    ADD COLUMN gfi_complex boolean 
    DEFAULT false;

UPDATE public.layers_type_ows SET gfi_complex = gfi_complex_alt::boolean;

ALTER TABLE public.layers_type_ows
    DROP COLUMN gfi_complex_alt CASCADE;

DROP VIEW public.service_layers;
CREATE OR REPLACE VIEW public.service_layers
    AS
     SELECT layers.layer_id,
    layers.service_id,
    layers.layer_name,
    layers.title,
    layers.title_alt,
    layers.layerattribution,
    layers.additional_categories,
    layers.tilesize,
    layers.legend_url_intranet,
    layers.legend_url_internet,
    layers.transparent,
    layers.transparency,
    layers.output_format,
    layers.scale_min,
    layers.scale_max,
    layers.gfi_theme,
    layers.gfi_theme_params,
    layers.gfi_config,
    layers.gfi_asnewwindow,
    layers.gfi_windowspecs,
	  layers.service_url_is_visible,
    ows.gutter,
    ows.singletile,
    ows.namespace,
    ows.datetime_column,
    ows.legend_type,
    ows.legend_path,
    ows.featurecount,
    ows.gfi_format,
    ows.gfi_complex,
    ows.hittolerance,
    ows.notsupportedfor3d,
    type3d.request_vertex_normals,
    type3d.maximum_screen_space_error,
    oblique.resolution,
    oblique.minzoom,
    oblique.hidelevels,
    oblique.projection,
    oblique.terrainurl,
    sensorthings.epsg,
    sensorthings.rootel,
    sensorthings.filter,
    sensorthings.expand,
    sensorthings.related_wms_layers,
    sensorthings.style_id,
    sensorthings.cluster_distance,
    sensorthings.load_things_only_in_current_extent,
    sensorthings.mouse_hover_field,
    sensorthings.sta_test_url,
    vectortiles.extent,
    vectortiles.origin,
    vectortiles.resolutions,
    vectortiles.visibility,
    vectortiles.vtstyles,
    (EXISTS ( SELECT layer_delete_requests_1.comment
           FROM layer_delete_requests layer_delete_requests_1
          WHERE (layer_delete_requests_1.layer_id = layers.layer_id))) AS layer_delete_request,
    layer_delete_requests.comment AS layer_delete_request_comment
   FROM ((((((layers
     LEFT JOIN layers_type_ows ows ON ((layers.layer_id = ows.layer_id)))
     LEFT JOIN layers_type_3d type3d ON ((layers.layer_id = type3d.layer_id)))
     LEFT JOIN layers_type_oblique oblique ON ((layers.layer_id = oblique.layer_id)))
     LEFT JOIN layers_type_sensorthings sensorthings ON ((layers.layer_id = sensorthings.layer_id)))
     LEFT JOIN layers_type_vectortiles vectortiles ON ((layers.layer_id = vectortiles.layer_id)))
     LEFT JOIN layer_delete_requests layer_delete_requests ON ((layers.layer_id = layer_delete_requests.layer_id)))
  WHERE (layers.deleted = false);

ALTER TABLE public.service_layers OWNER TO dienstemanager;
