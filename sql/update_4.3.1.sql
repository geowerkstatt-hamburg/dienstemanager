-- Neue Spalten der SensorThingsAPI hinzufügen

ALTER TABLE layers_type_sensorthings ADD COLUMN mqttEntity text[];
ALTER TABLE layers_type_sensorthings ADD COLUMN filter text;
ALTER TABLE layers_type_sensorthings ADD COLUMN expand text;

-- View layer_list anpassen

DROP VIEW public.layer_list;

CREATE OR REPLACE VIEW public.layer_list AS
 SELECT layers.layer_id,
    layers.layer_name,
    layers.title,
    layers.service_id,
    ( SELECT services.title
           FROM public.services
          WHERE (services.service_id = layers.service_id)) AS service_title,
    ( SELECT services.service_type
           FROM public.services
          WHERE (services.service_id = layers.service_id)) AS service_type
   FROM public.layers
   WHERE layers.deleted = false;

ALTER TABLE public.layer_list OWNER TO dienstemanager;

-- View service_layers anpassen

DROP VIEW public.service_layers;

CREATE OR REPLACE VIEW public.service_layers AS
 SELECT layers.layer_id,
    layers.service_id,
    layers.layer_name,
    layers.title,
    layers.title_alt,
    layers.layerattribution,
    layers.additional_categories,
    layers.tilesize,
    layers.legend_url_intranet,
    layers.legend_url_internet,
    layers.transparent,
    layers.transparency,
    layers.output_format,
    layers.scale_min,
    layers.scale_max,
    layers.gfi_theme,
    layers.gfi_config,
    ows.gutter,
    ows.singletile,
    ows.namespace,
    ows.legend_type,
    ows.legend_path,
    ows.featurecount,
    ows.gfi_format,
    ows.gfi_complex,
    ows.hittolerance,
    type3d.request_vertex_normals,
    type3d.maximum_screen_space_error,
    oblique.resolution,
    oblique.minzoom,
    oblique.hidelevels,
    oblique.projection,
    oblique.terrainurl,
    sensorthings.epsg,
    sensorthings.gfi_theme_params,
    sensorthings.mqttentity,
    sensorthings.filter,
    sensorthings.expand
   FROM public.layers
     LEFT JOIN layers_type_ows ows ON layers.layer_id = ows.layer_id
     LEFT JOIN layers_type_3d type3d ON layers.layer_id = type3d.layer_id
     LEFT JOIN layers_type_oblique oblique ON layers.layer_id = oblique.layer_id
     LEFT JOIN layers_type_sensorthings sensorthings ON layers.layer_id = sensorthings.layer_id
  WHERE layers.deleted = false;

ALTER TABLE public.service_layers OWNER TO dienstemanager;

ALTER TABLE layers_type_sensorthings DROP COLUMN url_params;