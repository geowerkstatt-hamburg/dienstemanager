DROP TABLE IF EXISTS public.layer_excludes;

CREATE TABLE public.service_jira_tickets
(
    id integer NOT NULL,
    service_id integer NOT NULL,
    key text COLLATE pg_catalog."default" NOT NULL,
    creator text COLLATE pg_catalog."default" NOT NULL,
    summary text COLLATE pg_catalog."default",
    assignee text COLLATE pg_catalog."default",
    CONSTRAINT service_jira_constraint PRIMARY KEY (id, service_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.service_jira_tickets
    OWNER to dienstemanager;


CREATE TABLE public.config_scheduled_tasks
(
    name text COLLATE pg_catalog."default" NOT NULL,
    descr text COLLATE pg_catalog."default",
    last_run text COLLATE pg_catalog."default",
    message text COLLATE pg_catalog."default",
    status text COLLATE pg_catalog."default",
    schedule text COLLATE pg_catalog."default",
    enabled boolean,
    CONSTRAINT scheduled_tasks_status_pk PRIMARY KEY (name)
    )
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

DROP TABLE IF EXISTS public.portals;

DROP TABLE IF EXISTS public.portal_links;

CREATE TABLE public.portal_links
(
    layer_id integer NOT NULL,
    title text COLLATE pg_catalog."default" NOT NULL,
    url text COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT portal_links_pkey PRIMARY KEY (title, url, layer_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.portal_links
    OWNER to dienstemanager;

ALTER TABLE public.config_scheduled_tasks
    OWNER to dienstemanager;

INSERT INTO public.config_scheduled_tasks(name, descr, last_run, message, status, schedule, enabled) VALUES ('delete_tmp_files', 'Löscht alle Dateien im /tmp Ordner', '', '', 'Noch nicht gelaufen','0 22 * * *', false);
INSERT INTO public.config_scheduled_tasks(name, descr, last_run, message, status, schedule, enabled) VALUES ('check_all_services', 'Startet für jeden produktiven WMS und WFS die Prüfroutine', '', '', 'Noch nicht gelaufen','0 22 * * *', false);
INSERT INTO public.config_scheduled_tasks(name, descr, last_run, message, status, schedule, enabled) VALUES ('update_datasets', 'Aktualisiert sämtliche Datensatzmetadaten', '', '', 'Noch nicht gelaufen','0 22 * * *', false);
INSERT INTO public.config_scheduled_tasks(name, descr, last_run, message, status, schedule, enabled) VALUES ('update_service_metadata', 'Aktualisiert sämtliche Dienstmetadaten', '', '', 'Noch nicht gelaufen','0 22 * * *', false);
INSERT INTO public.config_scheduled_tasks(name, descr, last_run, message, status, schedule, enabled) VALUES ('update_deegree_capabilities', 'Aktualisiert die Daten-Dienste-Kopplung und Metadateneinträge in den deegree Capabilities', '', '', 'Noch nicht gelaufen','0 22 * * *', false);
INSERT INTO public.config_scheduled_tasks(name, descr, last_run, message, status, schedule, enabled) VALUES ('generate_layer_json', 'Generiert sämtliche Layer JSON Objekte in der Datenbank neu', '', '', 'Noch nicht gelaufen','0 22 * * *', false);
INSERT INTO public.config_scheduled_tasks(name, descr, last_run, message, status, schedule, enabled) VALUES ('portal_linker', 'Prüft welche Layer in welchen Masterportalen eingebunden sind', '', '', 'Noch nicht gelaufen','0 22 * * *', false);


CREATE SEQUENCE public.config_dm_instance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE public.config_dm_instance_id_seq OWNER TO dienstemanager;

CREATE TABLE public.config_dm_instances
(
    config_dm_instance_id serial,
    name text COLLATE pg_catalog."default" NOT NULL,
    host text COLLATE pg_catalog."default" NOT NULL,
    port integer,
    database text COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT config_dm_instances_pkey PRIMARY KEY (config_dm_instance_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.config_dm_instances
    OWNER to dienstemanager;

CREATE TABLE public.layer_delete_requests
(
    layer_delete_request_id serial,
    layer_id integer,
    service_id integer,
    comment text COLLATE pg_catalog."default",
    CONSTRAINT layer_delete_requests_pkey PRIMARY KEY (layer_delete_request_id),
    CONSTRAINT layer_delete_requests_uc UNIQUE (layer_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.layer_delete_requests
    OWNER to dienstemanager;


DROP VIEW public.service_layers;

CREATE OR REPLACE VIEW public.service_layers
 AS
 SELECT layers.layer_id,
    layers.service_id,
    layers.layer_name,
    layers.title,
    layers.title_alt,
    layers.layerattribution,
    layers.additional_categories,
    layers.tilesize,
    layers.legend_url_intranet,
    layers.legend_url_internet,
    layers.transparent,
    layers.transparency,
    layers.output_format,
    layers.scale_min,
    layers.scale_max,
    layers.gfi_theme,
    layers.gfi_config,
    ows.gutter,
    ows.singletile,
    ows.namespace,
    ows.legend_type,
    ows.legend_path,
    ows.featurecount,
    ows.gfi_format,
    ows.gfi_complex,
    ows.hittolerance,
    type3d.request_vertex_normals,
    type3d.maximum_screen_space_error,
    oblique.resolution,
    oblique.minzoom,
    oblique.hidelevels,
    oblique.projection,
    oblique.terrainurl,
    sensorthings.epsg,
    sensorthings.gfi_theme_params,
    sensorthings.filter,
    sensorthings.expand,
    (EXISTS ( SELECT layer_delete_requests_1.comment
           FROM layer_delete_requests layer_delete_requests_1
          WHERE (layer_delete_requests_1.layer_id = layers.layer_id))) AS layer_delete_request,
    layer_delete_requests.comment AS layer_delete_request_comment
   FROM (((((layers
     LEFT JOIN layers_type_ows ows ON ((layers.layer_id = ows.layer_id)))
     LEFT JOIN layers_type_3d type3d ON ((layers.layer_id = type3d.layer_id)))
     LEFT JOIN layers_type_oblique oblique ON ((layers.layer_id = oblique.layer_id)))
     LEFT JOIN layers_type_sensorthings sensorthings ON ((layers.layer_id = sensorthings.layer_id)))
     LEFT JOIN layer_delete_requests layer_delete_requests ON ((layers.layer_id = layer_delete_requests.layer_id)))
  WHERE (layers.deleted = false);

ALTER TABLE public.service_layers
    OWNER TO dienstemanager;

-- DATABASE REFACTORING

DELETE FROM changelog a
WHERE NOT EXISTS (
  SELECT null FROM services b WHERE a.service_id = b.service_id
);

ALTER TABLE changelog ADD CONSTRAINT service_id_fkey
FOREIGN KEY (service_id)
REFERENCES services(service_id)
ON DELETE CASCADE;

DELETE FROM comments a
WHERE NOT EXISTS (
  SELECT null FROM services b WHERE a.service_id = b.service_id
);

ALTER TABLE comments ADD CONSTRAINT service_id_fkey
FOREIGN KEY (service_id)
REFERENCES services(service_id)
ON DELETE CASCADE;

DELETE FROM contact_links a
WHERE NOT EXISTS (
  SELECT null FROM services b WHERE a.service_id = b.service_id
);

ALTER TABLE contact_links ADD CONSTRAINT service_id_fkey
FOREIGN KEY (service_id)
REFERENCES services(service_id)
ON DELETE CASCADE;

DELETE FROM freigaben_links a
WHERE NOT EXISTS (
  SELECT null FROM services b WHERE a.service_id = b.service_id
);

DELETE FROM freigaben_links a
WHERE NOT EXISTS (
  SELECT null FROM freigaben b WHERE a.freigabe_id = b.freigabe_id
);

ALTER TABLE freigaben_links ADD CONSTRAINT freigabe_id_fkey
FOREIGN KEY (freigabe_id)
REFERENCES freigaben(freigabe_id)
ON DELETE CASCADE;

ALTER TABLE freigaben_links ADD CONSTRAINT service_id_fkey
FOREIGN KEY (service_id)
REFERENCES services(service_id)
ON DELETE CASCADE;

DELETE FROM keywords a
WHERE NOT EXISTS (
  SELECT null FROM services b WHERE a.service_id = b.service_id
);

ALTER TABLE keywords ADD CONSTRAINT service_id_fkey
FOREIGN KEY (service_id)
REFERENCES services(service_id)
ON DELETE CASCADE;

DELETE FROM layers a
WHERE NOT EXISTS (
  SELECT null FROM services b WHERE a.service_id = b.service_id
);

ALTER TABLE layers ADD CONSTRAINT service_id_fkey
FOREIGN KEY (service_id)
REFERENCES services(service_id)
ON DELETE CASCADE;

DELETE FROM service_jira_tickets a
WHERE NOT EXISTS (
  SELECT null FROM services b WHERE a.service_id = b.service_id
);

ALTER TABLE service_jira_tickets ADD CONSTRAINT service_id_fkey
FOREIGN KEY (service_id)
REFERENCES services(service_id)
ON DELETE CASCADE;

DELETE FROM service_test_results a
WHERE NOT EXISTS (
  SELECT null FROM services b WHERE a.service_id = b.service_id
);

ALTER TABLE service_test_results ADD CONSTRAINT service_id_fkey
FOREIGN KEY (service_id)
REFERENCES services(service_id)
ON DELETE CASCADE;

DELETE FROM service_test_results_checked a
WHERE NOT EXISTS (
  SELECT null FROM services b WHERE a.service_id = b.service_id
);

ALTER TABLE service_test_results_checked ADD CONSTRAINT service_id_fkey
FOREIGN KEY (service_id)
REFERENCES services(service_id)
ON DELETE CASCADE;

DELETE FROM visits a
WHERE NOT EXISTS (
  SELECT null FROM services b WHERE a.service_id = b.service_id
);

ALTER TABLE visits ADD CONSTRAINT service_id_fkey
FOREIGN KEY (service_id)
REFERENCES services(service_id)
ON DELETE CASCADE;

DELETE FROM layer_json a
WHERE NOT EXISTS (
  SELECT null FROM layers b WHERE a.layer_id = b.layer_id
);

ALTER TABLE layer_json ADD CONSTRAINT layer_id_fkey
FOREIGN KEY (layer_id)
REFERENCES layers(layer_id)
ON DELETE CASCADE;

DELETE FROM layers_type_3d a
WHERE NOT EXISTS (
  SELECT null FROM layers b WHERE a.layer_id = b.layer_id
);

ALTER TABLE layers_type_3d ADD CONSTRAINT layer_id_fkey
FOREIGN KEY (layer_id)
REFERENCES layers(layer_id)
ON DELETE CASCADE;

DELETE FROM layers_type_oblique a
WHERE NOT EXISTS (
  SELECT null FROM layers b WHERE a.layer_id = b.layer_id
);

ALTER TABLE layers_type_oblique ADD CONSTRAINT layer_id_fkey
FOREIGN KEY (layer_id)
REFERENCES layers(layer_id)
ON DELETE CASCADE;

DELETE FROM layers_type_ows a
WHERE NOT EXISTS (
  SELECT null FROM layers b WHERE a.layer_id = b.layer_id
);

ALTER TABLE layers_type_ows ADD CONSTRAINT layer_id_fkey
FOREIGN KEY (layer_id)
REFERENCES layers(layer_id)
ON DELETE CASCADE;

DELETE FROM layers_type_sensorthings a
WHERE NOT EXISTS (
  SELECT null FROM layers b WHERE a.layer_id = b.layer_id
);

ALTER TABLE layers_type_sensorthings ADD CONSTRAINT layer_id_fkey
FOREIGN KEY (layer_id)
REFERENCES layers(layer_id)
ON DELETE CASCADE;
