CREATE TABLE public.config_metadata_catalogs
(
    config_metadata_catalog_id serial,
    name text,
    csw_url text,
    show_doc_url text,
    proxy boolean,
    srs_metadata integer DEFAULT 4326
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.config_metadata_catalogs
    OWNER to dienstemanager;

--Proxy und Name müssen noch manuell über die Oberflächen eingegeben werden

insert into public.config_metadata_catalogs (name,csw_url, show_doc_url)
SELECT DISTINCT source_csw, source_csw, show_doc_url FROM public.services where source_csw is not NULL and source_csw != '';

ALTER TABLE services ADD COLUMN config_metadata_catalog_id integer;

UPDATE public.services SET config_metadata_catalog_id=
(SELECT config_metadata_catalog_id FROM public.config_metadata_catalogs WHERE services.source_csw = config_metadata_catalogs.csw_url);

ALTER TABLE datasets ADD COLUMN config_metadata_catalog_id integer;

UPDATE public.datasets SET config_metadata_catalog_id=
(SELECT config_metadata_catalog_id FROM public.config_metadata_catalogs WHERE datasets.source_csw = config_metadata_catalogs.csw_url);

update datasets set config_metadata_catalog_id = 1;

DROP VIEW public.service_list;
DROP VIEW public.layer_links_dataset;

ALTER TABLE services DROP COLUMN source_csw;
ALTER TABLE services DROP COLUMN show_doc_url;
ALTER TABLE datasets DROP COLUMN source_csw;
ALTER TABLE datasets DROP COLUMN show_doc_url;


DROP FUNCTION public.upsert_dataset(text, text, text, text, text, text, text, text, text, text, text, text, text, text, integer, integer);

CREATE OR REPLACE FUNCTION public.upsert_dataset(
	_dataset_name text,
	_md_id text,
	_rs_id text,
	_cat_opendata text,
	_cat_inspire text,
	_cat_hmbtg text,
	_cat_org text,
	_bbox text,
	_fees text,
	_access_constraints text,
	_config_metadata_catalog_id integer,
	_description text,
	_keywords text,
	_srs_metadata integer,
	_srs_services integer)
RETURNS void
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE
AS $BODY$
DECLARE
BEGIN
    UPDATE datasets SET dataset_name = _dataset_name, rs_id = _rs_id, cat_opendata = _cat_opendata, cat_inspire = _cat_inspire, cat_hmbtg = _cat_hmbtg, cat_org = _cat_org, fees = _fees, access_constraints = _access_constraints, description = _description, keywords = _keywords, config_metadata_catalog_id = _config_metadata_catalog_id, bbox = (SELECT replace(substring(ST_EXTENT(ST_Transform(ST_GeomFromText('MULTIPOINT(' || _bbox || ')', _srs_metadata),_srs_services))::text FROM 5 for char_length(ST_EXTENT(ST_Transform(ST_GeomFromText('MULTIPOINT(' || _bbox || ')',_srs_metadata),_srs_services))::text)-5),' ', ',')) WHERE md_id = _md_id;
    IF NOT FOUND THEN
    INSERT INTO datasets(
            dataset_name, md_id, rs_id, cat_opendata, cat_inspire, cat_hmbtg,
            cat_org, bbox, fees, access_constraints, config_metadata_catalog_id, description, keywords)
    VALUES (_dataset_name, _md_id, _rs_id, _cat_opendata, _cat_inspire, _cat_hmbtg,
            _cat_org, (SELECT replace(substring(ST_EXTENT(ST_Transform(ST_GeomFromText('MULTIPOINT(' || _bbox || ')', _srs_metadata),_srs_services))::text FROM 5 for char_length(ST_EXTENT(ST_Transform(ST_GeomFromText('MULTIPOINT(' || _bbox || ')',_srs_metadata),_srs_services))::text)-5),' ', ',')), _fees, _access_constraints, _config_metadata_catalog_id, _description, _keywords);
    END IF;
END;
$BODY$;

ALTER FUNCTION public.upsert_dataset(text, text, text, text, text, text, text, text, text, text, integer, text, text, integer, integer)
    OWNER TO dienstemanager;




CREATE OR REPLACE VIEW public.dataset_list AS
 SELECT d.dataset_name,
    d.md_id,
    d.rs_id,
    d.cat_opendata,
    d.cat_inspire,
    d.cat_hmbtg,
    d.cat_org,
    d.bbox,
    d.fees,
    d.description,
    d.keywords,
    d.access_constraints,
    ( SELECT a.show_doc_url
           FROM config_metadata_catalogs a
          WHERE a.config_metadata_catalog_id = d.config_metadata_catalog_id) AS show_doc_url,
    ( SELECT a.csw_url
           FROM config_metadata_catalogs a
          WHERE a.config_metadata_catalog_id = d.config_metadata_catalog_id) AS source_csw
   FROM datasets d;

ALTER TABLE public.dataset_list
	OWNER TO dienstemanager;

CREATE OR REPLACE VIEW public.layer_links_dataset AS
 SELECT ll.layer_links_id,
    ll.layer_id,
    ll.md_id,
    ll.service_id,
    d.dataset_name,
    ( SELECT a.show_doc_url
           FROM config_metadata_catalogs a
          WHERE a.config_metadata_catalog_id = d.config_metadata_catalog_id) AS show_doc_url
   FROM layer_links ll
     JOIN datasets d ON ll.md_id::text = d.md_id;

ALTER TABLE public.layer_links_dataset
    OWNER TO dienstemanager;

CREATE OR REPLACE VIEW service_list AS
 SELECT b.service_id,
    b.service_checked_status,
    to_char(b.last_edit_date, 'YYYY-MM-DD HH24:MI'::text) AS last_edit_date,
    b.service_name,
    b.title,
    b.fees,
    b.access_constraints,
    b.description,
    b.editor,
    b.status,
    b.service_type,
    b.software,
    b.server,
    b.folder,
    b.url_ext,
    b.url_int,
    b.url_sec,
    b.security_type,
    b.allowed_groups::text AS allowed_groups,
    b.only_intranet,
    b.only_internet,
    b.datasource,
    b.mapping_description,
    b.cached,
    b.service_md_id,
    ( SELECT a.csw_url
        FROM config_metadata_catalogs a
        WHERE a.config_metadata_catalog_id = b.config_metadata_catalog_id) AS source_csw,
	( SELECT a.show_doc_url
        FROM config_metadata_catalogs a
        WHERE a.config_metadata_catalog_id = b.config_metadata_catalog_id) AS show_doc_url,
	( SELECT a.name
        FROM config_metadata_catalogs a
        WHERE a.config_metadata_catalog_id = b.config_metadata_catalog_id) AS csw_name,
    b.dsc_complete,
    b.inspire_relevant,
    b.last_edited_by,
    b.version,
    b.qa,
    to_char(b.update_date::timestamp with time zone, 'YYYY-MM-DD'::text) AS update_date,
    b.update_unit,
    b.update_type,
    b.update_process_type,
    b.update_process_name,
    b.update_process_location,
    b.update_process_description,
    b.update_number
   FROM services b;

ALTER TABLE service_list
  OWNER TO dienstemanager;
