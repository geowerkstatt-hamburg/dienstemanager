ALTER TABLE public.layers_type_ows
    ADD COLUMN datetime_column text;

DROP VIEW public.service_layers;
CREATE OR REPLACE VIEW public.service_layers
    AS
     SELECT layers.layer_id,
    layers.service_id,
    layers.layer_name,
    layers.title,
    layers.title_alt,
    layers.layerattribution,
    layers.additional_categories,
    layers.tilesize,
    layers.legend_url_intranet,
    layers.legend_url_internet,
    layers.transparent,
    layers.transparency,
    layers.output_format,
    layers.scale_min,
    layers.scale_max,
    layers.gfi_theme,
    layers.gfi_config,
    layers.gfi_asnewwindow,
    layers.gfi_windowspecs,
	layers.service_url_is_visible,
    ows.gutter,
    ows.singletile,
    ows.namespace,
    ows.datetime_column,
    ows.legend_type,
    ows.legend_path,
    ows.featurecount,
    ows.gfi_format,
    ows.gfi_complex,
    ows.hittolerance,
    type3d.request_vertex_normals,
    type3d.maximum_screen_space_error,
    oblique.resolution,
    oblique.minzoom,
    oblique.hidelevels,
    oblique.projection,
    oblique.terrainurl,
    sensorthings.epsg,
    sensorthings.gfi_theme_params,
    sensorthings.rootel,
    sensorthings.filter,
    sensorthings.expand,
    sensorthings.related_wms_layers,
    sensorthings.style_id,
    sensorthings.cluster_distance,
    sensorthings.load_things_only_in_current_extent,
    sensorthings.mouse_hover_field,
    sensorthings.sta_test_url,
    vectortiles.extent,
    vectortiles.origin,
    vectortiles.resolutions,
    vectortiles.visibility,
    vectortiles.vtstyles,
    (EXISTS ( SELECT layer_delete_requests_1.comment
           FROM layer_delete_requests layer_delete_requests_1
          WHERE (layer_delete_requests_1.layer_id = layers.layer_id))) AS layer_delete_request,
    layer_delete_requests.comment AS layer_delete_request_comment
   FROM ((((((layers
     LEFT JOIN layers_type_ows ows ON ((layers.layer_id = ows.layer_id)))
     LEFT JOIN layers_type_3d type3d ON ((layers.layer_id = type3d.layer_id)))
     LEFT JOIN layers_type_oblique oblique ON ((layers.layer_id = oblique.layer_id)))
     LEFT JOIN layers_type_sensorthings sensorthings ON ((layers.layer_id = sensorthings.layer_id)))
     LEFT JOIN layers_type_vectortiles vectortiles ON ((layers.layer_id = vectortiles.layer_id)))
     LEFT JOIN layer_delete_requests layer_delete_requests ON ((layers.layer_id = layer_delete_requests.layer_id)))
  WHERE (layers.deleted = false);

ALTER TABLE public.service_layers OWNER TO dienstemanager;