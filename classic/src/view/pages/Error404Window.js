Ext.define("DiensteManager.view.pages.Error404Window", {
    extend: "Ext.window.Window",
    alias: "widget.pageserror404window",

    requires: [
        "Ext.container.Container",
        "Ext.toolbar.Spacer",
        "Ext.form.Label"
    ],

    autoShow: true,
    cls: "error-page-container",
    closable: false,
    title: "DiensteManager",
    titleAlign: "center",
    maximized: true,
    modal: true,

    layout: {
        type: "vbox",
        align: "center",
        pack: "center"
    },

    items: [
        {
            xtype: "container",
            width: 400,
            cls: "error-page-inner-container",
            layout: {
                type: "vbox",
                align: "center",
                pack: "center"
            },
            items: [
                {
                    xtype: "label",
                    cls: "error-page-top-text",
                    text: "404"
                },
                {
                    xtype: "label",
                    cls: "error-page-desc",
                    html: "<div>Unzulässige Route!</div><div>Zurück zur <a href=\"#dienst_liste\"> Startseite </a></div>"
                },
                {
                    xtype: "tbspacer",
                    flex: 1
                }
            ]
        }
    ]
});
