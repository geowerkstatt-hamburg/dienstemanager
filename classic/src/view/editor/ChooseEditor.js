Ext.define("DiensteManager.view.editor.ChooseEditor", {
    extend: "Ext.window.Window",
    id: "chooseeditor-window",
    alias: "editor.ChooseEditor",
    requires: [
        "Ext.window.*",
        "Ext.data.*",
        "Ext.util.*",
        "Ext.form.*"
    ],

    listeners: {
        beforeRender: "beforeRender"
    },

    controller: "chooseeditor",

    height: 200,
    width: 500,
    title: "Bearbeiter Wählen",
    closable: false,
    modal: true,
    closeAction: "hide",
    items: [
        {
            xtype: "container",
            padding: "20 20 20 20",
            autoScroll: true,
            items: [{
                xtype: "label",
                id: "chooseeditor_label",
                text: "Bitte wählen Sie einen Bearbeiter, unter dessen Namen die Änderungen gespeichert werden"
            }, {
                xtype: "combo",
                fieldLabel: "Bearbeiter",
                allowBlank: false,
                id: "chooseeditor_combo",
                labelWidth: 160,
                width: 450
            }]
        }
    ],
    buttons: [
        {
            text: "OK",
            listeners: {
                click: "onChooseEditorOk"
            }
        }
    ]
});
