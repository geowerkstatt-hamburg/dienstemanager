Ext.define("DiensteManager.view.servicedetails.ServiceDetails_Freigaben", {
    extend: "Ext.container.Container",
    xtype: "servicedetails-freigaben",

    requires: [
        "Ext.grid.*",
        "Ext.data.*",
        "Ext.util.*"
    ],

    controller: "servicedetails_freigaben",

    bodyPadding: "5 5 5 5",
    items: [
        {
            xtype: "grid",
            store: "LinkedFreigaben",
            id: "linkedfreigaben-grid",
            autoScroll: true,
            maxHeight: 677,
            height: Ext.Element.getViewportHeight() - 90,
            maxWidth: 1570,
            width: Ext.Element.getViewportWidth(),
            border: false,
            style: "border: solid #d0d0d0 1px",
            hideHeaders: true,
            viewConfig: {
                enableTextSelection: true,
                stripeRows: false
            },
            columns: [
                {
                    header: "Datenbezeichnung",
                    dataIndex: "datenbezeichnung",
                    align: "left",
                    flex: 1
                }, {
                    header: "Link",
                    dataIndex: "link",
                    width: 80,
                    align: "center",
                    renderer: function (value) {
                        return "<a href=\"" + value + "\" target=\"_blank\">Link</a>";
                    }
                }, {
                    xtype: "actioncolumn",
                    header: "Löschen",
                    name: "deletefreigabelink",
                    tooltip: "Die Freigabeverlinkung wird gelöscht!",
                    align: "center",
                    handler: "onDeleteFreigabeLink",
                    width: 27,
                    items: [
                        {
                            icon: "resources/images/drop-no.png"
                        }
                    ]
                }
            ],
            columnLines: true,
            dockedItems: [{
                xtype: "toolbar",
                dock: "top",
                items: [{
                    xtype: "button",
                    id: "onSearchFreigabe",
                    tooltip: "Freigabe suchen und zurodnen",
                    autoEl: {
                        tag: "div",
                        "data-qtip": "Freigaben suchen und zuordnen"
                    },
                    iconCls: "right-icon white-icon pictos pictos-attachment",
                    text: "Freigabe zuordnen",
                    listeners: {
                        click: "onSearchFreigabe"
                    }
                }]
            }]
        }
    ]
});
