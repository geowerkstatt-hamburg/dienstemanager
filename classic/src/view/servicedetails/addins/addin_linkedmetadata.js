Ext.define("DiensteManager.view.servicedetails.addins.addin_linkedmetadata", {
    extend: "Ext.grid.Panel",

    requires: [
        "Ext.grid.*",
        "Ext.data.*"
    ],

    xtype: "addin_linkedmetadata",

    store: "LayerLinks",
    name: "layerLinksGrid",
    title: "gekoppelte Datensätze",
    autoScroll: true,
    id: "layerlinks-grid",
    viewConfig: {
        enableTextSelection: true
    },
    columnLines: true,
    selModel: "rowmodel",
    columns: [
        {
            header: "Gekoppelter Datensatz",
            dataIndex: "dataset_name",
            align: "left",
            flex: 1
        },
        {
            xtype: "actioncolumn",
            header: "Info",
            name: "aktionen",
            align: "center",
            sortable: false,
            menuDisabled: true,
            flex: 20 / 100,
            items: [
                {
                    icon: "resources/images/info.png",
                    tooltip: "Anzeigen der Metadaten",
                    handler: "onClickMetadataLink"
                }
            ]
        },
        {
            xtype: "actioncolumn",
            header: "Löschen",
            name: "deleteDSLink",
            id: "deletedslink",
            hidden: window.read_only,
            align: "center",
            flex: 18 / 100,
            items: [
                {
                    icon: "resources/images/drop-no.png",
                    tooltip: "Metadatenverknüpfung löschen",
                    handler: "onDeleteDSLink"
                }
            ]
        }
    ]
});
