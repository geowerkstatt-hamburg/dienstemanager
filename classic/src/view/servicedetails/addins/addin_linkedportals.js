Ext.define("DiensteManager.view.servicedetails.addins.addin_linkedportals", {
    extend: "Ext.grid.Panel",

    requires: [
        "Ext.grid.*",
        "Ext.data.*"
    ],

    xtype: "addin_linkedportals",

    store: "Portals",
    title: "gekoppelte Portale",
    id: "portallinks-grid",
    autoScroll: true,
    viewConfig: {
        enableTextSelection: true
    },
    columnLines: true,
    selModel: "rowmodel",
    columns: [
        {
            header: "Portal",
            dataIndex: "title",
            align: "left",
            flex: 1
        }, {
            header: "",
            dataIndex: "url",
            width: 150,
            align: "center",
            renderer: function (value) {
                if (value !== "") {
                    return "<a href=\"" + value + "\" target=\"_blank\">Link</a>";
                }
                else {
                    return "";
                }
            }
        }
    ]
});
