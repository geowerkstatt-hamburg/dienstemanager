Ext.define("DiensteManager.view.servicedetails.addins.addin_freigaben", {
    extend: "Ext.container.Container",

    requires: [
        "Ext.grid.*",
        "Ext.data.*",
        "Ext.form.*"
    ],

    xtype: "addin_freigaben",
    maxWidth: 750,
    width: (Ext.Element.getViewportWidth() / 2) - 35,

    initComponent: function () {
        this.items = [
            {
                xtype: "button",
                id: "onSearchFreigabe",
                margin: "0 0 5 0",
                disabled: window.read_only,
                tooltip: "Freigabe suchen und zurodnen",
                autoEl: {
                    tag: "div",
                    "data-qtip": "Freigaben suchen und zuordnen"
                },
                iconCls: "right-icon white-icon pictos pictos-attachment",
                text: "Freigabe zuordnen",
                listeners: {
                    click: "onSearchFreigabe"
                }
            }, {
                xtype: "grid",
                store: "LinkedFreigaben",
                id: "linkedfreigaben-grid",
                height: 155,
                autoScroll: true,
                border: false,
                style: "border: solid #d0d0d0 1px",
                margin: "0 0 5 0",
                hideHeaders: true,
                viewConfig: {
                    enableTextSelection: true,
                    stripeRows: false
                },
                columns: [
                    {
                        header: "Datenbezeichnung",
                        dataIndex: "datenbezeichnung",
                        align: "left",
                        flex: 1
                    }, {
                        header: "Link",
                        dataIndex: "link",
                        width: 80,
                        align: "center",
                        renderer: function (value) {
                            return "<a href=\"" + value + "\" target=\"_blank\">Link</a>";
                        }
                    }, {
                        xtype: "actioncolumn",
                        header: "Löschen",
                        name: "deletefreigabelink",
                        tooltip: "Die Freigabeverlinkung wird gelöscht!",
                        align: "center",
                        handler: "onDeleteFreigabeLink",
                        hidden: window.read_only,
                        width: 27,
                        items: [
                            {
                                icon: "resources/images/drop-no.png"
                            }
                        ]
                    }],
                columnLines: true
            }];
        this.callParent(arguments);
    }
});
