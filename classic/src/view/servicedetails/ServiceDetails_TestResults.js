Ext.define("DiensteManager.view.servicedetails.ServiceDetails_TestResults", {
    extend: "Ext.container.Container",
    xtype: "servicedetails-testresults",
    controller: "servicedetails_testresults",
    bodyPadding: "5 5 5 5",
    items: [
        {
            xtype: "grid",
            store: "ServiceTestResults",
            autoScroll: true,
            maxHeight: 338,
            height: (Ext.Element.getViewportHeight() - 90) / 2,
            id: "testresults-grid",
            viewConfig: {
                enableTextSelection: true
            },
            columnLines: true,
            selModel: "rowmodel",
            columns: [
                {
                    header: "Meldungen",
                    dataIndex: "text",
                    flex: 80 / 100,
                    align: "left",
                    cellWrap: true
                }, {
                    xtype: "actioncolumn",
                    header: "Löschen",
                    tooltip: "Die Meldung wird gelöscht!",
                    hidden: window.read_only,
                    align: "center",
                    flex: 10 / 100,
                    handler: "onDeleteTestresult",
                    items: [
                        {
                            icon: "resources/images/drop-no.png"
                        }
                    ]
                }, {
                    xtype: "actioncolumn",
                    header: "Bestätigen",
                    hidden: window.read_only,
                    tooltip: "Die Meldung wird in die Liste der bestätigten Fehler verschoben!",
                    align: "center",
                    flex: 10 / 100,
                    handler: "onCheckTestresult",
                    items: [
                        {
                            icon: "resources/images/gruen.png"
                        }
                    ]
                }
            ],
            dockedItems: [{
                xtype: "toolbar",
                dock: "top",
                items: [{
                    id: "startservicequickcheck",
                    disabled: true,
                    text: "Testen",
                    handler: "onStartServiceQuickCheck"
                },
                {
                    id: "servicequickcheckinfo",
                    text: "?",
                    handler: "onServiceQuickCheckInfo"
                }]
            }]
        },
        {
            xtype: "grid",
            store: "ServiceTestResultsChecked",
            autoScroll: true,
            maxHeight: 338,
            height: (Ext.Element.getViewportHeight() - 90) / 2,
            id: "testresultschecked-grid",
            viewConfig: {
                enableTextSelection: true
            },
            columnLines: true,
            selModel: "rowmodel",
            columns: [
                {
                    header: "bestätigte Meldungen",
                    dataIndex: "text",
                    flex: 90 / 100,
                    align: "left",
                    cellWrap: true
                }, {
                    xtype: "actioncolumn",
                    header: "Löschen",
                    hidden: window.read_only,
                    tooltip: "Die Meldung wird gelöscht!",
                    align: "center",
                    flex: 10 / 100,
                    handler: "onDeleteTestresultChecked",
                    items: [
                        {
                            icon: "resources/images/drop-no.png"
                        }
                    ]
                }
            ]
        }
    ]
});
