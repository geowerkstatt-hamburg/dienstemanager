Ext.define("DiensteManager.view.servicedetails.allowedgroupschooser.AllowedGroupsChooserController", {
    extend: "Ext.app.ViewController",
    alias: "controller.allowedgroupschooser",

    onSearchADGroups: function () {
        var groupname = Ext.getCmp("adgroup_search_field").getSubmitValue();

        Ext.getStore("ADGroups").load({
            params: {groupname: groupname}
        });
    },

    onAddToAllowedGroups: function (grid, rowIndex) {
        var data = grid.getStore().getAt(rowIndex).data,
            allowed_groups_store = Ext.getStore("AllowedADGroups");

        allowed_groups_store.add(data);
    },

    onDeleteAllowedGroup: function (grid, rowIndex) {
        var record = grid.getStore().getAt(rowIndex),
            allowed_groups_store = Ext.getStore("AllowedADGroups");

        allowed_groups_store.remove(record);
    },

    onSaveAllowedGroups: function () {
        var allowed_groups_store = Ext.getStore("AllowedADGroups"),
            service_id = Ext.getCmp("serviceid").getSubmitValue(),
            allowed_groups = [];

        allowed_groups_store.each(function (allowed_group) {
            allowed_groups.push(allowed_group.get("name"));
        });

        Ext.Ajax.request({
            url: "backend/updateAllowedGroups",
            method: "POST",
            jsonData: {
                service_id: service_id,
                allowed_groups: allowed_groups,
                last_edited_by: window.auth_user,
                last_edit_date: moment().format("YYYY-MM-DD HH:mm")
            },
            success: function () {
                Ext.getStore("ServiceList").reload();
            },
            failure: function (response) {
                console.log(Ext.decode(response.responseText));
            }
        });
    }
});
