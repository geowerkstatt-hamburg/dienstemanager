Ext.define("DiensteManager.view.servicedetails.ServiceDetails_Jira", {
    extend: "Ext.container.Container",
    xtype: "servicedetails-jira",
    requires: [
        "Ext.grid.*",
        "Ext.data.*",
        "Ext.util.*"
    ],
    controller: "servicedetails_jiracontroller",
    bodyPadding: "5 5 5 5",
    items: [
        {
            xtype: "grid",
            id: "issues-grid",
            store: "JiraTickets",
            autoScroll: true,
            maxHeight: 677,
            height: Ext.Element.getViewportHeight() - 90,
            maxWidth: 1570,
            width: Ext.Element.getViewportWidth(),
            viewConfig: {
                enableTextSelection: true,
                stripeRows: false
            },
            columnLines: true,
            columns: [
                {
                    text: "Projekt",
                    dataIndex: "project",
                    minWidth: 90,
                    autoSizeColumn: true,
                    align: "left"
                }, {
                    text: "Key",
                    dataIndex: "key",
                    minWidth: 120,
                    autoSizeColumn: true,
                    align: "left"
                }, {
                    text: "Autor",
                    dataIndex: "creator",
                    minWidth: 150,
                    autoSizeColumn: true,
                    align: "left"
                }, {
                    text: "Bearbeiter",
                    dataIndex: "assignee",
                    minWidth: 150,
                    autoSizeColumn: true,
                    align: "left"
                }, {
                    text: "URL",
                    width: 85,
                    align: "center",
                    renderer: function (value, metadata, record) {
                        return "<a href=\"" + DiensteManager.Config.getJiraBrowseUrl() + "/" + record.get("key") + "\" target=\"_blank\">Jira Link</a>";
                    }
                }, {
                    text: "Zusammenfassung",
                    dataIndex: "summary",
                    flex: 1,
                    align: "left"
                }, {
                    xtype: "actioncolumn",
                    text: "Jira Ticket entfernen",
                    minWidth: 145,
                    autoSizeColumn: true,
                    sortable: false,
                    menuDisabled: true,
                    align: "center",
                    handler: "onDeleteIssue",
                    items: [{
                        iconCls: "x-fa fa-minus-circle",
                        tooltip: "Jira Ticket entfernen"
                    }]
                }
            ],
            dockedItems: [{
                xtype: "toolbar",
                dock: "top",
                items: [{
                    xtype: "textfield",
                    id: "issueSearchField",
                    width: 200,
                    tooltip: "Nach Jira Ticket suchen",
                    enableKeyEvents: true,
                    listeners: {
                        "keypress": "onKeyPress"
                    }
                }, {
                    id: "searchIssueButton",
                    disabled: window.read_only,
                    text: "Jira Tickets Suchen",
                    handler: "onSearchIssue"
                }, {
                    xtype: "tbspacer",
                    flex: 1
                }, {
                    id: "openJiraTicketWindow",
                    disabled: window.read_only,
                    text: "Jira Ticket anlegen",
                    handler: "onOpenJiraTicketWindow"
                }]
            }]
        }
    ]
});
