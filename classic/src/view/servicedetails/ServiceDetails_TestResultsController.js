Ext.define("DiensteManager.view.servicedetails.ServiceDetails_TestResultsController", {
    extend: "Ext.app.ViewController",
    alias: "controller.servicedetails_testresults",

    onDeleteTestresult: function (grid, rowIndex) {
        var data = grid.getStore().getAt(rowIndex).data;

        Ext.Ajax.request({
            url: "backend/deletetestresult",
            method: "POST",
            jsonData: {
                service_test_results_id: data.service_test_results_id
            },
            success: function () {
                grid.getStore().removeAt(rowIndex);
            },
            failure: function (response) {
                console.log(Ext.decode(response.responseText));
            }
        });
    },

    onCheckTestresult: function (grid, rowIndex) {
        var data = grid.getStore().getAt(rowIndex).data;

        Ext.Ajax.request({
            url: "backend/checktestresult",
            method: "POST",
            jsonData: {
                service_test_results_id: data.service_test_results_id
            },
            success: function () {
                grid.getStore().removeAt(rowIndex);
                Ext.getStore("ServiceTestResultsChecked").load({
                    params: {service_id: Ext.getCmp("serviceid").getSubmitValue()},
                    callback: function () {}
                });
            },
            failure: function (response) {
                console.log(Ext.decode(response.responseText));
            }
        });
    },

    onDeleteTestresultChecked: function (grid, rowIndex) {
        var data = grid.getStore().getAt(rowIndex).data;

        Ext.Ajax.request({
            url: "backend/deletetestresultchecked",
            method: "POST",
            jsonData: {
                service_test_results_id: data.service_test_results_id
            },
            success: function () {
                grid.getStore().removeAt(rowIndex);
            },
            failure: function (response) {
                console.log(Ext.decode(response.responseText));
            }
        });
    },

    onStartServiceQuickCheck: function (btn) {
        var service_id = Ext.getCmp("serviceid").getSubmitValue(),
            loadingsMask = new Ext.LoadMask({
                msg: "Bitte warten...",
                target: Ext.getCmp("servicedetails-window")
            });

        btn.setDisabled(true);

        loadingsMask.show();

        Ext.Ajax.request({
            url: "backend/startservicequickcheck",
            method: "POST",
            jsonData: {
                service_id: service_id
            },
            success: function (response) {
                var jsonobj = Ext.decode(response.responseText),
                    status = jsonobj.status;

                if (status === "canceled") {
                    Ext.MessageBox.alert("Fehler", jsonobj.error_message.message);
                    loadingsMask.hide();
                    btn.setDisabled(false);
                }
                else {
                    Ext.getCmp("servicecheckedstatus").setValue(status);

                    Ext.getStore("ServiceTestResults").load({
                        params: {service_id: service_id},
                        callback: function (records) {
                            if (records.length === 0) {
                                Ext.MessageBox.alert("Erfolgreich!", "keine Fehler gefunden.");
                            }
                            loadingsMask.hide();
                            btn.setDisabled(false);
                            Ext.getStore("ServiceList").reload();
                        }
                    });
                }
            },
            failure: function (response) {
                loadingsMask.hide();
                btn.setDisabled(false);
                console.log(Ext.decode(response.responseText));
            }
        });
    },

    onServiceQuickCheckInfo: function () {
        Ext.MessageBox.show({
            title: "Information",
            msg: "Folgende Tests werden ausgeführt: </br><ul>" +
            "<li>Stimmen die Layer in den Capabilities und dem Dienstemanager überein?</li>" +
            "<li>Werden alle gewünschten EPSG Codes unterstützt?</li>" +
            "<li>Sind Information zu Datenaktualisierung eingetragen?</li>" +
            "<li>(bei eingetragener Metadatenkopplung) Ist die Daten-Dienste-Kopplung in den Capabilties korrekt?</li>" +
            "<li>(bei eingetragener Metadatenkopplung) Ist der INSPIRE extended Capabilties Block in den Capabilities vorhanden?</li>" +
            "<li>(bei eingetragener Metadatenkopplung) Sind die Diensttitel im Metadatenkatalog, Dienstemanager und Capabilties synchron?</li>" +
            "<li>(bei eingetragener Metadatenkopplung) Sind die Nutzungsbedingungen (Fees) im Metadatenkatalog, Dienstemanager und Capabilties synchron?</li>" +
            "<li>(bei eingetragener Metadatenkopplung) Sind die Zugriffsbeschränkungen (Access Constraints) im Metadatenkatalog, Dienstemanager und Capabilties synchron?</li>" +
            "<li>(bei eingetragener Metadatenkopplung) Stimmen die Dienst-URLs im Dienstemanager und dem Metadatenkatalog überein?</li>" +
            "<li>(bei aktivem JIRA Modul) ist ein JIRA Ticket verknüpft?</li>" +
            "<li>Sind Nutzungsbedingungen eingetragen? Prüfen, ob Metadaten ebenfalls leer sind.</li></ul>",
            buttons: Ext.MessageBox.OK,
            icon: Ext.MessageBox.QUESTION
        });
    }
});
