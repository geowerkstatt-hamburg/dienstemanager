Ext.define("DiensteManager.view.servicedetails.metadata.LinkServiceMDController", {
    extend: "Ext.app.ViewController",
    alias: "controller.linkservicemd",

    onSaveServiceMDLink: function () {
        var datasetsgrid = Ext.getCmp("linkservicemd-grid");

        if (datasetsgrid.getSelectionModel().getSelection()[0] !== undefined) {
            var selectedDataset = datasetsgrid.getSelectionModel().getSelection()[0].data;

            Ext.getCmp("metadata_uuid").setValue(selectedDataset.md_uuid);

            if (selectedDataset.md_uuid !== "") {
                var selection = Ext.getCmp("servicesourcecswname").getSelection();

                Ext.getCmp("metadata_uuid_link").setValue("<a href=\"" + selection.data.show_doc_url + selectedDataset.md_uuid + "\" target=\"_blank\">Metadaten anzeigen</a>");
                Ext.getCmp("deleteMetadataCoupling").setDisabled(false);
            }

            var mb = Ext.MessageBox;

            mb.buttonText.yes = "ok";
            mb.buttonText.no = "abbrechen";

            mb.confirm("Hinweis", "Die Angaben zu Titel, Beschreibung, Nutzungsbedingungen und Zugriffsbeschränkungen werden mit den Informationen aus dem Metadatenkatalog überschrieben.", function (btn) {
                if (btn === "yes") {
                    Ext.getCmp("servicedetails-form").getForm().setValues({
                        servicetitle: selectedDataset.title,
                        servicedescription: selectedDataset.description,
                        serviceinspirerelevant: selectedDataset.inspire,
                        servicefees: selectedDataset.fees,
                        serviceaccessconstraints: selectedDataset.access_constraints
                    });
                    Ext.getCmp("save_service_details_button").setDisabled(false);
                }
            });

            // var existing_keywords = Ext.getCmp("servicekeywords").getValueRecords(),
            //     merged_keywords = selectedDataset.keywords.concat(existing_keywords);

            // Ext.getCmp("servicekeywords").setValue(_.uniq(merged_keywords));
        }

        Ext.getCmp("linkservicemd-window").close();
    }
});
