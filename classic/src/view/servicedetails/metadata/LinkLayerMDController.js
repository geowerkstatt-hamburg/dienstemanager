Ext.define("DiensteManager.view.servicedetails.metadata.LinkLayerMDController", {
    extend: "Ext.app.ViewController",
    alias: "controller.linklayermd",

    loadingsMask: null,

    onSaveLayerLinks: function () {
        var layergrid = Ext.getCmp("layers-grid"),
            datasetsgrid = Ext.getCmp("linkdatasets-grid"),
            selectedLayer = layergrid.getSelectionModel().getSelection()[0].data,
            loadingsMask = new Ext.LoadMask({
                msg: "Bitte warten...",
                target: Ext.getCmp("linklayermd-window")
            });

        if (datasetsgrid.getSelectionModel().getSelection()[0] !== undefined) {
            loadingsMask.show();

            var config_metadata_catalog_id = datasetsgrid.getSelectionModel().getSelection()[0].data.config_metadata_catalog_id;
            var configMetadataCatalogsStore = Ext.getStore("ConfigMetadataCatalogs");
            var md_catalog = configMetadataCatalogsStore.getAt(configMetadataCatalogsStore.findExact("config_metadata_catalog_id", config_metadata_catalog_id));

            Ext.Ajax.request({
                url: "backend/setlayerlinks",
                method: "POST",
                jsonData: {
                    dataset: datasetsgrid.getSelectionModel().getSelection()[0].data,
                    layer_id: selectedLayer.layer_id,
                    service_id: Ext.getCmp("serviceid").getSubmitValue(),
                    linkalllayers: Ext.getCmp("linkalllayers").getValue(),
                    last_edited_by: window.auth_user,
                    last_edit_date: moment().format("YYYY-MM-DD HH:mm"),
                    srs_metadata: md_catalog.get("srs_metadata")
                },
                success: function () {
                    loadingsMask.hide();
                    Ext.getCmp("linklayermd-window").close();

                    Ext.getStore("LayerLinks").reload({
                        params: {layer_id: selectedLayer.layer_id}
                    });
                    DiensteManager.app.getController("DiensteManager.controller.PublicFunctions").setDscStatus(Ext.getCmp("serviceid").getSubmitValue());
                },
                failure: function (response) {
                    loadingsMask.hide();
                    Ext.MessageBox.alert("Status", "Es ist ein Fehler aufgetreten!");
                    console.log(Ext.decode(response.responseText));
                }
            });
        }
    }
});
