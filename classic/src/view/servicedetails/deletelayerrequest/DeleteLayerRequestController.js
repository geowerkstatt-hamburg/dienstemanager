Ext.define("DiensteManager.view.servicedetails.deletelayerrequest.DeleteLayerRequestController", {
    extend: "Ext.app.ViewController",
    alias: "controller.deletelayerrequest",

    beforeRender: function () {
        var selectedLayer = Ext.getCmp("layers-grid").getSelectionModel().getSelection()[0].data;

        console.log();

        if (selectedLayer.layer_delete_request) {
            Ext.getCmp("deletelayerrequest-remove").setDisabled(false);
        }
        if (selectedLayer.layer_delete_request_comment) {
            Ext.getCmp("deletelayerrequest-comment").setValue(selectedLayer.layer_delete_request_comment);
        }
    },

    onSaveDeleteLayerRequest: function () {
        var linked_portals = [],
            selectedLayer = Ext.getCmp("layers-grid").getSelectionModel().getSelection()[0].data;

        Ext.getStore("Portals").getRange().forEach(function (record) {
            linked_portals.push(record.data);
        });

        Ext.Ajax.request({
            url: "backend/setdeletelayerrequest",
            method: "POST",
            jsonData: {
                layer_id: selectedLayer.layer_id,
                title: selectedLayer.title,
                comment: Ext.getCmp("deletelayerrequest-comment").getSubmitValue(),
                linked_portals: linked_portals,
                service_id: selectedLayer.service_id,
                last_edited_by: window.auth_user,
                last_edit_date: moment().format("YYYY-MM-DD HH:mm")
            },
            success: function (response) {
                var results = Ext.decode(response.responseText);

                if (results.success) {
                    Ext.getStore("Layers").reload({
                        params: {service_id: selectedLayer.service_id},
                        callback: function () {
                            Ext.getCmp("deletelayerrequest-remove").setDisabled(false);
                            Ext.getCmp("deletelayerrequest-window").close();
                        }
                    });
                }
                else {
                    Ext.MessageBox.alert("Fehler", "Löschvermerk konnte nicht gespeichert werden");
                }
            },
            failure: function (response) {
                console.log(Ext.decode(response.responseText));
                Ext.MessageBox.alert("Fehler", "Löschvermerk konnte nicht gespeichert werden");
            }
        });
    },

    onRemoveDeleteLayerRequest: function () {
        var selectedLayer = Ext.getCmp("layers-grid").getSelectionModel().getSelection()[0].data;

        Ext.Ajax.request({
            url: "backend/removedeletelayerrequest",
            method: "POST",
            jsonData: {
                layer_id: Ext.getCmp("layerid").getSubmitValue(),
                service_id: Ext.getCmp("serviceid").getSubmitValue(),
                last_edited_by: window.auth_user,
                last_edit_date: moment().format("YYYY-MM-DD HH:mm")
            },
            success: function (response) {
                var results = Ext.decode(response.responseText);

                if (results.success) {
                    Ext.getStore("Layers").reload({
                        params: {service_id: selectedLayer.service_id},
                        callback: function () {
                            Ext.getCmp("deletelayerrequest-remove").setDisabled(true);
                            Ext.getCmp("deletelayerrequest-window").close();
                        }
                    });
                }
                else {
                    Ext.MessageBox.alert("Fehler", "Löschvermerk konnte nicht gelöscht werden");
                }
            },
            failure: function (response) {
                console.log(Ext.decode(response.responseText));
                Ext.MessageBox.alert("Fehler", "Löschvermerk konnte nicht gelöscht werden");
            }
        });
    }
});
