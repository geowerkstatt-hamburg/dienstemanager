Ext.define("DiensteManager.view.servicedetails.ServiceDetails_DataUpdateStatusController", {
    extend: "Ext.app.ViewController",
    alias: "controller.servicedetails_dataupdatestatus",

    onRefreshUnitChange: function () {
        var refreshunit = Ext.getCmp("dataupdatestatusrefreshunit").getSubmitValue(),
            refreshnumber = Ext.getCmp("dataupdatestatusrefreshnumber");

        if (refreshunit === "unregelmäßig") {
            refreshnumber.setValue("");
            refreshnumber.setDisabled(true);
        }
        else {
            refreshnumber.setDisabled(false);
        }
    },

    onUpdateManually: function () {
        var manually = Ext.getCmp("dataupdatestatusmanually").getValue();

        if (manually) {
            Ext.getCmp("dataupdatestatuslastupdate").setDisabled(false);
            Ext.getCmp("dataupdatestatuscontainer").setDisabled(false);
        }
    },

    onUpdateAutomatically: function () {
        var automatically = Ext.getCmp("dataupdatestatusautomatically").getValue();

        if (automatically) {
            Ext.getCmp("dataupdatestatuslastupdate").setDisabled(true);
            Ext.getCmp("dataupdatestatuscontainer").setDisabled(false);
        }
    },

    onUpdateRealtime: function () {
        var realtime = Ext.getCmp("dataupdatestatusrealtime").getValue();

        if (realtime) {
            Ext.getCmp("dataupdatestatuslastupdate").setDisabled(true);
            Ext.getCmp("dataupdatestatuscontainer").setDisabled(true);
        }
    },

    onSaveDataUpdateStatusForm: function () {
        var service_id = Ext.getCmp("serviceid").getSubmitValue(),
            automatically = Ext.getCmp("dataupdatestatusautomatically").getValue(),
            realtime = Ext.getCmp("dataupdatestatusrealtime").getValue(),
            refreshnumber = Ext.getCmp("dataupdatestatusrefreshnumber").getSubmitValue(),
            refreshunit = Ext.getCmp("dataupdatestatusrefreshunit").getSubmitValue(),
            last_update = Ext.getCmp("dataupdatestatuslastupdate").getSubmitValue(),
            processtype = Ext.getCmp("dataupdatestatusprocesstype").getSubmitValue(),
            processname = Ext.getCmp("dataupdatestatusprocessname").getSubmitValue(),
            processlocation = Ext.getCmp("dataupdatestatusprocesslocation").getSubmitValue(),
            processdescription = Ext.getCmp("dataupdatestatusprocessdescription").getSubmitValue(),
            update_type = "manuell";

        if (realtime) {
            update_type = "realtime";
        }
        else if (automatically) {
            update_type = "automatisch";
        }

        if (last_update === "") {
            last_update = null;
        }

        if (service_id) {
            Ext.Ajax.request({
                url: "backend/updatedataupdatestatus",
                method: "POST",
                jsonData: {
                    service_id: service_id,
                    update_type: update_type,
                    update_number: refreshnumber,
                    update_unit: refreshunit,
                    update_date: last_update,
                    update_process_type: processtype,
                    update_process_name: processname,
                    update_process_location: processlocation,
                    update_process_description: processdescription,
                    last_edited_by: window.auth_user,
                    last_edit_date: moment().format("YYYY-MM-DD HH:mm")
                },
                success: function () {
                    Ext.getStore("ServiceList").reload();
                },
                failure: function (response) {
                    console.log(Ext.decode(response.responseText));
                }
            });
        }
    }
});
