Ext.define("DiensteManager.view.servicedetails.ServiceDetails_DataUpdateStatus", {
    extend: "Ext.container.Container",
    xtype: "servicedetails-dataupdatestatus",

    requires: [
        "Ext.grid.*",
        "Ext.data.*",
        "Ext.util.*",
        "Ext.form.*"
    ],

    controller: "servicedetails_dataupdatestatus",

    bodyPadding: "5 5 5 5",
    items: [
        {
            xtype: "form",
            header: false,
            name: "dataupdatestatusform",
            id: "servicedetails-dataupdatestatus-form",
            bodyPadding: "10 10 10 10",
            maxHeight: 680,
            height: Ext.Element.getViewportHeight() - 90,
            maxWidth: 1570,
            width: Ext.Element.getViewportWidth(),
            autoScroll: true,
            defaults: {
                defaultType: "textfield"
            },
            items: [{
                xtype: "fieldcontainer",
                id: "dataupdatestatustype",
                flex: 1,
                defaultType: "radio",
                layout: "hbox",
                items: [
                    {
                        checked: true,
                        id: "dataupdatestatusmanually",
                        boxLabel: "manuell",
                        margin: "0 0 0 20",
                        inputValue: "manuell",
                        name: "dataupdatestatus",
                        listeners: {
                            change: "onUpdateManually"
                        }
                    }, {
                        id: "dataupdatestatusautomatically",
                        boxLabel: "automatisch",
                        margin: "0 0 0 20",
                        inputValue: "automatisch",
                        name: "dataupdatestatus",
                        listeners: {
                            change: "onUpdateAutomatically"
                        }
                    }, {
                        id: "dataupdatestatusrealtime",
                        boxLabel: "echtzeit",
                        margin: "0 0 0 20",
                        inputValue: "echtzeit",
                        name: "dataupdatestatus",
                        listeners: {
                            change: "onUpdateRealtime"
                        }
                    }]
            }, {
                xtype: "fieldcontainer",
                id: "dataupdatestatuscontainer",
                layout: "hbox",
                labelWidth: 160,
                fieldLabel: "Datenaktualisierung",
                width: "100%",
                items: [
                    {
                        xtype: "numberfield",
                        fieldLabel: "alle",
                        id: "dataupdatestatusrefreshnumber",
                        minValue: 0,
                        step: 1,
                        labelWidth: 40,
                        width: 120
                    }, {
                        xtype: "combo",
                        store: ["Minuten", "Stunden", "Tag", "Woche", "Monate", "Jahre", "unregelmäßig"],
                        allowBlank: false,
                        id: "dataupdatestatusrefreshunit",
                        width: 160,
                        listeners: {
                            change: "onRefreshUnitChange"
                        }
                    }
                ]
            }, {
                xtype: "datefield",
                fieldLabel: "zuletzt aktualisiert am",
                id: "dataupdatestatuslastupdate",
                allowBlank: false,
                format: "Y-m-d",
                autoEl: {
                    tag: "div",
                    "data-qtip": "Manuelle Eingabe des Datum, an dem die Daten des Dienstes aktualisert wurden. Es wird automatisiert eine Benachrichtigungsmail verschickt."
                },
                labelWidth: 160,
                width: 360
            }, {
                xtype: "combo",
                fieldLabel: "Prozesstyp",
                store: ["ESRI Replikation", "FME Prozess", "batch Script", "Sonstiges"],
                allowBlank: false,
                id: "dataupdatestatusprocesstype",
                labelWidth: 160,
                width: 360
            }, {
                xtype: "textfield",
                fieldLabel: "Prozessname",
                id: "dataupdatestatusprocessname",
                labelWidth: 160,
                width: "100%"
            }, {
                xtype: "textfield",
                fieldLabel: "wo läuft der Prozess?",
                id: "dataupdatestatusprocesslocation",
                labelWidth: 160,
                width: "100%"
            }, {
                xtype: "textareafield",
                grow: false,
                fieldLabel: "Beschreibung",
                id: "dataupdatestatusprocessdescription",
                labelWidth: 160,
                width: "100%"
            }, {
                xtype: "button",
                id: "dataupdatestatussave",
                text: "Speichern",
                disabled: window.read_only,
                listeners: {
                    click: "onSaveDataUpdateStatusForm"
                }
            }
            ]
        }
    ]
});
