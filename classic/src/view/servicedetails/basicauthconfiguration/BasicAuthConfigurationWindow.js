Ext.define("DiensteManager.view.servicedetails.basicauthconfiguration.BasicAuthConfigurationWindow", {
    extend: "Ext.window.Window",
    id: "basicauthconfiguration-window",
    alias: "basicauthconfiguration.BasicAuthConfigurationWindow",
    controller: "basicauthconfiguration",
    padding: "5 0 0 0", // (top, right, bottom, left)
    layout: "fit",
    width: 550,
    height: 300,
    modal: true, // always on top
    title: "Basic Auth Konfiguration",
    listeners: {
        beforeRender: "beforeBasicAuthConfigurationWindowRender"
    },
    requires: [
        "Ext.selection.CellModel"
    ],
    tbar: [
        {
            text: "Benutzer hinzufügen",
            handler: "onAddUserRowClick"
        }
    ],
    items: [
        {
            xtype: "grid",
            id: "basicauthconfiguration-user-grid",
            autoScroll: true,
            selModel: {
                type: "cellmodel"
            },
            viewConfig: {
                enableTextSelection: true
            },
            plugins: {
                ptype: "cellediting",
                clicksToEdit: 1
            },
            columns: [
                {
                    text: "Benutzername",
                    dataIndex: "username",
                    flex: 0.5,
                    align: "left",
                    editor: {
                        allowBlank: false
                    }
                },
                {
                    text: "Passwort",
                    dataIndex: "password",
                    flex: 0.5,
                    align: "left",
                    editor: {
                        allowBlank: false
                    },
                    renderer: function (value) {
                        if (value !== "Neues Passwort") {
                            return "***";
                        }
                        else {
                            return value;
                        }
                    }
                },
                {
                    xtype: "actioncolumn",
                    width: 26,
                    sortable: false,
                    menuDisabled: true,
                    align: "center",
                    handler: "onDeleteUserRow",
                    items: [
                        {
                            icon: "resources/images/drop-no.png"
                        }
                    ]
                }
            ],
            store: Ext.create("Ext.data.Store", {
                fields: ["username", "password"],
                data: []
            })
        }
    ],
    buttons: [
        {
            xtype: "combobox",
            fieldLabel: "Berechtigung",
            id: "basicauthconfiguration-grant-rights",
            name: "grant_right",
            hidden: true,
            store: Ext.create("Ext.data.Store", {
                fields: ["abbr", "name"],
                data: [
                    {"abbr": "WFS:*", "name": "WFS"},
                    {"abbr": "WFS-T:*", "name": "WFS-T"}
                ]
            }),
            valueField: "abbr",
            displayField: "name",
            queryMode: "local",
            emptyText: "Berechtigungstyp auswählen..."
        },
        {
            text: "Konfiguration löschen",
            id: "deleteBasicAuthConfigurationButton",
            hidden: true,
            tooltip: "Die Basic Auth Konfiguration wird in der dee Datenbank gelöscht.",
            listeners: {
                click: "onDeleteBasicAuthConfiguration"
            }
        },
        {
            text: "Speichern",
            tooltip: "Die Basic Auth Konfiguration wird gespeichert.",
            listeners: {
                click: "onSaveBasicAuthConfiguration"
            }
        }
    ]
});
