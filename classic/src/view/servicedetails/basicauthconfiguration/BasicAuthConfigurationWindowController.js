Ext.define("DiensteManager.view.servicedetails.basicauthconfiguration.BasicAuthConfigurationWindowController", {
    extend: "Ext.app.ViewController",
    alias: "controller.basicauthconfiguration",

    beforeBasicAuthConfigurationWindowRender: function () {
        var service_type = Ext.getCmp("servicetype").getSubmitValue(),
            securityProxyConfigurationStore = Ext.data.StoreManager.lookup("SecurityProxyConfiguration"),
            securityProxyConfiguration,
            endpointNameIndex = securityProxyConfigurationStore.findExact("endpoint_name", Ext.getCmp("servicetitle").getSubmitValue());

        if (service_type === "WFS") {
            Ext.getCmp("basicauthconfiguration-grant-rights").setHidden(false);
        }

        if (endpointNameIndex !== -1) {
            securityProxyConfiguration = securityProxyConfigurationStore.getAt(endpointNameIndex).data;

            var grid = Ext.getCmp("basicauthconfiguration-user-grid"),
                userStore = grid.getStore();

            userStore.removeAll();

            securityProxyConfiguration.users.forEach(function (user) {
                userStore.add({"username": user.username, "password": user.password});
            });

            if (service_type === "WFS") {
                var store = Ext.getCmp("basicauthconfiguration-grant-rights").getStore();
                var i = store.findExact("abbr", securityProxyConfiguration.grant_right);

                Ext.getCmp("basicauthconfiguration-grant-rights").setSelection(store.getAt(i));
            }
            Ext.getCmp("deleteBasicAuthConfigurationButton").setHidden(false);
        }
    },

    onAddUserRowClick: function () {
        var grid = Ext.getCmp("basicauthconfiguration-user-grid"),
            userStore = grid.getStore();

        userStore.insert(0, {"username": "Neuer Benutzername", "password": "Neues Passwort"});
    },

    onRemoveUserRowClick: function (view, recIndex, cellIndex, item, e, record) {
        record.drop();
    },

    onDeleteUserRow: function (view, recIndex, cellIndex, item, e, record) {
        record.drop();
    },

    onSaveBasicAuthConfiguration: function () {
        var service_type = Ext.getCmp("servicetype").getSubmitValue(),
            me = this,
            grant_right = "WMS:*",
            controller = DiensteManager.app.getController("DiensteManager.controller.BasicAuthConfiguration"),
            layers = controller.getLayerObjects(service_type),
            userStore = Ext.getCmp("basicauthconfiguration-user-grid").getStore(),
            users = [],
            userRecords = (userStore.getData().getSource() || userStore.getData()).getRange();

        userRecords.forEach(function (user) {
            users.push({
                username: user.data.username.replace(/\s/g, ""),
                password: user.data.password.replace(/\s/g, ""),
                group_name: user.data.username + "_gruppe",
                role_name: user.data.username + "_rolle",
                email: "geobasisinfrastruktur@gv.hamburg.de",
                user_id: 0,
                group_id: 0,
                role_id: 0,
                password_type: "plain"
            });
        });
        if (service_type === "WFS") {
            grant_right = Ext.getCmp("basicauthconfiguration-grant-rights").getSubmitValue();
            layers = controller.getLayerObjects(service_type, grant_right);
            if (grant_right === "") {
                Ext.Msg.alert("Basic Auth Konfiguration", "Berechtigung auswählen!", Ext.emptyFn);
                return;
            }
        }

        var newConfiguration = {
            users: users,
            endpoint_name: Ext.getCmp("servicetitle").getSubmitValue(),
            proxy_url: Ext.getCmp("serviceurlint").getSubmitValue(),
            endpoint_url: Ext.getCmp("serviceurlsec").getSubmitValue(),
            url_int: Ext.getCmp("serviceurlint").getSubmitValue(),
            service_type: service_type,
            endpoint_id: 0,
            grant_right: grant_right,
            objects: Ext.util.JSON.encode(layers)
        };

        if (users.length > 0) {
            controller.usernameExists(users).then(function (existingUsernames) {
                var securityProxyConfigurationStore = Ext.data.StoreManager.lookup("SecurityProxyConfiguration"),
                    securityProxyConfiguration,
                    endpointNameIndex = securityProxyConfigurationStore.findExact("endpoint_name", Ext.getCmp("servicetitle").getSubmitValue());

                if (existingUsernames.length === 0) {
                    // Benutzernamen existieren noch nicht
                    if (endpointNameIndex !== -1) {
                        // Konfiguration bereits vorhanden, daher alte löschen und neue anlegen
                        securityProxyConfiguration = securityProxyConfigurationStore.getAt(endpointNameIndex).data;
                        controller.deleteConfiguration(securityProxyConfiguration).then(function () {
                            me.addBasicAuthConfiguration(newConfiguration).then(function () {
                                Ext.Msg.alert("Basic Auth Konfiguration", "Basic Auth erfolgreich konfiguriert.", Ext.emptyFn);
                                me.getBasicAuthConfiguration().then(function () {
                                    Ext.getCmp("basicauthconfiguration-window").close();
                                });
                            }).catch(function (error) {
                                console.log(error);
                            });
                        });
                    }
                    else {
                        // Konfiguration nicht vorhanden, daher neue anlegen
                        me.addBasicAuthConfiguration(newConfiguration).then(function () {
                            me.getBasicAuthConfiguration().then(function () {
                                Ext.getCmp("basicauthconfiguration-window").close();
                            });
                            Ext.Msg.alert("Basic Auth Konfiguration", "Basic Auth erfolgreich konfiguriert.", Ext.emptyFn);
                        });
                    }
                }
                else {
                    // Benutzernamen existieren bereits
                    if (endpointNameIndex !== -1) {
                        securityProxyConfiguration = securityProxyConfigurationStore.getAt(endpointNameIndex).data;
                        var checkedUsernames = controller.checkUsernames(existingUsernames, securityProxyConfiguration, users);

                        if (checkedUsernames.length === 0) {
                            // prüfen, ob sich Konfiguration irgendwo geändert hat
                            if (controller.configurationChanged(newConfiguration, securityProxyConfiguration)) {
                                controller.deleteConfiguration(securityProxyConfiguration).then(function () {
                                    me.addBasicAuthConfiguration(newConfiguration).then(function () {
                                        me.getBasicAuthConfiguration().then(function () {
                                            Ext.getCmp("basicauthconfiguration-window").close();
                                        });
                                        Ext.Msg.alert("Basic Auth Konfiguration", "Basic Auth erfolgreich konfiguriert.", Ext.emptyFn);
                                    }).catch(function (error) {
                                        console.log(error);
                                    });
                                });
                            }
                            else {
                                Ext.Msg.alert("Basic Auth Konfiguration", "Keine Änderung der Konfiguration festgestellt.", Ext.emptyFn);
                            }
                        }
                        else {
                            if (checkedUsernames.length > 1) {
                                Ext.Msg.alert("Basic Auth Konfiguration", "Diese Nutzernamen existieren bereits: " + checkedUsernames.join() + ". Speichern nicht möglich!", Ext.emptyFn);
                            }
                            else {
                                Ext.Msg.alert("Basic Auth Konfiguration", "Dieser Nutzername existiert bereits: " + checkedUsernames[0] + ". Speichern nicht möglich!", Ext.emptyFn);
                            }
                        }
                    }
                    else {
                        // noch keine Konfiguration vorhanden, aber mind. 1 Benutzername existiert schon
                        if (existingUsernames.length > 1) {
                            Ext.Msg.alert("Basic Auth Konfiguration", "Diese Nutzernamen existieren bereits: " + existingUsernames.join() + ". Speichern nicht möglich!", Ext.emptyFn);
                        }
                        else {
                            Ext.Msg.alert("Basic Auth Konfiguration", "Dieser Nutzername existiert bereits: " + existingUsernames[0] + ". Speichern nicht möglich!", Ext.emptyFn);
                        }
                    }
                }
            });
        }
        else {
            Ext.Msg.alert("Basic Auth Konfiguration", "Mindestens einen Benutzernamen mit Passwort erstellen!", Ext.emptyFn);
        }
    },

    onDeleteBasicAuthConfiguration: function () {
        var securityProxyConfigurationStore = Ext.data.StoreManager.lookup("SecurityProxyConfiguration"),
            securityProxyConfiguration,
            endpointNameIndex = securityProxyConfigurationStore.findExact("endpoint_name", Ext.getCmp("servicetitle").getSubmitValue());

        if (endpointNameIndex !== -1) {
            var controller = DiensteManager.app.getController("DiensteManager.controller.BasicAuthConfiguration");

            securityProxyConfiguration = securityProxyConfigurationStore.getAt(endpointNameIndex).data;
            if (securityProxyConfiguration.hasOwnProperty("endpoint_name")) {
                controller.deleteConfiguration(securityProxyConfiguration).then(function () {
                    Ext.getCmp("basicauthconfiguration-user-grid").getStore().removeAll();
                    securityProxyConfigurationStore.removeAt(endpointNameIndex);
                    Ext.getCmp("basicauthconfiguration-window").close();
                    Ext.Msg.alert("Konfiguration gelöscht", "Die Basic Auth Konfiguration wurde erfolgreich gelöscht.", Ext.emptyFn);
                });
            }
        }
    },

    addBasicAuthConfiguration: function (securityProxyConfiguration) {
        var controller = DiensteManager.app.getController("DiensteManager.controller.BasicAuthConfiguration");

        return controller.addBasicAuthConfiguration(securityProxyConfiguration);
    },

    getBasicAuthConfiguration: function () {
        var controller = DiensteManager.app.getController("DiensteManager.controller.BasicAuthConfiguration"),
            service_id = Ext.getCmp("serviceid").getSubmitValue(),
            endpoint_name = Ext.getCmp("servicetitle").getSubmitValue();

        return controller.getBasicAuthConfiguration(service_id, endpoint_name);
    }
});
