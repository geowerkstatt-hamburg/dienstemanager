Ext.define("DiensteManager.view.servicedetails.ServiceDetails_LayerGetMapController", {
    extend: "Ext.app.ViewController",
    alias: "controller.serviceDetails_LayerGetMapController",

    onChangeGetMapBbox: function () {
        if (this.getViewModel().data.layerData) {

            var selectedLayer = this.getViewModel().data.layerData.data;

            var req = this.getViewModel().data.req,
                getmapbbox_name = Ext.getCmp("bbox_getMap").getValue(),
                getmapbboxvalue = DiensteManager.Config.getBboxGetmap_extent()[getmapbbox_name][0];

            var allLayers = "";

            Ext.getStore("Layers").each(function (layer) {
                allLayers += layer.get("layer_name") + ",";
            });

            allLayers = allLayers.substr(0, allLayers.length - 1);

            var serviceStore = Ext.data.StoreManager.lookup("ServiceList");
            var serviceData;
            var i = -1;

            if (selectedLayer.service_title) {
                i = serviceStore.findExact("title", selectedLayer.service_title);
            }
            else if (selectedLayer.service_id) {
                i = serviceStore.findExact("service_id", selectedLayer.service_id);
            }
            if (i !== -1) {
                serviceData = serviceStore.getAt(i).data;
            }

            var proj = "CRS";

            if (serviceData.version === "1.1.1") {
                proj = "SRS";
            }

            var url = "";

            if (req === "getMap - intern") {
                url = serviceData.url_int + "?SERVICE=WMS&VERSION=" + serviceData.version + "&REQUEST=GetMap&FORMAT=image/png&BBOX=" + getmapbboxvalue + "&WIDTH=256&HEIGHT=256&STYLES=&" + proj + "=EPSG:" + DiensteManager.Config.getSrsDienste() + "&LAYERS=" + selectedLayer.layer_name;
            }
            else if (req === "getMap alle Layer - intern") {
                url = serviceData.url_int + "?SERVICE=WMS&VERSION=" + serviceData.version + "&REQUEST=GetMap&FORMAT=image/png&BBOX=" + getmapbboxvalue + "&WIDTH=256&HEIGHT=256&STYLES=&" + proj + "=EPSG:" + DiensteManager.Config.getSrsDienste() + "&LAYERS=" + allLayers;
            }
            else if (req === "getMap - extern") {
                url = serviceData.url_ext + "?SERVICE=WMS&VERSION=" + serviceData.version + "&REQUEST=GetMap&FORMAT=image/png&BBOX=" + getmapbboxvalue + "&WIDTH=256&HEIGHT=256&STYLES=&" + proj + "=EPSG:" + DiensteManager.Config.getSrsDienste() + "&LAYERS=" + selectedLayer.layer_name;
            }
            else if (req === "getMap alle Layer - extern") {
                url = serviceData.url_ext + "?SERVICE=WMS&VERSION=" + serviceData.version + "&REQUEST=GetMap&FORMAT=image/png&BBOX=" + getmapbboxvalue + "&WIDTH=256&HEIGHT=256&STYLES=&" + proj + "=EPSG:" + DiensteManager.Config.getSrsDienste() + "&LAYERS=" + allLayers;
            }

            var getMapRequestWindow = Ext.getCmp("layerrequestgetmap-window");

            getMapRequestWindow.int_ext_url = url;

            if (Ext.getCmp("bbox_getMap").getValue() !== null) {
                Ext.getCmp("getMapRequest").setValue(url);
            }
            else {
                Ext.getCmp("bbox_getMap_ok").disable();
                Ext.getCmp("bbox_getMap_cancel").disable();
            }
        }
    },

    onRequestgetMapBbox: function () {
        var getMapRequestWindow = Ext.getCmp("layerrequestgetmap-window");

        window.open(getMapRequestWindow.int_ext_url, "_blank");
    },

    onCancelgetMapBbox: function () {
        Ext.getCmp("layerrequestgetmap-window").close();
        return false;
    }
});
