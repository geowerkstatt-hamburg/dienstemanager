Ext.define("DiensteManager.view.servicedetails.jira.JiraTicketWindowController", {
    extend: "Ext.app.ViewController",
    alias: "controller.jiraticket-controller",

    onAddIssue: function (grid, rowIndex) {
        var record = grid.getStore().getAt(rowIndex),
            service_id = Ext.getCmp("serviceid").getSubmitValue();

        Ext.Ajax.request({
            url: "backend/addServiceIssue",
            method: "POST",
            jsonData: {
                id: record.data.id,
                service_id: service_id,
                key: record.data.key,
                creator: record.data.creator,
                summary: record.data.summary,
                assignee: record.data.assignee,
                project: record.data.project
            },
            success: function (response) {
                if (response.responseText === "") {
                    Ext.Msg.alert("Jira Ticket", "Fehler: Jira Ticket bereits hinzugefügt?", Ext.emptyFn);
                }
                else {
                    Ext.getCmp("jiraticket-grid").getStore().remove(record);
                    Ext.Msg.alert("Jira Ticket", "Das Jira Ticket wurde erfolgreich hinzugefügt.", Ext.emptyFn);
                    Ext.getStore("JiraTickets").reload({
                        params: {service_id: service_id}
                    });
                }
            },
            failure: function (response) {
                Ext.Msg.alert("Jira Ticket", "Leider ist ein Fehler aufgetreten: " + response.responseText, Ext.emptyFn);
                console.log(Ext.decode(response.responseText));
            }
        });
    }

});
