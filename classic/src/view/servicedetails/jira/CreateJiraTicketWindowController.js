Ext.define("DiensteManager.view.servicedetails.jira.CreateJiraTicketWindowController", {
    extend: "Ext.app.ViewController",
    alias: "controller.createjiraticket-controller",

    onCreateJiraTicket: function () {
        var service_id = Ext.getCmp("serviceid").getSubmitValue(),
            datasource = Ext.getCmp("servicedatasource").getSubmitValue(),
            workspace = Ext.getCmp("servicefolder").getSubmitValue(),
            servicename = Ext.getCmp("servicename").getSubmitValue(),
            responsible_party = Ext.getCmp("serviceresponsibleparty").getSubmitValue(),
            linkedticket = Ext.getCmp("createjiraticketlinkedticket").getSubmitValue(),
            tickettype = Ext.getCmp("createjiratickettype").getSubmitValue(),
            duedate = Ext.getCmp("createjiraticketduedate").getSubmitValue(),
            description = Ext.getCmp("createjiraticketdescription").getValue(),
            update_components = Ext.getCmp("createjiraticketcomponents").getValue(),
            fme_component_activate = Ext.getCmp("createjiraticketfmecomponent").getValue(),
            fme_processname = Ext.getCmp("dataupdatestatusprocessname").getValue(),
            fme_processlocation = Ext.getCmp("dataupdatestatusprocesslocation").getValue(),
            jiraproject_records = Ext.getCmp("createjiraticketproject").getSelection(),
            jiraproject_name_to_write = jiraproject_records.data.name,
            issuetype = "",
            epic_link_customfield_id = "",
            epic_link_key = "",
            story_points_customfield_id = "",
            story_points_value,
            project_components,
            servicetype_checkbox = Ext.getCmp("createjiraticketservicetype").getValue(),
            servicetype = Ext.getCmp("servicetype").getSubmitValue(),
            security_setting = Ext.getCmp("createjiraticketsecurity").getSubmitValue(),
            activate_epic_link,
            jiraproject_label = [Ext.getCmp("createjiraticketlabel").getSubmitValue()],
            update_components_values = [];

        if (jiraproject_records.data.new_as_subtask === true) {
            issuetype = "Sub-task";
            story_points_customfield_id = jiraproject_records.data.story_points_customfield_id;
            story_points_value = jiraproject_records.data.story_points_value;
            project_components = jiraproject_records.data.components;
        }
        else {
            issuetype = "Task";
            epic_link_customfield_id = jiraproject_records.data.epic_link_customfield_id;
            epic_link_key = jiraproject_records.data.epic_link_key;
            story_points_customfield_id = jiraproject_records.data.story_points_customfield_id;
            story_points_value = jiraproject_records.data.story_points_value;
            project_components = jiraproject_records.data.components;
        }

        Object.values(update_components).forEach(array => {
            update_components_values = update_components_values.concat(array);
        });

        if (jiraproject_records.data.active_epic_link === true) {
            activate_epic_link = true;
        }
        else {
            activate_epic_link = false;
        }

        if (tickettype.length === 0 || duedate.length === 0) {
            Ext.Msg.alert("Hinweis", "Bitte Felder füllen.");
        }
        else if (tickettype === "Dienst erstellen" && Ext.getCmp("createjiraticketfmecomponent").allowBlank === false) {
            Ext.Msg.alert("Hinweis", "Bitte einen der Radio Buttons bei 'FME Komponenten aktivieren' auswählen.");
        }
        else if (tickettype === "Dienst erstellen" && (!fme_processname || !fme_processlocation) && fme_component_activate.fmecomponent !== "Keine") {
            Ext.Msg.alert("Hinweis", "Felder 'Prozessname' und 'wo läuft der Prozess?' im Reiter 'Datenaktualität' sind nicht gefüllt. Bitte beide Felder füllen.");
        }
        else if (tickettype === "Dienst aktualisieren" && (!fme_processname || !fme_processlocation) && (update_components_values.includes("FME Schedule") || update_components_values.includes("FME Automation"))) {
            Ext.Msg.alert("Hinweis", "Felder 'Prozessname' und 'wo läuft der Prozess?' im Reiter 'Datenaktualität' sind nicht gefüllt. Bitte beide Felder füllen.");
        }
        else if (tickettype === "Dienst aktualisieren" && (update_components_values.includes("FME Schedule") && update_components_values.includes("FME Automation"))) {
            Ext.Msg.alert("Hinweis", "Bitte nur eine der beiden oder keine FME Checkboxen auswählen!");
        }
        else {
            Ext.Ajax.request({
                url: "backend/createjiraticket",
                method: "POST",
                jsonData: {
                    tickettype: tickettype,
                    service_id: service_id,
                    duedate: duedate,
                    description: description,
                    user: window.auth_user,
                    datasource: datasource,
                    workspace: workspace,
                    servicename: servicename,
                    responsible_party: responsible_party,
                    linkedticket: linkedticket,
                    update_components: update_components,
                    fme_component_activate: fme_component_activate.fmecomponent,
                    fme_processname: fme_processname,
                    fme_processlocation: fme_processlocation,
                    jiraproject_name_to_write: jiraproject_name_to_write,
                    issuetype: issuetype,
                    epic_link_customfield_id: epic_link_customfield_id,
                    epic_link_key: epic_link_key,
                    story_points_customfield_id: story_points_customfield_id,
                    story_points_value: story_points_value,
                    project_components: project_components,
                    servicetype_checkbox: servicetype_checkbox,
                    servicetype: servicetype,
                    security_setting: security_setting,
                    activate_epic_link: activate_epic_link,
                    jiraproject_label: jiraproject_label
                },
                success: function (response) {
                    var jsonResponseText = JSON.parse(response.responseText);

                    if (jsonResponseText.error === true) {
                        if (jsonResponseText.errorMessages && jsonResponseText.errorMessages.length > 0) {
                            Ext.Msg.alert("Jira Ticket", "Jira Ticket konnte nicht erzeugt werden. Fehler: " + jsonResponseText.errorMessages[0], Ext.emptyFn);
                        }
                        else if (jsonResponseText.errors) {
                            Ext.Msg.alert("Jira Ticket", "Jira Ticket konnte nicht erzeugt werden. Fehler: " + JSON.stringify(jsonResponseText.errors), Ext.emptyFn);
                        }
                        else {
                            Ext.Msg.alert("Jira Ticket", "Jira Ticket konnte nicht erzeugt werden.", Ext.emptyFn);
                        }
                    }
                    else {
                        Ext.getCmp("createjiraticket-window").close();
                        Ext.Msg.alert("Jira Ticket", "Das Jira Ticket wurde erfolgreich angelegt.", Ext.emptyFn);
                        Ext.getStore("JiraTickets").reload({
                            params: {service_id: service_id}
                        });
                    }
                },
                failure: function (response) {
                    Ext.Msg.alert("Jira Ticket", "Leider ist ein Fehler aufgetreten: " + response.responseText, Ext.emptyFn);
                    console.log(Ext.decode(response.responseText));
                }
            });
        }
    },

    collapseLinkedTicket: function () {
        Ext.getCmp("createjiraticketlinkedticket").collapse();
    },

    linkedticketShowHide: function () {
        var jiraproject_records = Ext.getCmp("createjiraticketproject").getSelection(),
            linkedticket = Ext.getCmp("createjiraticketlinkedticket"),
            linkedticket_element = Ext.getCmp("createjiraticketlinkedticket").getEl(),
            jiraproject_labels_element = Ext.getCmp("createjiraticketlabel").getEl(),
            service_id = Ext.getCmp("serviceid").getValue();

        linkedticket.setValue("");

        if (jiraproject_records.data.new_as_subtask) {
            linkedticket_element.show();
            jiraproject_labels_element.show();

            Ext.getStore("JiraLinkedTickets").load({
                params: {
                    service_id: service_id,
                    project: jiraproject_records.data.name
                }
            });
        }
        else {
            linkedticket.setValue("");
            linkedticket_element.hide();
        }
    },

    tickettypeShowHideElements: function () {
        var tickettype = Ext.getCmp("createjiratickettype").getValue(),
            servicetype = Ext.getCmp("servicetype").getSubmitValue(),
            components_to_update_element = Ext.getCmp("createjiraticketcomponents").getEl(),
            fme_component_element = Ext.getCmp("createjiraticketfmecomponent").getEl(),
            servicetype_element = Ext.getCmp("createjiraticketservicetype").getEl(),
            wms_checkbox = Ext.getCmp("createjiraticketservicetype").getComponent("wms_createjiraticketservicetype"),
            wfs_checkbox = Ext.getCmp("createjiraticketservicetype").getComponent("wfs_createjiraticketservicetype"),
            security_element = Ext.getCmp("createjiraticketsecurity").getEl();

        function _clearCheckboxesRadioButtons () {
            var radio_button_scheduler = Ext.getCmp("createjiraticketfmecomponent").getComponent("createjiraticketfmescheduler"),
                radio_button_automation = Ext.getCmp("createjiraticketfmecomponent").getComponent("createjiraticketfmeautomation"),
                radio_button_nocomponent = Ext.getCmp("createjiraticketfmecomponent").getComponent("createjiraticketnofme"),
                checkboxes_components_to_update = Ext.getCmp("createjiraticketcomponents");

            radio_button_scheduler.setValue(false);
            radio_button_automation.setValue(false);
            radio_button_nocomponent.setValue(false);
            checkboxes_components_to_update.setValue(false);
        }

        if (servicetype === "WMS") {
            servicetype_element.show();
            wms_checkbox.setValue(true);
            wfs_checkbox.setValue(false);
        }
        else if (servicetype === "WFS") {
            servicetype_element.show();
            wms_checkbox.setValue(false);
            wfs_checkbox.setValue(true);
        }
        else {
            servicetype_element.hide();
            wms_checkbox.setValue(false);
            wfs_checkbox.setValue(false);
        }

        if (tickettype === "Dienst aktualisieren") {
            components_to_update_element.show();
            fme_component_element.hide();
            security_element.show();
            Ext.getCmp("createjiraticketsecurity").setFieldLabel("Absicherung anpassen");
            _clearCheckboxesRadioButtons();
        }
        else if (tickettype === "Dienst erstellen") {
            components_to_update_element.hide();
            fme_component_element.show();
            security_element.show();
            Ext.getCmp("createjiraticketsecurity").setFieldLabel("Absicherung einrichten");
            _clearCheckboxesRadioButtons();
        }
        else if (tickettype === "Dienst löschen") {
            components_to_update_element.hide();
            fme_component_element.hide();
            security_element.hide();
            _clearCheckboxesRadioButtons();
        }
    },

    adddatabasealert: function () {
        var update_components = Ext.getCmp("createjiraticketcomponents").getValue();

        if (update_components.data === "Database") {
            Ext.getCmp("createjiraticketdescription").setValue("Bitte hier angeben, falls nur Teile der Datenbank kopiert werden sollen!");
            Ext.getCmp("createjiraticketdescription").setFieldStyle({
                "color": "#e82323",
                "font-weight": "bold"
            });
        }
        else if (update_components.data !== "Database") {
            Ext.getCmp("createjiraticketdescription").emptyText = ["Anmerkungen"];
            Ext.getCmp("createjiraticketdescription").reset();
            Ext.getCmp("createjiraticketdescription").setFieldStyle({
                "color": "#404040",
                "font-weight": "normal"
            });
        }
    },

    settextstyle: function () {
        var textarea_value = Ext.getCmp("createjiraticketdescription").getValue();

        if (textarea_value === "" || textarea_value) {
            Ext.getCmp("createjiraticketdescription").setFieldStyle({
                "color": "#404040",
                "font-weight": "normal"
            });
        }
    }

});
