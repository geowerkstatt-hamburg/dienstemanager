Ext.define("DiensteManager.view.servicedetails.jira.JiraTicketWindow", {
    extend: "Ext.window.Window",
    id: "jiraticket-window",
    alias: "jira.JiraTicketWindow",
    controller: "jiraticket-controller",
    padding: "5 0 0 0", // (top, right, bottom, left)
    layout: "fit",
    width: 700,
    height: 400,
    modal: true,
    title: "Jira Tickets hinzufügen",
    requires: [
        "Ext.selection.CellModel"
    ],
    items: [
        {
            xtype: "grid",
            id: "jiraticket-grid",
            autoScroll: true,
            columns: [
                {
                    text: "Key",
                    dataIndex: "key",
                    width: 90,
                    align: "left"
                }, {
                    text: "Autor",
                    dataIndex: "creator",
                    width: 140,
                    align: "left"
                }, {
                    text: "Bearbeiter",
                    dataIndex: "assignee",
                    width: 140,
                    align: "left"
                }, {
                    text: "Zusammenfassung",
                    dataIndex: "summary",
                    width: 200,
                    align: "left"
                }, {
                    text: "URL",
                    width: 85,
                    align: "left",
                    renderer: function (value, metadata, record) {
                        return "<a href=\"" + DiensteManager.Config.getJiraBrowseUrl() + "/" + record.get("key") + "\" target=\"_blank\">Jira Link</a>";
                    }
                }, {
                    xtype: "actioncolumn",
                    width: 24,
                    sortable: false,
                    menuDisabled: true,
                    align: "center",
                    handler: "onAddIssue",
                    items: [{
                        iconCls: "x-fa fa-plus-circle",
                        tooltip: "Jira Ticket hinzufügen"
                    }]
                }
            ],
            store: Ext.create("Ext.data.Store", {
                fields: [],
                data: []
            })
        }
    ]
});
