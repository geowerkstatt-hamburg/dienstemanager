Ext.define("DiensteManager.view.servicedetails.ServiceDetails_Stats", {
    extend: "Ext.panel.Panel",
    xtype: "servicedetails-stats",
    controller: "servicedetails_stats",
    bodyPadding: "5 5 5 5",
    maxHeight: 677,
    height: Ext.Element.getViewportHeight() - 90,
    items: [
        {
            xtype: "cartesian",
            id: "serviceVisitsChart",
            reference: "chart",
            width: "100%",
            maxHeight: 620,
            height: Ext.Element.getViewportHeight() - 190,
            animation: {
                duration: 200
            },
            store: "Visits",
            insetPadding: 40,
            innerPadding: {
                left: 40,
                right: 40
            },
            legend: {
                type: "sprite",
                docked: "right"
            },
            sprites: [],
            axes: [
                {
                    type: "numeric",
                    position: "left",
                    grid: true,
                    minimum: 0
                }, {
                    type: "category",
                    position: "bottom",
                    grid: true,
                    label: {
                        rotate: {
                            degrees: -45
                        }
                    }
                }
            ],
            series: [{
                type: "bar",
                title: ["Internet", "Intranet"],
                xField: "month",
                yField: ["visits_internet", "visits_intranet"],
                stacked: true,
                style: {
                    opacity: 0.80,
                    maxBarWidth: 30
                },
                highlight: {
                    fillStyle: "yellow"
                },
                tooltip: {
                    renderer: "onBarTooltipRender"
                }
            }]
        }
    ],
    dockedItems: [
        {
            xtype: "toolbar",
            id: "visitsStatsToolbar",
            dock: "top",
            items: [
                {
                    id: "savevvisitsstats",
                    text: "Grafik speichern",
                    handler: "onSaveVisitsStats"
                }
            ]
        }
    ]
});
