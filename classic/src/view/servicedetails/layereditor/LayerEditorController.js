Ext.define("DiensteManager.view.servicedetails.layereditor.LayerEditorController", {
    extend: "Ext.app.ViewController",
    alias: "controller.layereditor",

    onAddRow: function () {
        var rec = new DiensteManager.model.GFIConfig({
                attr_name: "",
                map_name: ""
            }),
            count = Ext.getStore("Layers").count();

        Ext.getStore("Layers").insert(count, rec);
        Ext.getCmp("layereditor-grid").findPlugin("cellediting").startEdit(rec, 0);
    },

    onDeleteRow: function (view, recIndex, cellIndex, item, e, record) {
        record.drop();

        if (record.data.layer_id !== null && record.data.layer_id !== undefined) {
            var mb = Ext.MessageBox;

            mb.buttonText.yes = "ja";
            mb.buttonText.no = "nein";

            mb.confirm("Löschen", "Wirklich löschen?", function (btn) {
                if (btn === "yes") {
                    Ext.Ajax.request({
                        url: "backend/deletelayer",
                        method: "POST",
                        jsonData: {
                            layer_id: record.data.layer_id,
                            service_id: Ext.getCmp("serviceid").getSubmitValue(),
                            title: record.data.title,
                            status: Ext.getCmp("servicestatus").getSubmitValue(),
                            last_edited_by: window.auth_user,
                            last_edit_date: moment().format("YYYY-MM-DD HH:mm")
                        },
                        success: function () {
                            Ext.getStore("Layers").reload({
                                params: {service_id: Ext.getCmp("serviceid").getSubmitValue()}
                            });
                        },
                        failure: function (response) {
                            console.log(Ext.decode(response.responseText));
                        }
                    });
                }
            });
        }
    },

    onSaveLayer: function () {
        var store = Ext.getStore("Layers"),
            service_id = Ext.getCmp("serviceid").getSubmitValue(),
            service_name = Ext.getCmp("servicename").getSubmitValue(),
            service_type = Ext.getCmp("servicetype").getSubmitValue();

        store.each(function (record) {

            if (record.data.layer_id === undefined || record.data.layer_id === null) {
                Ext.Ajax.request({
                    url: "backend/addlayer",
                    method: "POST",
                    jsonData: {
                        service_id: service_id,
                        service_name: service_name,
                        service_type: service_type,
                        layer_name: record.data.layer_name,
                        title: record.data.title,
                        scale_min: "0",
                        scale_max: "1000000",
                        output_format: "text/html; charset=utf-8"
                    },
                    failure: function (response) {
                        console.log(Ext.decode(response.responseText));
                    }
                });
            }
            else {
                record.data.service_type = service_type;
                DiensteManager.app.getController("DiensteManager.controller.PublicFunctions").updateLayer(
                    service_id,
                    record.data.layer_id,
                    record.data,
                    true
                );
            }
        });
        store.reload({
            params: {service_id: service_id, task: "LISTING-LAYER"}
        });
    },

    onClosingWindow: function () {
        var store = Ext.getStore("Layers"),
            service_id = Ext.getCmp("serviceid").getSubmitValue();

        store.reload({
            params: {service_id: service_id, task: "LISTING-LAYER"}
        });
    }
});
