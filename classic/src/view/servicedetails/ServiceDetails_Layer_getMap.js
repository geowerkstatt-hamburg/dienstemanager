Ext.define("DiensteManager.view.servicedetails.ServiceDetails_Layer_getMap", {
    extend: "Ext.window.Window",
    xtype: "ServiceDetails_Layer_getMap",
    id: "layerrequestgetmap-window",
    alias: "servicedetails.ServiceDetails_Layer_getMap",
    height: 200,
    width: 600,
    title: "Auswahl Bounding Box für Testrequest (256x256)",
    bodyStyle: "margin: 10px;",
    controller: "serviceDetails_LayerGetMapController",
    int_ext_url: "int",
    initComponent: function () {
        this.items = [
            {
                xtype: "combo",
                fieldLabel: "Bounding Box",
                store: DiensteManager.Config.getBboxGetmap_name(),
                displayField: "name",
                allowBlank: false,
                id: "bbox_getMap",
                valueField: "bbox",
                editable: false,
                margin: "0 10 0 0",
                labelWidth: 100,
                width: 300,
                listeners: {
                    change: "onChangeGetMapBbox"
                }
            },
            {
                xtype: "textfield",
                id: "getMapRequest",
                fieldLabel: "get Map Aufruf",
                width: 500,
                allowBlank: false,
                style: "font-weight: bold; color: #003168;"
            }
        ];
        this.buttons = [
            {
                text: "Request ausführen",
                margin: "0 5 0 10",
                id: "bbox_getMap_ok",
                disabled: false,
                listeners: {
                    click: "onRequestgetMapBbox"
                }
            },
            {
                text: "Abbrechen",
                margin: "0 5 0 10",
                id: "bbox_getMap_cancel",
                disabled: false,
                listeners: {
                    click: "onCancelgetMapBbox"
                }
            }
        ];
        this.callParent(arguments);
    }
});
