Ext.define("DiensteManager.view.servicedetails.freigaben.LinkFreigabenController", {
    extend: "Ext.app.ViewController",
    alias: "controller.linkfreigaben",

    onSaveFreigabeLink: function () {
        var linkfreigaben = Ext.getCmp("linkfreigaben-grid");

        if (linkfreigaben.getSelectionModel().getSelection()[0] !== undefined) {
            var selectedFreigabe = linkfreigaben.getSelectionModel().getSelection()[0].data,
                service_id = Ext.getCmp("serviceid").getSubmitValue(),
                freigabe_id = selectedFreigabe.freigabe_id;

            Ext.Ajax.request({
                url: "backend/addfreigabelink",
                method: "POST",
                jsonData: {
                    service_id: service_id,
                    freigabe_id: freigabe_id,
                    last_edited_by: window.auth_user,
                    last_edit_date: moment().format("YYYY-MM-DD HH:mm")
                },
                success: function () {
                    Ext.getStore("LinkedFreigaben").reload({
                        params: {service_id: service_id}
                    });
                },
                failure: function (response) {
                    console.log(Ext.decode(response.responseText));
                }
            });
        }
    }
});
