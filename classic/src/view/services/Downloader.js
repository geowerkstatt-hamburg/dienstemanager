Ext.define("DiensteManager.view.services.Downloader", {
    extend: "Ext.window.Window",
    id: "downloader-window",
    alias: "services.Downloader",
    requires: [
        "Ext.window.*",
        "Ext.grid.*",
        "Ext.data.*",
        "Ext.util.*",
        "Ext.form.*"
    ],

    controller: "downloader",

    maxHeight: 765,
    height: Ext.Element.getViewportHeight(),
    width: 650,
    title: "WFS-Download",
    closeAction: "hide",

    wfsUrl: null,
    version: null,

    items: [{
        xtype: "fieldset",
        title: "FeatureTypes wählen",
        padding: "5 5 5 5",
        margin: "10 10 10 10",
        items: [{
            xtype: "grid",
            store: "Layers",
            name: "layersGrid",
            id: "downloader-grid",
            autoScroll: true,
            maxHeight: 400,
            height: Ext.Element.getViewportHeight() - 360,
            viewConfig: {
                enableTextSelection: true
            },
            columns: [
                {
                    header: "Name",
                    align: "left",
                    dataIndex: "layer_name",
                    flex: 1
                },
                {
                    header: "Titel",
                    align: "left",
                    dataIndex: "title",
                    flex: 2
                }
            ],
            columnLines: true,
            selModel: {
                type: "checkboxmodel"
            }
        }]
    },
    {
        xtype: "button",
        id: "estimateSize",
        margin: "5 10 5 10",
        listeners: {
            click: "onEstimateSize"
        },
        text: "Größe schätzen"
    },
    {
        xtype: "textfield",
        fieldLabel: "geschätzte Größe [MB]",
        name: "filesize",
        padding: "5 5 5 10",
        width: 400,
        readOnly: true,
        labelWidth: 160,
        id: "gml_filesize"
    },
    {
        xtype: "textfield",
        fieldLabel: "Dateiname",
        name: "download_filename",
        padding: "5 5 5 10",
        width: 400,
        labelWidth: 160,
        id: "download_filename"
    },
    {
        xtype: "button",
        id: "downloadFeatures",
        margin: "10 10 10 10",
        disabled: true,
        listeners: {
            click: "onDownload"
        },
        text: "Download"
    },
    {
        xtype: "label",
        id: "downloadLabel",
        forId: "downloadFeatures",
        text: "",
        margins: "0 0 0 10"
    }
    ],
    buttons: [{
        text: "Schliessen",
        handler: function () {
            Ext.getCmp("downloader-window").hide();
        }
    }],

    showDownloader: function (url, service_name, version) {
        this.version = version;
        this.wfsUrl = url;
        Ext.getCmp("download_filename").setValue(service_name);
        Ext.getCmp("downloadFeatures").setDisabled(true);
        Ext.getCmp("gml_filesize").setValue("");

        this.show();
    }
});
