Ext.define("DiensteManager.view.services.DownloaderController", {
    extend: "Ext.app.ViewController",
    alias: "controller.downloader",

    onDownload: function (btn) {
        var url = Ext.getCmp("downloader-window").wfsUrl,
            version = Ext.getCmp("downloader-window").version,
            grid = Ext.getCmp("downloader-grid"),
            srs = DiensteManager.Config.getSrsDienste(),
            selectedFT = grid.getSelectionModel().getSelection(),
            ft_list = "",
            loadingsMask = new Ext.LoadMask({
                msg: "Bitte warten...",
                target: Ext.getCmp("downloader-window")
            });

        if (selectedFT.length > 0) {
            loadingsMask.show();

            btn.setDisabled(true);

            for (var i in selectedFT) {
                ft_list += selectedFT[i].data.layer_name;

                if (i < selectedFT.length - 1) {
                    ft_list += ",";
                }
            }

            var gf_url = url + "?SERVICE=WFS&VERSION=" + version + "&REQUEST=GetFeature&srsName=EPSG:" + srs + "&typename=" + ft_list,
                filename = Ext.getCmp("download_filename").getValue(),
                date = new Date(),
                dd = date.getDate(),
                mm = date.getMonth() + 1,
                yyyy = date.getFullYear();

            if (dd < 10) {
                dd = "0" + dd;
            }
            if (mm < 10) {
                mm = "0" + mm;
            }
            var today = yyyy + "-" + mm + "-" + dd;

            Ext.Ajax.request({
                url: "backend/downloadgml",
                method: "POST",
                timeout: 1200000,
                jsonData: {
                    url: gf_url,
                    size: Ext.getCmp("gml_filesize").getSubmitValue(),
                    filename: filename + "_" + today
                },
                success: function (response) {
                    var response_json = Ext.decode(response.responseText);

                    btn.setDisabled(false);
                    loadingsMask.hide();

                    if (response_json.link_zip) {
                        var a = document.createElement("A");

                        a.href = response_json.link_zip;
                        a.download = response_json.link_zip.substr(response_json.link_zip.lastIndexOf("/") + 1);
                        document.body.appendChild(a);
                        a.click();
                        document.body.removeChild(a);
                    }
                    else {
                        var a_gml = document.createElement("A"),
                            a_xsd = document.createElement("A");

                        a_xsd.href = response_json.link_xsd;
                        a_xsd.download = response_json.link_xsd.substr(response_json.link_xsd.lastIndexOf("/") + 1);
                        document.body.appendChild(a_xsd);
                        a_xsd.click();
                        document.body.removeChild(a_xsd);
                        a_gml.href = response_json.link_gml;
                        a_gml.download = response_json.link_gml.substr(response_json.link_gml.lastIndexOf("/") + 1);
                        document.body.appendChild(a_gml);
                        a_gml.click();
                        document.body.removeChild(a_gml);
                    }
                },
                failure: function (response) {
                    btn.setDisabled(false);
                    loadingsMask.hide();
                    Ext.MessageBox.alert("Status", "Es ist ein Fehler aufgetreten!");
                    console.log(Ext.decode(response.responseText));
                }
            });
        }
    },

    onEstimateSize: function (btn) {
        var grid = Ext.getCmp("downloader-grid"),
            selectedFT = grid.getSelectionModel().getSelection(),
            version = Ext.getCmp("downloader-window").version,
            srs = DiensteManager.Config.getSrsDienste(),
            loadingsMask = new Ext.LoadMask({
                msg: "Bitte warten...",
                target: Ext.getCmp("downloader-window")
            }),
            selected_ft_names = [];

        if (selectedFT.length > 0) {
            btn.setDisabled(true);
            loadingsMask.show();
            for (var i = 0; i < selectedFT.length; i++) {
                selected_ft_names.push(selectedFT[i].data.layer_name);
            }

            Ext.Ajax.request({
                url: "backend/estimategmlsize",
                method: "POST",
                jsonData: {
                    selected_ft: selected_ft_names,
                    base_url: Ext.getCmp("downloader-window").wfsUrl + "?SERVICE=WFS&VERSION=" + version + "&REQUEST=GetFeature&srsName=EPSG:" + srs
                },
                success: function (response) {
                    var response_json = Ext.decode(response.responseText);

                    btn.setDisabled(false);
                    loadingsMask.hide();
                    Ext.getCmp("gml_filesize").setValue(response_json.estimated_size);
                    Ext.getCmp("downloadFeatures").setDisabled(false);
                },
                failure: function (response) {
                    btn.setDisabled(false);
                    loadingsMask.hide();
                    Ext.MessageBox.alert("Status", "Es ist ein Fehler aufgetreten!");
                    console.log(Ext.decode(response.responseText));
                }
            });
        }
    }
});
