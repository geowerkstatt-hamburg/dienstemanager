Ext.define("DiensteManager.view.contacts.ContactListController", {
    extend: "Ext.app.ViewController",

    alias: "controller.contact_list",

    init: function () {
        var contactDetailsButtons = Ext.getCmp("contactDetailsButtons");

        contactDetailsButtons.add({
            xtype: "button",
            id: "new_contact",
            margin: "0 20 5 0",
            tooltip: "Neuen Kontakt manuell eingeben",
            autoEl: {
                tag: "div",
                "data-qtip": "Neuen Kontakt manuell eingeben"
            },
            text: "Neu",
            listeners: {
                click: "onNewContact"
            }
        });

        if (DiensteManager.Config.getLdapModul()) {

            contactDetailsButtons.add({
                xtype: "button",
                id: "new_ad_contact",
                margin: "0 20 5 0",
                tooltip: "Neuen Kontakt aus LDAP importieren",
                autoEl: {
                    tag: "div",
                    "data-qtip": "Neuen Kontakt aus LDAP einfügen"
                },
                text: "Neu aus LDAP",
                listeners: {
                    click: "onNewADContact"
                }
            });
        }

        contactDetailsButtons.add({
            xtype: "button",
            id: "delete_contact",
            margin: "0 0 5 0",
            tooltip: "Kontakt löschen",
            autoEl: {
                tag: "div",
                "data-qtip": "Kontakt löschen"
            },
            text: "löschen",
            listeners: {
                click: "onDeleteContact"
            }
        });
    },

    onSelectionChange: function (selModel, records) {
        var rec = records[0];

        if (rec) {
            this.getView().getForm().loadRecord(rec);
        }
    },

    onSaveContact: function () {
        var selection = Ext.getCmp("contacts_grid").getSelectionModel().getSelection();

        if (selection.length === 1) {
            var data = selection[0].data;

            Ext.Ajax.request({
                url: "backend/updatecontact",
                method: "POST",
                jsonData: {
                    contact_id: data.contact_id,
                    given_name: data.given_name,
                    surname: data.surname,
                    name: data.surname + ", " + data.given_name,
                    company: data.company,
                    ad_account: data.ad_account,
                    email: data.email,
                    tel: data.tel
                },
                success: function () {
                    Ext.getStore("Contacts").reload();
                },
                failure: function (response) {
                    console.log(Ext.decode(response.responseText));
                }
            });
        }
        else if (selection.length === 0 && Ext.getCmp("contactgivenname").getSubmitValue() !== "" && Ext.getCmp("contactsurname").getSubmitValue() !== "") {
            var contact = Ext.getStore("Contacts").findRecord("email", Ext.getCmp("contactemail").getSubmitValue(), 0, false, false, true);

            if (!contact) {
                Ext.Ajax.request({
                    url: "backend/addcontact",
                    method: "POST",
                    jsonData: {
                        given_name: Ext.getCmp("contactgivenname").getSubmitValue(),
                        surname: Ext.getCmp("contactsurname").getSubmitValue(),
                        name: Ext.getCmp("contactgivenname").getSubmitValue() + ", " + Ext.getCmp("contactsurname").getSubmitValue(),
                        company: Ext.getCmp("contactcompany").getSubmitValue(),
                        ad_account: Ext.getCmp("contactadaccount").getSubmitValue(),
                        email: Ext.getCmp("contactemail").getSubmitValue(),
                        tel: Ext.getCmp("contacttel").getSubmitValue()
                    },
                    success: function () {
                        Ext.getStore("Contacts").reload();
                    },
                    failure: function (response) {
                        console.log(Ext.decode(response.responseText));
                    }
                });
            }
            else {
                Ext.MessageBox.alert("Achtung", "Kontakt schon vorhanden");
            }

        }
    },

    onNewContact: function () {
        Ext.getCmp("contacts_grid").getSelectionModel().deselectAll();
    },

    onNewADContact: function () {
        var newADContactWindow = Ext.getCmp("newadcontact-window");

        if (!newADContactWindow) {
            newADContactWindow = Ext.create("contacts.newadcontact");
        }
        newADContactWindow.show();
    },

    onDeleteContact: function () {
        var selection = Ext.getCmp("contacts_grid").getSelectionModel().getSelection();

        if (selection.length === 1) {
            var mb = Ext.MessageBox;

            mb.buttonText.yes = "ja";
            mb.buttonText.no = "nein";

            mb.confirm("Kontakt Löschen", "wirklich löschen?", function (btn) {
                if (btn === "yes") {
                    var data = selection[0].data;

                    Ext.Ajax.request({
                        url: "backend/deletecontact",
                        method: "POST",
                        jsonData: {
                            contact_id: data.contact_id
                        },
                        success: function () {
                            Ext.getStore("Contacts").reload();
                        },
                        failure: function (response) {
                            console.log(Ext.decode(response.responseText));
                        }
                    });
                }
            });
        }
    }
});
