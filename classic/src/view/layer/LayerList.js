Ext.define("DiensteManager.view.layer.LayerList", {
    extend: "Ext.grid.Panel",

    requires: [
        "Ext.grid.*",
        "Ext.data.*",
        "Ext.util.*",
        "Ext.form.*"
    ],

    id: "layer_list",

    store: "LayerList",

    controller: "layer_list",

    listeners: {
        celldblclick: "onLayerDblClick"
    },

    viewConfig: {
        enableTextSelection: true,
        preserveScrollOnRefresh: true,
        preserveScrollOnReload: true
    },

    headerBorders: false,
    rowLines: false,
    autoScroll: true,

    height: Ext.Element.getViewportHeight() - 70,

    columns: [
        {
            header: "Layer ID",
            dataIndex: "layer_id",
            align: "center",
            width: 80
        }, {
            header: "Layer Name",
            dataIndex: "layer_name",
            align: "left",
            flex: 1
        }, {
            header: "Layer Titel",
            dataIndex: "title",
            align: "left",
            flex: 1
        }, {
            header: "Dienst ID",
            dataIndex: "service_id",
            align: "center",
            width: 80
        }, {
            header: "Dienst Titel",
            dataIndex: "service_title",
            align: "left",
            flex: 1
        }, {
            xtype: "actioncolumn",
            header: "Test intern",
            width: 100,
            align: "center",
            items: [{
                icon: "resources/images/settings.png",
                tooltip: "Erzeugt einen internen Testrequest für diesen Layer",
                handler: "onTestInternClick"
            }]
        }, {
            xtype: "actioncolumn",
            header: "Test extern",
            width: 100,
            align: "center",
            items: [{
                icon: "resources/images/settings.png",
                tooltip: "Erzeugt einen externen Testrequest für diesen Layer",
                handler: "onTestExternClick"
            }]
        }, {
            xtype: "actioncolumn",
            header: "In Karte",
            name: "aktionen",
            align: "center",
            width: 100,
            renderer: function (value, metadata, record) {
                if (record.get("service_type") === "WMS") {
                    this.items[0].icon = "resources/images/map.png";
                    this.items[0].tooltip = "Der Layer wird im Vorschauportal angezeigt";
                    this.items[0].handler = "onShowLayerClick";
                }
                else {
                    this.items[0].icon = "";
                    this.items[0].tooltip = "";
                }
            },
            items: [
                {
                    icon: "resources/images/map.png",
                    tooltip: "Der Layer wird im Vorschauportal angezeigt",
                    handler: "onShowLayerClick"
                }
            ]
        }
    ]
});
