Ext.define("DiensteManager.view.portals.PortalListController", {
    extend: "Ext.app.ViewController",
    alias: "controller.portal_list",

    onSelectPortal: function (view, record) {
        var selectedLayer = record.data;

        Ext.getStore("PortalLayerList").load({
            params: {PortalUrl: selectedLayer.url}
        });
    },

    onPortalLayerDblClick: function (view, td, cellIndex, record) {
        var data = record.data,
            service = Ext.getStore("ServiceList").query("service_id", data.service_id, false, false, true).items[0].data;

        DiensteManager.app.getController("DiensteManager.controller.PublicFunctions").openServiceDetailsWindow(service, data.layer_id);
    }
});
