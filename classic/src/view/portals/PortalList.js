Ext.define("DiensteManager.view.portals.PortalList", {
    extend: "Ext.panel.Panel",

    requires: [
        "Ext.grid.*",
        "Ext.data.*",
        "Ext.util.*",
        "Ext.form.*"
    ],

    controller: "portal_list",

    layout: {
        type: "hbox",
        align: "stretch"
    },
    viewConfig: {
        enableTextSelection: true,
        preserveScrollOnRefresh: true,
        preserveScrollOnReload: true
    },

    headerBorders: false,
    rowLines: false,
    autoScroll: true,

    height: Ext.Element.getViewportHeight() - 70,

    items: [{
        title: "Portale",
        xtype: "grid",
        store: "PortalList",
        flex: 1,
        listeners: {
            select: "onSelectPortal"
        },
        columns: [{
            text: "Titel",
            dataIndex: "title",
            align: "left",
            flex: 1
        }, {
            text: "Link",
            dataIndex: "url",
            align: "left",
            width: 120,
            renderer: function (value) {
                if (value !== "") {
                    return "<a href=\"" + value + "\" target=\"_blank\">Portal öffnen</a>";
                }
                else {
                    return "";
                }
            }
        }]
    }, {
        title: "Enthaltene Layer",
        xtype: "grid",
        store: "PortalLayerList",
        flex: 1,
        margin: "0 0 0 10",
        listeners: {
            celldblclick: "onPortalLayerDblClick"
        },
        columns: [{
            text: "Layer-ID",
            dataIndex: "layer_id",
            align: "left"
        }, {
            text: "Layer-Name",
            dataIndex: "layer_name",
            align: "left",
            flex: 1
        }, {
            text: "Layer-Titel",
            dataIndex: "title",
            align: "left",
            flex: 1
        }],
        viewConfig: {
            emptyText: "Kein Portal ausgewählt",
            deferEmptyText: false
        }
    }]
});
