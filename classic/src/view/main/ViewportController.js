Ext.define("DiensteManager.view.main.ViewportController", {
    extend: "Ext.app.ViewController",
    alias: "controller.mainviewport",

    requires: [
        "Ext.list.Tree",
        "Ext.form.*",
        "Ext.LoadMask"
    ],

    id: "viewport-controller",

    route_id: "",

    params: null,

    listen: {
        controller: {
            "#": {
                unmatchedroute: "onRouteChange"
            }
        }
    },

    routes: {
        ":node": "onRouteChange"
    },

    setCurrentView: function (hashTag) {
        var hashTag_lc = (hashTag || "").toLowerCase(),
            me = this,
            refs = me.getReferences(),
            mainCard = refs.mainCardPanel,
            mainLayout = mainCard.getLayout(),
            navigationList = refs.navigationTreeList,
            store = navigationList.getStore(),
            node = store.findNode("routeId", hashTag_lc),
            view = node ? node.get("view") : null,
            existingItem = mainCard.child("component[routeId=" + hashTag_lc + "]"),
            newView,
            lastView = mainLayout.getActiveItem();

        if (!existingItem) {
            newView = Ext.create("DiensteManager.view." + (view || "pages.Error404Window"), {
                hideMode: "offsets",
                routeId: hashTag_lc
            });
        }

        if (!newView || !newView.isWindow) {
            if (existingItem) {
                if (existingItem !== lastView) {
                    mainLayout.setActiveItem(existingItem);
                }
                newView = existingItem;
            }
            else {
                Ext.suspendLayouts();
                mainLayout.setActiveItem(mainCard.add(newView));
                Ext.resumeLayouts(true);
            }
        }

        navigationList.setSelection(node);

        if (newView.isFocusable(true)) {
            newView.focus();
        }
    },

    onNavigationTreeSelectionChange: function (tree, node) {
        if (node && node.get("view")) {
            this.redirectTo(node.get("routeId"));
        }
    },

    onToggleNavigationSize: function () {
        var me = this,
            refs = me.getReferences(),
            navigationList = refs.navigationTreeList,
            wrapContainer = refs.mainContainerWrap,
            collapsing = !navigationList.getMicro(),
            new_width = collapsing ? 64 : 210;

        if (Ext.isIE9m || !Ext.os.is.Desktop) {
            Ext.suspendLayouts();

            refs.dmLogo.setWidth(new_width);

            navigationList.setWidth(new_width);
            navigationList.setMicro(collapsing);

            Ext.resumeLayouts();

            wrapContainer.layout.animatePolicy = wrapContainer.layout.animate = null;
            wrapContainer.updateLayout();
        }
        else {
            if (!collapsing) {
                navigationList.setMicro(false);
            }

            refs.dmLogo.animate({dynamic: true, to: {width: new_width}});

            navigationList.width = new_width;
            wrapContainer.updateLayout({isRoot: true});

            if (collapsing) {
                navigationList.on({
                    afterlayoutanimation: function () {
                        navigationList.setMicro(true);
                    },
                    single: true
                });
            }
        }
    },

    onMainViewRender: function () {
        var default_route = "service_list";

        if (window.location.href.indexOf("?") > 0) {
            default_route += "?" + window.location.href.split("?")[1];
        }

        this.onRouteChange(default_route);
    },

    onRouteChange: function (id) {
        var r_id = id.split("?")[0],
            me = this;

        if (id.indexOf("?") > 0) {
            this.params = Ext.Object.fromQueryString(id.split("?")[1]);
        }

        Ext.getCmp("button-rotate").setActiveItem("card_" + r_id);

        if (r_id === "layer_list") {
            Ext.getCmp("csvListExport").setDisabled(false);
            Ext.getCmp("search_input").setDisabled(false);
            Ext.getCmp("filterkeywords").setDisabled(true);
            Ext.getStore("LayerList").load();
            Ext.getCmp("search_input").storeId = "LayerList";
        }
        else if (r_id === "portal_list") {
            Ext.getCmp("csvListExport").setDisabled(true);
            Ext.getCmp("search_input").setDisabled(false);
            Ext.getCmp("filterkeywords").setDisabled(true);
            Ext.getStore("PortalList").load();
            Ext.getCmp("search_input").storeId = "PortalList";
        }
        else if (r_id === "datasets_list") {
            Ext.getCmp("csvListExport").setDisabled(false);
            Ext.getCmp("search_input").setDisabled(false);
            Ext.getCmp("filterkeywords").setDisabled(true);
            Ext.getStore("DatasetList").load();
            Ext.getCmp("search_input").storeId = "DatasetList";
        }
        else if (r_id === "service_list") {
            Ext.getCmp("csvListExport").setDisabled(false);
            Ext.getCmp("search_input").setDisabled(false);
            Ext.getCmp("filterkeywords").setDisabled(false);
            Ext.getCmp("search_input").storeId = "ServiceList";

            Ext.getStore("ServiceList").load({
                callback: function () {
                    if (me.params) {
                        if (me.params.filter) {
                            var keywords = "",
                                keyword_array = [];

                            for (var i = 0; i < me.params.filter.split(",").length; i++) {
                                keywords += "'" + me.params.filter.split(",")[i] + "',";
                                keyword_array.push(me.params.filter.split(",")[i]);
                            }

                            if (keywords.length > 0) {
                                keywords = keywords.substring(0, keywords.length - 1);
                            }

                            if (keyword_array.length > 0) {
                                me.setKeywordFilter(keywords);
                                Ext.getCmp("filterkeywords").setValue(keyword_array);
                            }
                        }
                        else if (me.params.service_id) {
                            var service = Ext.getStore("ServiceList").query("service_id", me.params.service_id, false, false, true).items[0].data;

                            DiensteManager.app.getController("DiensteManager.controller.PublicFunctions").openServiceDetailsWindow(service);
                        }
                    }
                }
            });
        }
        else if (r_id === "deleted_service_list") {
            Ext.getCmp("csvListExport").setDisabled(false);
            Ext.getCmp("search_input").setDisabled(false);
            Ext.getCmp("filterkeywords").setDisabled(true);
            Ext.getStore("DeletedServiceList").load();
            Ext.getCmp("search_input").storeId = "DeletedServiceList";
        }
        else if (r_id === "freigaben_list") {
            Ext.getCmp("csvListExport").setDisabled(false);
            Ext.getCmp("search_input").setDisabled(false);
            Ext.getCmp("filterkeywords").setDisabled(true);
            Ext.getStore("FreigabenList").load();
            Ext.getCmp("search_input").storeId = "FreigabenList";
        }
        else if (r_id === "statistics") {
            Ext.getCmp("csvListExport").setDisabled(true);
            Ext.getCmp("search_input").setDisabled(true);
            Ext.getCmp("filterkeywords").setDisabled(true);
            Ext.getStore("LayerStats").load({
                params: {scope: "all"},
                callback: function (records) {
                    var total_layer_count = 0;

                    for (var i = 0; i < records.length; i++) {
                        total_layer_count += records[i].data.count;
                    }
                    Ext.getCmp("layerStatisticsChart").setConfig("title", "Anzahl Layer / FeatureTypes - gesamt: " + total_layer_count);
                }
            });
            Ext.getStore("ServiceStats").load({
                params: {scope: "all"},
                callback: function (records) {
                    var total_service_count = 0;

                    for (var i = 0; i < records.length; i++) {
                        total_service_count += records[i].data.count;
                    }
                    Ext.getCmp("serviceStatisticsChart").setConfig("title", "Diensttypen - gesamt: " + total_service_count);
                }
            });
            Ext.getStore("SoftwareStats").load({
                params: {scope: "all"},
                callback: function (records) {
                    var total_software_count = 0;

                    for (var i = 0; i < records.length; i++) {
                        total_software_count += records[i].data.count;
                    }
                    Ext.getCmp("softwareStatisticsChart").setConfig("title", "Eingesetzte Software - gesamt: " + total_software_count);
                }
            });
            if (DiensteManager.Config.getZugriffsstatistikModul()) {
                Ext.getStore("VisitsTop").load({
                    params: {month: moment().format("YYYY-MM")}
                });
                Ext.getStore("VisitsTotal").load();

                Ext.Ajax.request({
                    url: "backend/getdistinctmonth",
                    method: "GET",
                    success: function (response) {
                        var response_json = Ext.decode(response.responseText),
                            month = [];

                        for (var i = 0; i < response_json.length; i++) {
                            month.push(response_json[i].month);
                        }
                        if (month.length > 0) {
                            Ext.getCmp("chooseTopVisitsMonth").setStore(month);
                        }
                    },
                    failure: function (response) {
                        console.log(Ext.decode(response.responseText));
                    }
                });
            }
        }
        else if (r_id === "tests") {
            Ext.getCmp("csvListExport").setDisabled(true);
            Ext.getCmp("search_input").setDisabled(true);
            Ext.getCmp("filterkeywords").setDisabled(true);
        }
        else if (r_id === "management") {
            Ext.getCmp("csvListExport").setDisabled(true);
            Ext.getCmp("search_input").setDisabled(true);
            Ext.getCmp("filterkeywords").setDisabled(true);
        }
        else if (r_id === "contacts") {
            Ext.getCmp("csvListExport").setDisabled(true);
            Ext.getCmp("search_input").setDisabled(false);
            Ext.getCmp("filterkeywords").setDisabled(true);
            Ext.getCmp("search_input").storeId = "Contacts";
        }

        if (this.route_id !== r_id) {
            this.route_id = r_id;
            this.setCurrentView(r_id);
        }
    },

    onExportList: function () {
        var store = Ext.getStore("ServiceList"),
            csv_name = "services.csv",
            csv = "",
            loadingsMask = new Ext.LoadMask({target: Ext.getCmp(this.route_id), msg: "Bitte warten..."});

        loadingsMask.show();

        if (this.route_id === "layer_list") {
            store = Ext.getStore("LayerList");
            csv_name = "layers.csv";

            csv = "Layer ID;Layer Name;Layer Titel;Dienst ID;Dienst Titel\r\n";

            store.each(function (record) {
                csv += record.get("layer_id") + ";" + record.get("layer_name") + ";" + record.get("title") + ";" + record.get("service_id") + ";" + record.get("service_title") + "\r\n";
            });
        }
        else if (this.route_id === "datasets_list") {
            store = Ext.getStore("DatasetList");
            csv_name = "datasets.csv";
            csv = "Datensatzname;Metadaten ID;Ressource ID;OpenData Kategorie;INSPIRE Kategorie;HambTG Kategorie;Lizenz;Metadaten;Geoportal (intern);Geoportal (extern)\r\n";

            store.each(function (record) {
                var fees = record.get("fees"),
                    metadata_link = record.get("show_doc_url") + record.get("md_id"),
                    geoportal_link_int = DiensteManager.Config.getGeoportalUrl() + "?mdid=" + record.get("md_id"),
                    geoportal_link_ext = DiensteManager.Config.getGeoportalUrlExt() + "?mdid=" + record.get("md_id");

                fees = fees.replace(new RegExp("\n", "g"), "").replace(new RegExp("&gt;", "g"), ">").replace(new RegExp("&lt;", "g"), "<").replace(new RegExp(";", "g"), "");

                csv += record.get("dataset_name") + ";" + record.get("md_id") + ";" + record.get("rs_id") + ";" + record.get("cat_opendata") + ";" + record.get("cat_inspire") + ";" + record.get("cat_hmbtg") + ";" + fees + ";" + metadata_link + ";" + geoportal_link_int + ";" + geoportal_link_ext + "\r\n";
            });
        }
        else if (this.route_id === "service_list") {
            csv = "Dienst ID;Typ;Software;Titel;Name;Internet-URL;Intranet-URL;Metadaten UUID\r\n";

            store.each(function (record) {
                csv += record.get("service_id") + ";" + record.get("service_type") + ";" + record.get("software") + ";" + record.get("title") + ";" + record.get("service_name") + ";" + record.get("url_ext") + ";" + record.get("url_int") + ";" + record.get("service_md_id") + "\r\n";
            });
        }
        else if (this.route_id === "freigaben_liste") {
            store = Ext.getStore("FreigabenList");
            csv_name = "freigaben.csv";

            csv = "Datensatzbezeichnung;Link;Dateneigentümer;Leitzeichen;Ansprechpartner;Erhalten am;HMDK Eintrag;Einschränkung;Bemerkungen\r\n";

            store.each(function (record) {
                csv += record.get("datenbezeichnung") + ";" + record.get("link") + ";" + record.get("dateneigentuemer") + ";" + record.get("leitzeichen") + ";" + record.get("ansprechpartner") + ";" + record.get("erhalten_am") + ";" + record.get("hmdk_eintrag") + ";" + record.get("einschraenkung") + ";" + record.get("bemerkungen") + "\r\n";
            });
        }

        saveAs(new Blob(["\ufeff", csv], {type: "text/csv;charset=UTF-8"}), csv_name);

        loadingsMask.hide();
    },

    onInsertNewService: function () {
        var servicedetailswindow = Ext.getCmp("servicedetails-window");

        Ext.getStore("Layers").removeAll();
        Ext.getStore("ServiceTestResults").removeAll();
        Ext.getStore("Comments").removeAll();
        Ext.getStore("LinkedFreigaben").removeAll();

        if (servicedetailswindow) {
            servicedetailswindow.destroy();
        }

        servicedetailswindow = Ext.create("servicedetails.ServiceDetails");

        Ext.getCmp("servicedetails-form").getForm().setValues({
            servicelasteditdate: moment().format("YYYY-MM-DD HH:mm"),
            serviceeditor: window.auth_user
        });

        Ext.getCmp("updatelayer").setDisabled(true);
        Ext.getCmp("layerdeleterequest").setDisabled(true);

        servicedetailswindow.show();
    },

    onCopyServiceSwitch: function () {
        var grid = Ext.getCmp("service_list");

        if (grid.getSelectionModel().hasSelection()) {
            Ext.getStore("Layers").removeAll();
            Ext.getStore("ServiceTestResults").removeAll();
            Ext.getStore("Comments").removeAll();
            Ext.getStore("LinkedFreigaben").removeAll();

            var service = grid.getSelectionModel().getSelection()[0].data;

            if (service.service_type === "WFS" || service.service_type === "WMS") {
                var title = "",
                    url_int = "",
                    url_ext = "",
                    url_sec = "",
                    service_name = "",
                    version = "",
                    service_type = "";

                if (service.service_type === "WFS") {
                    service_type = "WMS";
                    version = "1.3.0";
                    service_name = service.service_name.replace(new RegExp("wfs", "g"), "wms").replace(new RegExp("WFS", "g"), "WMS");
                    url_ext = service.url_ext.replace(new RegExp("wfs", "g"), "wms").replace(new RegExp("WFS", "g"), "WMS");
                    url_int = service.url_int.replace(new RegExp("wfs", "g"), "wms").replace(new RegExp("WFS", "g"), "WMS");
                    url_sec = service.url_sec.replace(new RegExp("wfs", "g"), "wms").replace(new RegExp("WFS", "g"), "WMS");
                    title = service.title.replace(new RegExp("WFS", "g"), "WMS").replace(new RegExp("wfs", "g"), "wms");
                }
                else if (service.service_type === "WMS") {
                    service_type = "WFS";
                    version = "1.1.0";
                    service_name = service.service_name.replace(new RegExp("wms", "g"), "wfs").replace(new RegExp("WMS", "g"), "WFS");
                    url_ext = service.url_ext.replace(new RegExp("wms", "g"), "wfs").replace(new RegExp("WMS", "g"), "WFS");
                    url_int = service.url_int.replace(new RegExp("wms", "g"), "wfs").replace(new RegExp("WMS", "g"), "WFS");
                    url_sec = service.url_sec.replace(new RegExp("wms", "g"), "wfs").replace(new RegExp("WMS", "g"), "WFS");
                    title = service.title.replace(new RegExp("wms", "g"), "wfs").replace(new RegExp("WMS", "g"), "WFS");
                }

                var servicedetailswindow = Ext.getCmp("servicedetails-window");

                if (servicedetailswindow) {
                    servicedetailswindow.destroy();
                }

                servicedetailswindow = Ext.create("servicedetails.ServiceDetails");

                Ext.getCmp("servicedetails-form").getForm().setValues({
                    servicechecked: "Ungeprüft",
                    servicelasteditdate: moment().format("YYYY-MM-DD HH:mm"),
                    servicetitle: title,
                    serviceeditor: window.auth_user,
                    servicelasteditedby: window.auth_user,
                    servicestatus: "In Bearbeitung",
                    servicetype: service_type,
                    serviceversion: version,
                    servicesoftware: service.software,
                    serviceserver: service.server,
                    servicefolder: service.folder,
                    servicename: service_name,
                    serviceurlint: url_int,
                    serviceurlext: url_ext,
                    serviceurlsec: url_sec,
                    servicesecuritytype: service.security_type,
                    serviceonlyintranet: service.only_intranet,
                    serviceonlyinternet: service.only_internet,
                    servicedatasource: service.datasource,
                    servicemappingdescription: service.mapping_description,
                    servicecache: false,
                    serviceinspirerelevant: service.inspire_relevant
                });

                Ext.getCmp("updatelayer").setDisabled(true);
                Ext.getCmp("layerdeleterequest").setDisabled(true);

                servicedetailswindow.show();
            }
        }
    },

    onCopyService: function () {
        var grid = Ext.getCmp("service_list");

        if (grid.getSelectionModel().hasSelection()) {
            Ext.getStore("Layers").removeAll();
            Ext.getStore("ServiceTestResults").removeAll();
            Ext.getStore("Comments").removeAll();
            Ext.getStore("LinkedFreigaben").removeAll();

            var service = grid.getSelectionModel().getSelection()[0].data,
                title = service.title + " KOPIE",
                url_ext = service.url_ext + "_KOPIE",
                servicedetailswindow = Ext.getCmp("servicedetails-window");

            if (servicedetailswindow) {
                servicedetailswindow.destroy();
            }

            servicedetailswindow = Ext.create("servicedetails.ServiceDetails");

            Ext.getCmp("servicedetails-form").getForm().setValues({
                servicechecked: "Ungeprüft",
                servicelasteditdate: moment().format("YYYY-MM-DD HH:mm"),
                servicetitle: title,
                serviceeditor: window.auth_user,
                servicelasteditedby: window.auth_user,
                servicestatus: "In Bearbeitung",
                servicetype: service.service_type,
                serviceversion: service.version,
                servicesoftware: service.software,
                serviceserver: service.server,
                servicefolder: service.folder,
                servicename: service.service_name,
                serviceurlint: service.url_int,
                serviceurlext: url_ext,
                serviceurlsec: service.url_sec,
                servicesecuritytype: service.security_type,
                serviceonlyintranet: service.only_intranet,
                serviceonlyinternet: service.only_internet,
                servicedatasource: service.datasource,
                servicemappingdescription: service.mapping_description,
                servicecache: false,
                serviceinspirerelevant: service.inspire_relevant
            });

            Ext.getCmp("updatelayer").setDisabled(true);
            Ext.getCmp("layerdeleterequest").setDisabled(true);

            servicedetailswindow.show();
        }
    },

    onDeleteService: function () {
        var grid = Ext.getCmp("service_list");

        if (grid.getSelectionModel().hasSelection()) {
            var mb = Ext.MessageBox;

            mb.buttonText.yes = "ja";
            mb.buttonText.no = "nein";

            mb.confirm("Löschen", "Wirklich löschen?", function (btn) {
                if (btn === "yes") {
                    var row = grid.getSelectionModel().getSelection()[0];

                    if (DiensteManager.Config.getSecurityModul()) {
                        if (row.data.security_type === "Basic Auth") {
                            var controller = DiensteManager.app.getController("DiensteManager.controller.BasicAuthConfiguration");

                            controller.getBasicAuthConfiguration(row.data.service_id, row.data.servicetitle).then(function (configuration) {
                                controller.deleteConfiguration(configuration).then(function () {
                                    Ext.Msg.alert("Konfiguration gelöscht", "Basic Auth Konfiguration ebenfalls gelöscht.", Ext.emptyFn);
                                }).catch(function (error) {
                                    if (error.status === 0) {
                                        console.log("Basic Auth war im Dienst " + row.data.servicetitle + " aktiviert, jedoch nicht konfiguriert.");
                                    }
                                });
                            });
                        }
                    }
                    Ext.Ajax.request({
                        url: "backend/deleteservice",
                        method: "POST",
                        jsonData: {
                            service_id: row.data.service_id,
                            last_edited_by: window.auth_user,
                            last_edit_date: moment().format("YYYY-MM-DD HH:mm")
                        },
                        success: function () {
                            Ext.getStore("ServiceList").reload();
                            Ext.getStore("DeletedServiceList").reload();
                        },
                        failure: function (response) {
                            console.log(Ext.decode(response.responseText));
                        }
                    });
                }
            });
        }
    },

    onRestoreService: function () {
        var grid = Ext.getCmp("deleted_service_list");

        if (grid.getSelectionModel().hasSelection()) {
            var mb = Ext.MessageBox;

            mb.buttonText.yes = "ja";
            mb.buttonText.no = "nein";

            mb.confirm("Wiederherstellen", "Dienst wiederherstellen?", function (btn) {
                if (btn === "yes") {
                    var row = grid.getSelectionModel().getSelection()[0];

                    Ext.Ajax.request({
                        url: "backend/restoreservice",
                        method: "POST",
                        jsonData: {
                            service_id: row.data.service_id,
                            last_edited_by: window.auth_user,
                            last_edit_date: moment().format("YYYY-MM-DD HH:mm")
                        },
                        success: function () {
                            Ext.getStore("ServiceList").reload();
                            Ext.getStore("DeletedServiceList").reload();
                        },
                        failure: function (response) {
                            console.log(Ext.decode(response.responseText));
                        }
                    });
                }
            });
        }
    },

    onFinalDeleteService: function () {
        var grid = Ext.getCmp("deleted_service_list");

        if (grid.getSelectionModel().hasSelection()) {
            var mb = Ext.MessageBox;

            mb.buttonText.yes = "ja";
            mb.buttonText.no = "nein";

            mb.confirm("Löschen", "Dienst endgültig löschen?", function (btn) {
                if (btn === "yes") {
                    var row = grid.getSelectionModel().getSelection()[0];

                    Ext.Ajax.request({
                        url: "backend/deleteservicefinal",
                        method: "POST",
                        jsonData: {
                            service_id: row.data.service_id,
                            last_edited_by: window.auth_user,
                            last_edit_date: moment().format("YYYY-MM-DD HH:mm")
                        },
                        success: function () {
                            Ext.getStore("ServiceList").reload();
                            Ext.getStore("DeletedServiceList").reload();
                        },
                        failure: function (response) {
                            console.log(Ext.decode(response.responseText));
                        }
                    });
                }
            });
        }
    },

    onKeywordFilter: function () {
        var keyword_array = Ext.getCmp("filterkeywords").getValueRecords(),
            keywords = "";

        Ext.getStore("ServiceList").removeFilter("keywordfilter");

        for (var i = 0; i < keyword_array.length; i++) {
            keywords += "'" + keyword_array[i].data.text + "',";
        }

        if (keywords.length > 0) {
            keywords = keywords.substring(0, keywords.length - 1);
        }
        if (keyword_array.length > 0) {
            this.setKeywordFilter(keywords);
        }
    },

    setKeywordFilter: function (keywords) {
        Ext.Ajax.request({
            url: "backend/getservicesbykeyword",
            method: "POST",
            jsonData: {
                text: keywords
            },
            success: function (response) {
                var response_json = Ext.decode(response.responseText),
                    service_id_array = [];

                for (var i = 0; i < response_json.length; i++) {
                    service_id_array.push(parseInt(response_json[i].service_id));
                }

                Ext.getStore("ServiceList").addFilter(new Ext.util.Filter({
                    id: "keywordfilter",
                    filterFn: function (item) {
                        return _.indexOf(service_id_array, item.get("service_id")) !== -1;
                    }
                }));
            },
            failure: function (response) {
                console.log(Ext.decode(response.responseText));
            }
        });
    },

    onImportService: function () {
        Ext.getStore("DeletedServiceList").reload();
        var configDMInstancesStore = Ext.getStore("ConfigDMInstances");

        if (configDMInstancesStore.getCount() > 0) {
            var serviceImportWindow = Ext.getCmp("serviceimport-window");

            if (!serviceImportWindow) {
                serviceImportWindow = Ext.create("serviceimport.ServiceImportWindow");
            }
            serviceImportWindow.show();
        }
        else {
            Ext.Msg.alert("Dienst Import", "Bisher keine externe Dienstemanager-Instanz konfiguriert (Verwaltung > Einstellungen > Externe Dienstemanager-Instanzen konfigurieren).", Ext.emptyFn);
        }
    }
});
