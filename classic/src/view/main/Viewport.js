Ext.define("DiensteManager.view.main.Viewport", {
    extend: "Ext.container.Viewport",
    xtype: "mainviewport",

    requires: [
        "Ext.list.Tree",
        "Ext.form.*"
    ],

    controller: "mainviewport",

    itemId: "mainView",

    layout: {
        type: "vbox",
        align: "stretch"
    },

    listeners: {
        render: "onMainViewRender"
    },

    items: [
        {
            xtype: "toolbar",
            cls: "dm-dash-dash-headerbar toolbar-btn-shadow",
            height: 70,
            itemId: "headerBar",
            items: [
                {
                    xtype: "image",
                    reference: "dmLogo",
                    src: "resources/images/logo_dm.svg",
                    centered: true,
                    width: 210,
                    height: 70
                },
                {
                    margin: "0 0 0 8",
                    cls: "delete-focus-bg",
                    iconCls: "x-fa fa-navicon",
                    id: "main-navigation-btn",
                    handler: "onToggleNavigationSize"
                },
                {
                    width: 210,
                    xtype: "dmsearchfield",
                    id: "search_input",
                    storeId: "ServiceList",
                    margin: "0 5px 0 15px",
                    height: 49
                },
                {
                    xtype: "tagfield",
                    id: "filterkeywords",
                    fieldLabel: "Schlagworte",
                    store: "Keywords",
                    margin: "0 0 0 20",
                    displayField: "text",
                    valueField: "text",
                    forceSelection: false,
                    autocomplete: "off",
                    filterPickList: true,
                    matchFieldWidth: false,
                    grow: false,
                    height: 49,
                    listeners: {
                        change: "onKeywordFilter"
                    }
                },
                {
                    xtype: "tbspacer",
                    flex: 1
                },
                {
                    xtype: "panel",
                    id: "button-rotate",
                    layout: "card",
                    items: [
                        {
                            xtype: "panel",
                            id: "card_service_list",
                            layout: "hbox",
                            items: [
                                {
                                    xtype: "splitbutton",
                                    margin: "0 5px 0 5px",
                                    width: 120,
                                    text: "Dienst...",
                                    id: "service_split_button",
                                    menu: [{
                                        text: "Neu erfassen",
                                        type: "dienst_neu",
                                        itemId: "dienst_neu",
                                        handler: "onInsertNewService"
                                    }, {
                                        text: "Kopieren (1 zu 1)",
                                        type: "dienst_kopieren",
                                        itemId: "dienst_kopieren",
                                        handler: "onCopyService"
                                    }, {
                                        text: "Kopieren (WMS <-> WFS)",
                                        type: "dienst_kopieren_switch",
                                        itemId: "dienst_kopieren_switch",
                                        handler: "onCopyServiceSwitch"
                                    }, {
                                        text: "Löschen",
                                        type: "dienst_loeschen",
                                        itemId: "dienst_loeschen",
                                        handler: "onDeleteService"
                                    }, {
                                        text: "Importieren",
                                        type: "dienst_import",
                                        itemId: "dienst_import",
                                        handler: "onImportService"
                                    }]
                                }
                            ]
                        }, {
                            xtype: "panel",
                            id: "card_deleted_service_list",
                            layout: "hbox",
                            items: [
                                {
                                    xtype: "button",
                                    text: "Dienst wiederherstellen",
                                    id: "restore_deleted_service_button",
                                    margin: "0 5px 0 5px",
                                    width: 200,
                                    tooltip: "Dienst wieder in die aktive Liste überführen",
                                    listeners: {
                                        click: "onRestoreService"
                                    }
                                }, {
                                    xtype: "button",
                                    text: "Dienst endgültig löschen",
                                    id: "final_delete_service_button",
                                    margin: "0 5px 0 5px",
                                    width: 200,
                                    tooltip: "Dienst endgültig löschen",
                                    listeners: {
                                        click: "onFinalDeleteService"
                                    }
                                }
                            ]
                        }, {
                            xtype: "panel",
                            id: "card_layer_list",
                            layout: "hbox",
                            items: [
                            ]
                        }, {
                            xtype: "panel",
                            id: "card_portal_list",
                            layout: "hbox",
                            items: [
                            ]
                        }, {
                            xtype: "panel",
                            id: "card_datasets_list",
                            layout: "hbox",
                            items: [
                            ]
                        }, {
                            xtype: "panel",
                            id: "card_freigaben_list",
                            layout: "hbox",
                            items: [
                            ]
                        }, {
                            xtype: "panel",
                            id: "card_statistics",
                            layout: "hbox",
                            items: [
                            ]
                        }, {
                            xtype: "panel",
                            id: "card_tests",
                            layout: "hbox",
                            items: [
                            ]
                        }, {
                            xtype: "panel",
                            id: "card_management",
                            layout: "hbox",
                            items: [
                            ]
                        }, {
                            xtype: "panel",
                            id: "card_contacts",
                            layout: "hbox",
                            items: [
                            ]
                        }
                    ]
                },
                {
                    xtype: "button",
                    id: "csvListExport",
                    text: "CSV Download",
                    margin: "0 35px 0 5px",
                    width: 120,
                    tooltip: "Liste aller Datensätze als CSV Datei exportieren",
                    listeners: {
                        click: "onExportList"
                    }
                },
                {
                    xtype: "component",
                    id: "dmVersionField",
                    html: DiensteManager.Config.getVersion()
                }
            ]
        },
        {
            xtype: "maincontainerwrap",
            id: "main-view-detail-wrap",
            reference: "mainContainerWrap",
            flex: 1,
            items: [
                {
                    xtype: "treelist",
                    reference: "navigationTreeList",
                    itemId: "navigationTreeList",
                    ui: "navigation",
                    store: "NavigationTree",
                    width: 210,
                    expanderFirst: false,
                    expanderOnly: false,
                    listeners: {
                        selectionchange: "onNavigationTreeSelectionChange"
                    }
                },
                {
                    xtype: "container",
                    flex: 1,
                    reference: "mainCardPanel",
                    itemId: "contentPanel",
                    layout: {
                        type: "card",
                        anchor: "100%"
                    }
                }
            ]
        }
    ]
});
