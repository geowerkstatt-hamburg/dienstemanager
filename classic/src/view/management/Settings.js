Ext.define("DiensteManager.view.management.Settings", {
    extend: "Ext.container.Container",
    xtype: "settings",
    viewModel: {
        data: {
            ConfigMetadataCatalog: null
        }
    },
    controller: "settings",
    items: [
        {
            xtype: "fieldset",
            title: "Metadatenkataloge konfigurieren",
            cls: "prio-fieldset",
            collapsible: true,
            collapsed: true,
            layout: "column",
            items: [{
                xtype: "gridpanel",
                id: "config_metadatacatalogs_grid",
                autoScroll: true,
                height: 300,
                columnWidth: 0.35,
                bind: {
                    selection: "{ConfigMetadataCatalog}"
                },
                store: "ConfigMetadataCatalogs",
                columns: [
                    {
                        dataIndex: "name",
                        text: "Bezeichnung",
                        align: "center",
                        flex: 1
                    }
                ]
            }, {
                xtype: "fieldset",
                title: "Parameter",
                columnWidth: 0.65,
                margin: "0 10 0 10",
                layout: "anchor",
                defaultType: "textfield",
                items: [
                    {
                        xtype: "fieldcontainer",
                        layout: "hbox",
                        labelWidth: 0,
                        width: "100%",
                        items: [
                            {
                                xtype: "button",
                                id: "new_configmetadatacatalog",
                                margin: "0 20 5 0",
                                tooltip: "Neuen Metadatenkatalog eingeben",
                                autoEl: {
                                    tag: "div",
                                    "data-qtip": "Neuen Metadatenkatalog eingeben"
                                },
                                text: "Neu",
                                listeners: {
                                    click: "onNewConfigMetadataCatalog"
                                }
                            },
                            {
                                xtype: "button",
                                id: "delete_configmetadatacatalog",
                                margin: "0 0 5 0",
                                tooltip: "Metadatenkatalog löschen",
                                autoEl: {
                                    tag: "div",
                                    "data-qtip": "Metadatenkatalog löschen"
                                },
                                text: "löschen",
                                listeners: {
                                    click: "onDeleteConfigMetadataCatalog"
                                }
                            }
                        ]
                    },
                    {
                        fieldLabel: "Bezeichnung",
                        labelWidth: 160,
                        id: "configmetadatacatalogname",
                        bind: "{ConfigMetadataCatalog.name}",
                        width: "100%"
                    }, {
                        fieldLabel: "CSW URL",
                        labelWidth: 160,
                        id: "configmetadatacatalogcswurl",
                        bind: "{ConfigMetadataCatalog.csw_url}",
                        width: "100%"
                    }, {
                        fieldLabel: "Show Doc URL",
                        labelWidth: 160,
                        id: "configmetadatacatalogshowdocurl",
                        bind: "{ConfigMetadataCatalog.show_doc_url}",
                        width: "100%"
                    }, {
                        fieldLabel: "Metadaten SRS",
                        labelWidth: 160,
                        id: "configmetadatacatalogsrs",
                        bind: "{ConfigMetadataCatalog.srs_metadata}",
                        width: "100%"
                    }, {
                        xtype: "checkbox",
                        fieldLabel: "Proxy",
                        id: "configmetadatacatalogproxy",
                        bind: "{ConfigMetadataCatalog.proxy}",
                        width: "100%"
                    }, {
                        xtype: "button",
                        id: "save_configmetadatacatalog",
                        margin: "10 0 5 0",
                        text: "Speichern",
                        listeners: {
                            click: "onSaveConfigMetadataCatalog"
                        }
                    }
                ]
            }]
        }, {
            xtype: "fieldset",
            title: "Externe Dienstemanager-Instanzen konfigurieren",
            cls: "prio-fieldset",
            collapsible: true,
            collapsed: true,
            layout: "column",
            items: [{
                xtype: "gridpanel",
                id: "config_dminstances_grid",
                autoScroll: true,
                height: 300,
                columnWidth: 0.35,
                bind: {
                    selection: "{ConfigDMInstance}"
                },
                store: "ConfigDMInstances",
                columns: [
                    {
                        dataIndex: "name",
                        text: "Bezeichnung",
                        align: "center",
                        flex: 1
                    }
                ]
            }, {
                xtype: "fieldset",
                title: "Parameter",
                columnWidth: 0.65,
                margin: "0 10 0 10",
                layout: "anchor",
                defaultType: "textfield",
                items: [
                    {
                        xtype: "fieldcontainer",
                        layout: "hbox",
                        labelWidth: 0,
                        width: "100%",
                        items: [
                            {
                                xtype: "button",
                                id: "add_dminstance",
                                margin: "0 20 5 0",
                                tooltip: "Neue Dienstemanager-Instanz eingeben",
                                autoEl: {
                                    tag: "div",
                                    "data-qtip": "Neue Dienstemanager-Instanz eingeben"
                                },
                                text: "Neu",
                                listeners: {
                                    click: "onNewConfigDMInstance"
                                }
                            },
                            {
                                xtype: "button",
                                id: "delete_dminstance",
                                margin: "0 0 5 0",
                                tooltip: "Dienstemanager Instan löschen",
                                autoEl: {
                                    tag: "div",
                                    "data-qtip": "Dienstemanager Instan löschen"
                                },
                                text: "löschen",
                                listeners: {
                                    click: "onDeleteConfigDMInstance"
                                }
                            }
                        ]
                    },
                    {
                        fieldLabel: "Bezeichnung",
                        labelWidth: 160,
                        id: "configdminstancename",
                        bind: "{ConfigDMInstance.name}",
                        width: "100%"
                    }, {
                        fieldLabel: "Host",
                        labelWidth: 160,
                        id: "configdminstancehost",
                        bind: "{ConfigDMInstance.host}",
                        width: "100%"
                    }, {
                        fieldLabel: "Port",
                        labelWidth: 160,
                        id: "configdminstanceport",
                        bind: "{ConfigDMInstance.port}",
                        width: "100%"
                    }, {
                        fieldLabel: "Datenbankname",
                        labelWidth: 160,
                        id: "configdminstancedatabase",
                        bind: "{ConfigDMInstance.database}",
                        width: "100%"
                    }, {
                        fieldLabel: "DB-User",
                        labelWidth: 160,
                        id: "configdminstancedbuser",
                        bind: "{ConfigDMInstance.db_user}",
                        width: "100%"
                    }, {
                        fieldLabel: "Passwort",
                        inputType: "password",
                        labelWidth: 160,
                        id: "configdminstancepassword",
                        bind: "{ConfigDMInstance.pw}",
                        width: "100%"
                    }, {
                        xtype: "button",
                        id: "save_dminstance",
                        margin: "10 0 5 0",
                        text: "Speichern",
                        listeners: {
                            click: "onSaveConfigDMInstance"
                        }
                    }
                ]
            }]
        }
    ]
});
