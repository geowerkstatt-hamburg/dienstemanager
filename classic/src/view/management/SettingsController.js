Ext.define("DiensteManager.view.management.SettingsController", {
    extend: "Ext.app.ViewController",
    alias: "controller.settings",

    onSaveConfigMetadataCatalog: function () {
        var selection = Ext.getCmp("config_metadatacatalogs_grid").getSelectionModel().getSelection();

        if (selection.length === 1) {
            var data = selection[0].data;

            Ext.Ajax.request({
                url: "backend/updateconfigmetadatacatalog",
                method: "POST",
                jsonData: {
                    config_metadata_catalog_id: data.config_metadata_catalog_id,
                    name: data.name,
                    csw_url: data.csw_url,
                    show_doc_url: data.show_doc_url,
                    proxy: data.proxy,
                    srs_metadata: data.srs_metadata
                },
                success: function () {
                    Ext.getStore("ConfigMetadataCatalogs").reload();
                },
                failure: function (response) {
                    console.log(Ext.decode(response.responseText));
                }
            });
        }
        else if (selection.length === 0 && Ext.getCmp("configmetadatacatalogname").getSubmitValue() !== "" && Ext.getCmp("configmetadatacatalogcswurl").getSubmitValue() !== "") {
            var configMetadataCatalog = Ext.getStore("ConfigMetadataCatalogs").findRecord("csw_url", Ext.getCmp("configmetadatacatalogcswurl").getSubmitValue(), 0, false, false, true);

            if (!configMetadataCatalog) {
                configMetadataCatalog = Ext.getStore("ConfigMetadataCatalogs").findRecord("name", Ext.getCmp("configmetadatacatalogname").getSubmitValue(), 0, false, false, true);
                if (!configMetadataCatalog) {
                    Ext.Ajax.request({
                        url: "backend/addconfigmetadatacatalog",
                        method: "POST",
                        jsonData: {
                            name: Ext.getCmp("configmetadatacatalogname").getSubmitValue(),
                            csw_url: Ext.getCmp("configmetadatacatalogcswurl").getSubmitValue(),
                            show_doc_url: Ext.getCmp("configmetadatacatalogshowdocurl").getSubmitValue(),
                            proxy: Ext.getCmp("configmetadatacatalogproxy").getValue(),
                            srs_metadata: Ext.getCmp("configmetadatacatalogsrs").getSubmitValue()
                        },
                        success: function () {
                            Ext.getStore("ConfigMetadataCatalogs").reload();
                        },
                        failure: function (response) {
                            console.log(Ext.decode(response.responseText));
                        }
                    });
                }
                else {
                    Ext.MessageBox.alert("Achtung", "Name schon vergeben");
                }
            }
            else {
                Ext.MessageBox.alert("Achtung", "Metadatenkatalog schon vorhanden");
            }

        }
    },

    onNewConfigMetadataCatalog: function () {
        Ext.getCmp("config_metadatacatalogs_grid").getSelectionModel().deselectAll();
    },

    onDeleteConfigMetadataCatalog: function () {
        var selection = Ext.getCmp("config_metadatacatalogs_grid").getSelectionModel().getSelection();

        if (selection.length === 1) {
            var mb = Ext.MessageBox;

            mb.buttonText.yes = "ja";
            mb.buttonText.no = "nein";

            mb.confirm("Metadatenkatalog Löschen", "wirklich löschen?", function (btn) {
                if (btn === "yes") {
                    var data = selection[0].data;

                    Ext.Ajax.request({
                        url: "backend/deleteconfigmetadatacatalog",
                        method: "POST",
                        jsonData: {
                            config_metadata_catalog_id: data.config_metadata_catalog_id
                        },
                        success: function () {
                            Ext.getStore("ConfigMetadataCatalogs").reload();
                        },
                        failure: function (response) {
                            console.log(Ext.decode(response.responseText));
                        }
                    });
                }
            });
        }
    },

    onNewConfigDMInstance: function () {
        Ext.getCmp("config_dminstances_grid").getSelectionModel().deselectAll();
    },

    onDeleteConfigDMInstance: function () {
        var selection = Ext.getCmp("config_dminstances_grid").getSelectionModel().getSelection();

        if (selection.length === 1) {
            var mb = Ext.MessageBox;

            mb.buttonText.yes = "Ja";
            mb.buttonText.no = "Nein";

            mb.confirm("Dienstemanager-Instanz Löschen", "Wirklich löschen?", function (btn) {
                if (btn === "yes") {
                    var data = selection[0].data;

                    Ext.Ajax.request({
                        url: "backend/deleteconfigdminstance",
                        method: "POST",
                        jsonData: {
                            config_dm_instance_id: data.config_dm_instance_id
                        },
                        success: function () {
                            Ext.getStore("ConfigDMInstances").reload();
                        },
                        failure: function (response) {
                            console.log(Ext.decode(response.responseText));
                        }
                    });
                }
            });
        }
    },

    onSaveConfigDMInstance: function () {
        var selection = Ext.getCmp("config_dminstances_grid").getSelectionModel().getSelection(),
            name = Ext.getCmp("configdminstancename").getSubmitValue(),
            database = Ext.getCmp("configdminstancedatabase").getSubmitValue(),
            host = Ext.getCmp("configdminstancehost").getSubmitValue(),
            port = Ext.getCmp("configdminstanceport").getSubmitValue(),
            db_user = Ext.getCmp("configdminstancedbuser").getSubmitValue(),
            pw = Ext.getCmp("configdminstancepassword").getSubmitValue();

        if (selection.length === 1) {
            var data = selection[0].data;

            Ext.Ajax.request({
                url: "backend/updateconfigdminstance",
                method: "POST",
                jsonData: {
                    config_dm_instance_id: data.config_dm_instance_id,
                    name: data.name,
                    host: data.host,
                    port: data.port,
                    database: data.database,
                    db_user: data.db_user,
                    pw: data.pw
                },
                success: function () {
                    Ext.getStore("ConfigDMInstances").reload();
                },
                failure: function (response) {
                    console.log(Ext.decode(response.responseText));
                }
            });
        }
        else if (selection.length === 0 && name !== "" && database !== "" && host !== "" && port !== "" && db_user !== "" && pw !== "") {
            var configDMInstance = Ext.getStore("ConfigDMInstances").findRecord("name", name, 0, false, false, true);

            if (!configDMInstance) {
                configDMInstance = Ext.getStore("ConfigDMInstances").findRecord("host", host, 0, false, false, true);
                if (!configDMInstance) {
                    Ext.Ajax.request({
                        url: "backend/addconfigdminstance",
                        method: "POST",
                        jsonData: {
                            name: name,
                            host: host,
                            port: port,
                            database: database,
                            db_user: db_user,
                            pw: pw
                        },
                        success: function () {
                            Ext.getStore("ConfigDMInstances").reload();
                        },
                        failure: function (response) {
                            console.log(Ext.decode(response.responseText));
                        }
                    });
                }
                else {
                    Ext.MessageBox.alert("Achtung", "Name schon vergeben");
                }
            }
            else {
                Ext.MessageBox.alert("Achtung", "Dienstemanager-Instanz schon vorhanden");
            }

        }
    }
});
