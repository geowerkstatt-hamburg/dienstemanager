Ext.define("DiensteManager.view.management.ManagementController", {
    extend: "Ext.app.ViewController",
    alias: "controller.verwaltung",

    init: function () {
        var managementItems = Ext.getCmp("managementItems");

        if (!DiensteManager.Config.getProxyupdateModul()) {
            managementItems.remove("geodienste_proxy_label", true);
            managementItems.remove("geodienste_proxy_start", true);
        }

        if (!DiensteManager.Config.getFreigabenModul()) {
            managementItems.remove("freigaben_update_label", true);
            managementItems.remove("freigaben_update_start", true);
        }

        managementItems.updateLayout();
    },

    onGeodiensteProxyAktualisieren: function () {
        Ext.getCmp("geodienste_proxy_start").setDisabled(true);
        Ext.getCmp("geodienste_proxy_label").setValue("...in Bearbeitung");

        Ext.Ajax.request({
            url: "backend/updategeoproxyconf",
            method: "GET",
            success: function (response) {
                var response_json = Ext.decode(response.responseText);

                if (response_json.success) {
                    Ext.getCmp("geodienste_proxy_label").setValue(moment().format("HH:mm") + " h : Erfolgreich abgeschlossen!");
                    Ext.getCmp("geodienste_proxy_start").setDisabled(false);
                }
                else {
                    Ext.getCmp("geodienste_proxy_label").setValue(moment().format("HH:mm") + " h : Fehler!");
                    Ext.getCmp("geodienste_proxy_start").setDisabled(false);
                }
            },
            failure: function (response) {
                Ext.getCmp("geodienste_proxy_label").setValue(moment().format("HH:mm") + " h : Fehler!");
                Ext.getCmp("geodienste_proxy_start").setDisabled(false);
                console.log(Ext.decode(response.responseText));
            }
        });
    },

    onFreigabenUpdate: function () {
        Ext.getCmp("freigaben_update_start").setDisabled(true);
        Ext.getCmp("freigaben_update_label").setValue("...in Bearbeitung");

        Ext.Ajax.request({
            url: "backend/updatefreigaben",
            method: "GET",
            success: function (response) {
                var response_json = Ext.decode(response.responseText);

                if (response_json.success) {
                    Ext.getCmp("freigaben_update_label").setValue(moment().format("HH:mm") + " h : Erfolgreich abgeschlossen");
                    Ext.getCmp("freigaben_update_start").setDisabled(false);
                }
                else {
                    Ext.getCmp("freigaben_update_label").setValue(moment().format("HH:mm") + " h : Fehler bei der Aktualisierung der Freigabenliste!");
                    Ext.getCmp("freigaben_update_start").setDisabled(false);
                }
            },
            failure: function (response) {
                Ext.getCmp("freigaben_update_label").setValue(moment().format("HH:mm") + " h : Fehler bei der Aktualisierung der Freigabenliste!");
                Ext.getCmp("freigaben_update_start").setDisabled(false);
                console.log(Ext.decode(response.responseText));
            }
        });
    }
});
