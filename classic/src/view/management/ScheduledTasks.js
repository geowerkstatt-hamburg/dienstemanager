Ext.define("DiensteManager.view.management.ScheduledTasks", {
    extend: "Ext.container.Container",
    xtype: "scheduledtasks",
    controller: "scheduledtasks",
    items: [
        {
            xtype: "grid",
            store: "ScheduledTasks",
            autoScroll: true,
            height: 400,
            id: "scheduledtasks-grid",
            viewConfig: {
                enableTextSelection: true,
                stripeRows: false
            },
            columnLines: true,
            selModel: "rowmodel",
            plugins: [
                {
                    ptype: "cellediting",
                    clicksToMoveEditor: 1
                },
                {
                    ptype: "rowexpander",
                    rowBodyTpl: new Ext.XTemplate("<p>{message}</p>")
                }
            ],
            columns: [
                {
                    dataIndex: "name",
                    header: "Name",
                    align: "left",
                    width: 300
                },
                {
                    dataIndex: "descr",
                    header: "Beschreibung",
                    align: "left",
                    flex: 1
                },
                {
                    dataIndex: "last_run",
                    header: "Letzte Ausführung",
                    align: "center",
                    width: 300
                },
                {
                    dataIndex: "status",
                    header: "Status",
                    align: "center",
                    width: 200
                },
                {
                    dataIndex: "schedule",
                    header: "Zeitplan",
                    align: "center",
                    width: 300,
                    editor: {
                        allowBlank: false
                    }
                },
                {
                    xtype: "checkcolumn",
                    dataIndex: "enabled",
                    header: "aktiv",
                    align: "center",
                    width: 100,
                    editor: {
                        xtype: "checkbox"
                    }
                },
                {
                    xtype: "actioncolumn",
                    header: "Starten",
                    align: "center",
                    width: 100,
                    items: [
                        {
                            icon: "resources/images/start.png",
                            tooltip: "Aufgabe starten",
                            handler: "onStartScheduledTask"
                        }
                    ],
                    renderer: function (value, meta, record) {
                        if (record.get("status") === "in Ausführung") {
                            this.items[0].icon = "resources/images/not_available.png";
                            this.items[0].tooltip = "Aufgabe läuft";
                        }
                        else {
                            this.items[0].icon = "resources/images/start.png";
                            this.items[0].tooltip = "Aufgabe starten";
                            this.items[0].handler = "onStartScheduledTask";
                        }
                    }
                }
            ],
            fbar: [
                {
                    text: "",
                    iconCls: "right-icon white-icon pictos pictos-refresh",
                    handler: "getStatus"
                },
                {
                    text: "Speichern",
                    handler: "saveChanges"
                }
            ]
        }
    ]
});
