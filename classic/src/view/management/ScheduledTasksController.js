Ext.define("DiensteManager.view.management.ScheduledTasksController", {
    extend: "Ext.app.ViewController",
    alias: "controller.scheduledtasks",

    onStartScheduledTask: function (grid, rowIndex) {
        var store = grid.getStore(),
            model = store.getAt(rowIndex);

        if (model.get("status") === "in Ausführung") {
            var mb = Ext.MessageBox;

            mb.buttonText.yes = "ja";
            mb.buttonText.no = "nein";

            mb.confirm("Zurücksetzen", "Status zurück setzen? Die laufende Aufgabe wird nicht abgebrochen!", function (btn) {
                if (btn === "yes") {
                    model.set("status", "Abgebrochen");

                    Ext.Ajax.request({
                        url: "backend/cancelscheduledtask",
                        method: "POST",
                        jsonData: {
                            task_name: model.data.name
                        }
                    });
                }
            });
        }
        else {
            model.set("status", "in Ausführung");

            Ext.Ajax.request({
                url: "backend/startscheduledtask",
                method: "POST",
                jsonData: {
                    task_name: model.data.name
                }
            });
        }
    },

    getStatus: function () {
        Ext.getStore("ScheduledTasks").reload();
    },

    saveChanges: function () {
        var updated_records = Ext.getStore("ScheduledTasks").getUpdatedRecords(),
            data = [],
            valid_cron = true;

        for (var i = 0; i < updated_records.length; i++) {
            var cronregex = new RegExp(/^(\*|([0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9])|\*\/([0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9])) (\*|([0-9]|1[0-9]|2[0-3])|\*\/([0-9]|1[0-9]|2[0-3])) (\*|([1-9]|1[0-9]|2[0-9]|3[0-1])|\*\/([1-9]|1[0-9]|2[0-9]|3[0-1])) (\*|([1-9]|1[0-2])|\*\/([1-9]|1[0-2])) (\*|([0-6])|\*\/([0-6]))$/);

            if (!cronregex.test(updated_records[i].data.schedule)) {
                valid_cron = false;
            }

            data.push({
                name: updated_records[i].data.name,
                schedule: updated_records[i].data.schedule,
                enabled: updated_records[i].data.enabled
            });
        }

        if (valid_cron && data.length > 0) {
            Ext.Ajax.request({
                url: "backend/updatescheduledtasks",
                method: "POST",
                jsonData: {
                    tasks: data
                },
                success: function () {
                    Ext.getStore("ScheduledTasks").reload();
                },
                failure: function (response) {
                    console.log(Ext.decode(response.responseText));
                }
            });
        }
        else if (!valid_cron && data.length > 0) {
            Ext.Msg.alert("Format ungültig", "cron Formateingabe ungültig");
        }
    }
});
