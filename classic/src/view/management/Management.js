Ext.define("DiensteManager.view.management.Management", {
    extend: "Ext.panel.Panel",
    requires: [
        "Ext.layout.container.VBox",
        "Ext.panel.Panel",
        "Ext.form.*"
    ],
    id: "verwaltung-view",
    width: "100%",
    controller: "verwaltung",
    items: [{
        xtype: "tabpanel",
        activeTab: 0,
        defaults: {
            layout: "anchor"
        },
        items: [{
            xtype: "container",
            title: "Allgemeines",
            items: {
                xtype: "fieldset",
                id: "managementItems",
                layout: "vbox",
                items: [
                    {
                        xtype: "displayfield",
                        id: "geodienste_proxy_label",
                        margin: "20px 0px 10px 0px",
                        width: 500,
                        fieldLabel: "Geodienste Proxykonfiguration erzeugen:",
                        labelWidth: 250
                    },
                    {
                        xtype: "button",
                        text: "Start",
                        id: "geodienste_proxy_start",
                        tooltip: "Startet den FME Prozess, der die Geoproxy Konfiguration generiert und auf dem WFALGPT002 unter E:\\GeodiensteUpdate ablegt.",
                        listeners: {
                            click: "onGeodiensteProxyAktualisieren"
                        }
                    },
                    {
                        xtype: "displayfield",
                        id: "freigaben_update_label",
                        margin: "20px 0px 10px 0px",
                        width: 500,
                        fieldLabel: "Freigaben aus dem Sharepoint aktualisieren:",
                        labelWidth: 250
                    },
                    {
                        xtype: "button",
                        text: "Start",
                        id: "freigaben_update_start",
                        tooltip: "Startet den FME Prozess, der die Daten über Freigabeerklärungen aus der Sharepointliste aktualisiert.",
                        listeners: {
                            click: "onFreigabenUpdate"
                        }
                    }
                ]
            }
        },
        {
            xtype: "settings",
            title: "Einstellungen"
        },
        {
            xtype: "scheduledtasks",
            title: "Geplante Aufgaben"
        }]
    }]
});
