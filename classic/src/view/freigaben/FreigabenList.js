Ext.define("DiensteManager.view.freigaben.FreigabenList", {
    extend: "Ext.grid.Panel",

    requires: [
        "Ext.grid.*",
        "Ext.data.*",
        "Ext.util.*",
        "Ext.form.*"
    ],

    id: "freigaben_list",

    store: "FreigabenList",

    viewConfig: {
        enableTextSelection: true
    },

    headerBorders: false,
    rowLines: false,
    autoScroll: true,

    height: Ext.Element.getViewportHeight() - 70,

    columns: [
        {
            header: "Datenbezeichnung",
            dataIndex: "datenbezeichnung",
            align: "left",
            width: 350
        }, {
            header: "Link",
            dataIndex: "link",
            width: 80,
            align: "center",
            renderer: function (value) {
                return "<a href=\"" + value + "\" target=\"_blank\">Link</a>";
            }
        }, {
            header: "Dateneigentümer",
            dataIndex: "dateneigentuemer",
            align: "left",
            width: 150
        }, {
            header: "Leitzeichen",
            dataIndex: "leitzeichen",
            align: "left",
            width: 70
        }, {
            header: "Ansprechpartner",
            dataIndex: "ansprechpartner",
            align: "left",
            flex: 1
        }, {
            header: "Erhalten am",
            dataIndex: "erhalten_am",
            align: "center",
            width: 100
        }, {
            header: "HMDK Eintrag",
            dataIndex: "hmdk_eintrag",
            align: "left",
            flex: 1
        }, {
            header: "Einschränkungen",
            dataIndex: "einschraenkung",
            align: "left",
            flex: 1
        }, {
            header: "Bemerkungen",
            dataIndex: "bemerkungen",
            align: "left",
            width: 150
        }, {
            xtype: "checkcolumn",
            header: "HMDK Daten",
            dataIndex: "hmdk_daten",
            hidden: true,
            disabled: true,
            align: "center",
            flex: 1
        }, {
            xtype: "checkcolumn",
            header: "HMDK Dienst",
            dataIndex: "hmdk_dienst",
            hidden: true,
            disabled: true,
            align: "center",
            flex: 1
        }, {
            xtype: "checkcolumn",
            header: "3A Web",
            dataIndex: "aaa_web",
            hidden: true,
            disabled: true,
            align: "center",
            flex: 1
        }, {
            xtype: "checkcolumn",
            header: "Geoportal HH",
            dataIndex: "geoportal_hh",
            hidden: true,
            disabled: true,
            align: "center",
            flex: 1
        }, {
            xtype: "checkcolumn",
            header: "Frei nutzbar",
            dataIndex: "frei_nutzbar",
            hidden: true,
            disabled: true,
            align: "center",
            flex: 1
        }, {
            xtype: "checkcolumn",
            header: "WMS intranet",
            dataIndex: "wms_intranet",
            hidden: true,
            disabled: true,
            align: "center",
            flex: 1
        }, {
            xtype: "checkcolumn",
            header: "WMS internet",
            dataIndex: "wms_internet",
            hidden: true,
            disabled: true,
            align: "center",
            flex: 1
        }, {
            xtype: "checkcolumn",
            header: "WFS intranet",
            dataIndex: "wfs_intranet",
            hidden: true,
            disabled: true,
            align: "center",
            flex: 1
        }, {
            xtype: "checkcolumn",
            header: "WFS internet",
            dataIndex: "wfs_internet",
            hidden: true,
            disabled: true,
            align: "center",
            flex: 1
        }, {
            xtype: "checkcolumn",
            header: "Internet",
            dataIndex: "internet",
            hidden: true,
            disabled: true,
            align: "center",
            flex: 1
        }, {
            xtype: "checkcolumn",
            header: "hambtg",
            dataIndex: "hambtg",
            hidden: true,
            disabled: true,
            align: "center",
            flex: 1
        }, {
            xtype: "checkcolumn",
            header: "OpenData",
            dataIndex: "opendata",
            hidden: true,
            disabled: true,
            align: "center",
            flex: 1
        }, {
            xtype: "checkcolumn",
            header: "FHH-Atlas",
            dataIndex: "fhh_atlas",
            hidden: true,
            disabled: true,
            align: "center",
            flex: 1
        }
    ]
});
