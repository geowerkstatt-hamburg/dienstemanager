Ext.define("DiensteManager.controller.BasicAuthConfiguration", {
    extend: "Ext.app.Controller",
    alias: "controller.BasicAuthConfiguration",

    getBasicAuthStore: function () {
        var store = Ext.data.StoreManager.lookup("SecurityProxyConfiguration");

        if (!store) {
            Ext.create("Ext.data.Store", {
                model: "SecurityProxyConfigurationModel",
                storeId: "SecurityProxyConfiguration"
            });
            store = Ext.data.StoreManager.lookup("SecurityProxyConfiguration");
        }
        return store;
    },

    getServiceDataFromStore: function (service_id) {
        return Ext.getStore("ServiceList").query("service_id", service_id, false, false, true).items[0].data;
    },

    configurationChanged: function (newConfiguration, oldConfiguration) {
        if (newConfiguration.grant_right !== oldConfiguration.grant_right) {
            return true;
        }
        if (oldConfiguration.users.length === newConfiguration.users.length) {
            for (var i = 0; i < oldConfiguration.users.length; i++) {
                if (oldConfiguration.users[i].password !== newConfiguration.users[i].password) {
                    return true;
                }
                if (oldConfiguration.users[i].username !== newConfiguration.users[i].username) {
                    return true;
                }
            }
            return false;
        }
        else {
            return true;
        }
    },

    checkUsernames: function (existingUsernames, oldConfiguration, newUsers) {
        var usernames = [],
            usernameFails = [],
            newUsernames = [];

        oldConfiguration.users.forEach(function (user) {
            usernames.push(user.username);
        });

        newUsers.forEach(function (user) {
            newUsernames.push(user.username);
        });

        // check if username can't be found
        existingUsernames.forEach(function (user) {
            if (usernames.indexOf(user) < 0) {
                usernameFails.push(user);
            }
        });

        // check if username is duplicate of existing one
        var duplicateUsernames = newUsernames.filter(function (e, i, a) {
            return a.indexOf(e) !== i;
        });

        return usernameFails.concat(duplicateUsernames);
    },

    usernameExists: function (users) {
        return new Ext.Promise(function (resolve, reject) {
            Ext.Ajax.request({
                url: "backend/usernameExistsInSecurityProxy",
                jsonData: {
                    users: users
                },
                success: function (response) {
                    resolve(Ext.decode(response.responseText));
                },
                failure: function (response) {
                    reject(response.status);
                }
            });
        });
    },

    updateServiceDetailsView: function () {
        Ext.getCmp("servicedetails-form").getForm().setValues({
            serviceurlsec: "",
            servicesecuritytype: "keine"
        });
    },

    removeBasicAuthConfigurationFromStore: function (endpoint_name) {
        var me = this,
            store = me.getBasicAuthStore(),
            i = store.findExact("endpoint_name", endpoint_name);

        if (i !== -1) {
            store.removeAt(i);
        }
    },

    getBasicAuthConfiguration: function (service_id, endpoint_name) {

        var me = this,
            store = me.getBasicAuthStore();

        me.removeBasicAuthConfigurationFromStore(endpoint_name);

        return new Ext.Promise(function (resolve, reject) {
            var service = me.getServiceDataFromStore(service_id);

            Ext.Ajax.request({
                url: "backend/getSecurityProxyConfiguration",
                jsonData: {
                    endpoint_name: service.title,
                    proxy_url: service.url_int
                },
                success: function (response) {
                    var result = Ext.decode(response.responseText);

                    if (result.status !== "noSecurityProxyConfiguration") {
                        store.add(result);
                    }

                    resolve(result);
                },
                failure: function (response) {
                    reject(response.status);
                }
            });
        });
    },

    addBasicAuthConfiguration: function (basicAuthConfiguration) {
        return new Ext.Promise(function (resolve, reject) {
            Ext.Ajax.request({
                url: "backend/addSecurityProxyConfiguration",
                jsonData: basicAuthConfiguration,
                success: function (response) {
                    resolve(Ext.decode(response.responseText));
                },
                failure: function (response) {
                    reject(response.status);
                }
            });
        });
    },

    updateBasicAuthEndpoint: function (service_id, endpoint_name, proxy_url, params) {
        var me = this;

        return new Ext.Promise(function (resolve, reject) {
            Ext.Ajax.request({
                url: "backend/updateEndpoint",
                jsonData: {
                    endpoint_name: endpoint_name,
                    proxy_url: proxy_url,
                    endpoint_id: params.endpoint_id,
                    url_sec: params.url_sec
                },
                success: function (response) {
                    var service = me.getServiceDataFromStore(service_id),
                        store = Ext.data.StoreManager.lookup("SecurityProxyConfiguration");

                    if (store) {
                        var i = store.findExact("endpoint_name", service.title);

                        if (i !== -1) {
                            var record = store.getAt(i);

                            record.set("endpoint_name", Ext.getCmp("servicetitle").getSubmitValue());
                            record.set("proxy_url", Ext.getCmp("serviceurlint").getSubmitValue());
                        }
                    }
                    resolve(Ext.decode(response.responseText));
                },
                failure: function (response) {
                    reject(response.status);
                }
            });
        });
    },

    deleteConfiguration: function (securityProxyConfiguration) {

        return new Ext.Promise(function (resolve, reject) {
            Ext.Ajax.request({
                url: "backend/deleteSecurityProxyConfiguration",
                jsonData: securityProxyConfiguration,
                success: function (response) {
                    resolve(Ext.decode(response.responseText));
                },
                failure: function (response) {
                    reject(response);
                }
            });
        });
    },

    getLayerObjects: function (service_type, grant_right) {

        var layers = [];

        Ext.getStore("Layers").each(function (layer) {

            // Doppelpunkte machen Probleme
            var layer_name = layer.get("layer_name").split(":");

            if (layer_name.length > 1) {
                layer_name = layer_name[1];
            }
            else {
                layer_name = layer.get("layer_name");
            }

            if (service_type === "WFS") {
                var layerEmptyNamespace = {
                    object_type: service_type,
                    object_name: "{}:" + layer_name,
                    object_title: layer.get("title")
                };

                layers.push(layerEmptyNamespace);

                // erzeuge für jeden WFS Layer ein Duplikat mit namespace im object name
                if (layer.get("namespace").length > 0) {
                    var layerNamespace = {
                        object_type: service_type,
                        object_name: "{" + layer.get("namespace") + "}:" + layer_name,
                        object_title: layer.get("title")
                    };

                    layers.push(layerNamespace);
                }
            }
            else {
                var layerObj = {
                    object_type: service_type,
                    object_name: layer_name,
                    object_title: layer.get("title")
                };

                layers.push(layerObj);
            }
        });

        if (grant_right === "WFS-T:*") {
            var layerFeatureCollection = {
                object_type: service_type,
                object_name: "{}:FeatureCollection",
                object_title: ""
            };
            var layerFeatureCollectionNamespaceWFS200 = {
                object_type: service_type,
                object_name: "{http://www.opengis.net/gml/3.2}:FeatureCollection",
                object_title: ""
            };
            var layerFeatureCollectionNamespaceWFS110 = {
                object_type: service_type,
                object_name: "{http://www.opengis.net/gml}:FeatureCollection",
                object_title: ""
            };

            layers.push(layerFeatureCollection);
            layers.push(layerFeatureCollectionNamespaceWFS200);
            layers.push(layerFeatureCollectionNamespaceWFS110);
        }

        return layers;
    },

    updateEndpointLayers: function (endpoint_name, service_id, service_type, grant_right) {

        var me = this,
            store = me.getBasicAuthStore(),
            i = store.findExact("endpoint_name", endpoint_name),
            layers = me.getLayerObjects(service_type, grant_right),
            endpoint_url = Ext.getCmp("serviceurlsec").getSubmitValue(),
            url_int = Ext.getCmp("serviceurlint").getSubmitValue();

        if (i !== -1) {
            var configuration = store.getAt(i).data;

            configuration.endpoint_url = endpoint_url;
            configuration.url_int = url_int;
            configuration.users.forEach(user => {
                user.password_type = "plain";
            });

            if (configuration.hasOwnProperty("endpoint_name")) {
                me.deleteConfiguration(configuration).then(function () {
                    configuration.objects = layers;
                    if (layers.length > 0) {
                        me.addBasicAuthConfiguration(configuration).then(function (resultArray) {
                            if (resultArray.length > 0) {
                                Ext.Msg.alert("Konfiguration erfolgreich", "Die Basic Auth Konfiguration wurde erfolgreich geändert.", Ext.emptyFn);
                            }
                            else {
                                Ext.Msg.alert("Konfiguration nicht erfolgreich", "Die Basic Auth Konfiguration wurde nicht geändert.", Ext.emptyFn);
                            }
                            me.getBasicAuthConfiguration(service_id, endpoint_name);
                        });
                    }
                    else {
                        me.updateServiceDetailsView();
                        Ext.Msg.alert("Konfiguration gelöscht", "Die Basic Auth Konfiguration wurde gelöscht, da keine Layer mehr vorhanden sind.", Ext.emptyFn);
                        me.getBasicAuthConfiguration(service_id, endpoint_name);
                    }
                });
            }
        }
        else {
            me.getBasicAuthConfiguration(service_id, endpoint_name).then(function (basicAuthConfiguration) {

                basicAuthConfiguration.endpoint_url = endpoint_url;
                basicAuthConfiguration.url_int = url_int;
                basicAuthConfiguration.users.forEach(user => {
                    user.password_type = "plain";
                });

                if (basicAuthConfiguration.hasOwnProperty("endpoint_name")) {
                    me.deleteConfiguration(basicAuthConfiguration).then(function () {
                        basicAuthConfiguration.objects = layers;
                        if (layers.length > 0) {
                            me.addBasicAuthConfiguration(basicAuthConfiguration).then(function (resultArray) {
                                if (resultArray.length > 0) {
                                    Ext.Msg.alert("Konfiguration erfolgreich", "Die Basic Auth Konfiguration wurde erfolgreich geändert.", Ext.emptyFn);
                                }
                                else {
                                    Ext.Msg.alert("Konfiguration nicht erfolgreich", "Die Basic Auth Konfiguration wurde nicht geändert.", Ext.emptyFn);
                                }
                                me.getBasicAuthConfiguration(service_id, endpoint_name);
                            });
                        }
                        else {
                            me.updateServiceDetailsView();
                            Ext.Msg.alert("Konfiguration gelöscht", "Die Basic Auth Konfiguration wurde gelöscht, da keine Layer mehr vorhanden sind.", Ext.emptyFn);
                            me.getBasicAuthConfiguration(service_id, endpoint_name);
                        }
                    });
                }
            });
        }
    }
});
