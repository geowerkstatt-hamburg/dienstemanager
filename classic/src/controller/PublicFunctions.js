Ext.define("DiensteManager.controller.PublicFunctions", {
    extend: "Ext.app.Controller",
    alias: "controller.publicfunctions",

    setDscStatus: function (service_id) {
        Ext.Ajax.request({
            url: "backend/setdscstatus?service_id=" + service_id,
            method: "GET",
            success: function () {
                var service = Ext.getStore("ServiceList").query("service_id", service_id, false, false, true).items[0].data;

                if ((service.software === "ESRI" && service.service_type === "WMS") || (service.software === "deegree")) {
                    Ext.getCmp("getINSPIRECaps").setDisabled(false);
                }

                Ext.getStore("ServiceList").reload();
            },
            failure: function (response) {
                console.log(Ext.decode(response.responseText));
            }
        });
    },

    updateLayer: function (service_id, layer_id, selected, nameident) {

        selected.service_id = service_id;
        selected.layer_id = layer_id;
        selected.last_edited_by = window.auth_user;
        selected.last_edit_date = moment().format("YYYY-MM-DD HH:mm");

        Ext.Ajax.request({
            url: "backend/updatelayer",
            method: "POST",
            jsonData: selected,
            success: function () {
                if (!nameident) {

                    var i = Ext.getStore("NewLayer").findExact("layer_name", selected.layer_name);

                    if (i !== -1) {
                        Ext.getStore("NewLayer").removeAt(i);
                    }
                }
                Ext.getStore("Layers").reload({
                    params: {service_id: service_id}
                });
                Ext.getCmp("servicelasteditedby").setValue(window.auth_user);
                Ext.getCmp("servicelasteditdate").setValue(moment().format("YYYY-MM-DD HH:mm"));
            },
            failure: function (response) {
                console.log(Ext.decode(response.responseText));
            }
        });
    },

    openServiceDetailsWindow: function (service, select_layer_id) {
        Ext.getStore("Layers").removeAll();
        Ext.getStore("ServiceTestResults").removeAll();
        Ext.getStore("ServiceTestResultsChecked").removeAll();
        Ext.getStore("Comments").removeAll();
        Ext.getStore("LinkedFreigaben").removeAll();
        Ext.getStore("LayerLinks").removeAll();
        Ext.getStore("Portals").removeAll();

        if (DiensteManager.Config.getAnmerkungenModul()) {
            Ext.getStore("ServiceTestResults").load({
                params: {service_id: service.service_id},
                callback: function (records) {
                    Ext.getStore("ServiceTestResultsChecked").load({
                        params: {service_id: service.service_id},
                        callback: function (records_checked) {
                            if (records.length > 0) {
                                var checked_messages = 0;

                                for (var i = 0; i < records.length; i++) {
                                    for (var j = 0; j < records_checked.length; j++) {
                                        if (records[i].data.service_id === records_checked[j].data.service_id && records[i].data.text === records_checked[j].data.text) {
                                            checked_messages = checked_messages + 1;
                                        }
                                    }
                                }

                                if (records.length > checked_messages) {
                                    Ext.MessageBox.alert("Achtung", "Es wurden Fehler oder Unstimmigkeiten entdeckt!");
                                }
                            }
                        }
                    });
                }
            });
        }

        Ext.getStore("Comments").load({
            params: {service_id: service.service_id}
        });

        if (DiensteManager.Config.getZugriffsstatistikModul()) {
            Ext.getStore("Visits").load({
                params: {service_id: service.service_id}
            });
        }

        if (DiensteManager.Config.getFreigabenModul()) {
            Ext.getStore("LinkedFreigaben").load({
                params: {service_id: service.service_id}
            });
        }

        if (DiensteManager.Config.getJiraModul()) {
            Ext.getStore("JiraTickets").load({
                params: {service_id: service.service_id}
            });
        }

        Ext.getStore("LinkedContacts").load({
            params: {service_id: service.service_id},
            callback: function (records) {
                Ext.getCmp("servicecontacts").setValue(records);
                Ext.getCmp("save_service_details_button").setDisabled(true);
            }
        });

        Ext.Ajax.request({
            url: "backend/getkeywords?service_id=" + service.service_id,
            method: "GET",
            success: function (response) {
                var jsonobj = Ext.decode(response.responseText),
                    keyword_array = [];

                for (var i = 0; i < jsonobj.length; i++) {
                    keyword_array.push(jsonobj[i].text);
                }
                if (keyword_array.length > 0) {
                    Ext.getCmp("servicekeywords").setValue(keyword_array);
                }
            }
        });

        var servicedetailswindow = Ext.getCmp("servicedetails-window");

        if (servicedetailswindow) {
            servicedetailswindow.destroy();
        }

        servicedetailswindow = Ext.create("servicedetails.ServiceDetails");

        Ext.getCmp("servicedetails-form").getForm().setValues({
            servicecheckedstatus: service.service_checked_status,
            serviceid: service.service_id,
            servicelasteditdate: service.last_edit_date,
            servicetitle: service.title,
            servicedescription: service.description,
            servicefees: service.fees,
            serviceaccessconstraints: service.access_constraints,
            serviceeditor: service.editor,
            serviceresponsibleparty: service.responsible_party,
            servicelasteditedby: service.last_edited_by,
            servicestatus: service.status,
            servicetype: service.service_type,
            serviceversion: service.version,
            servicesoftware: service.software,
            serviceserver: service.server,
            servicefolder: service.folder,
            servicename: service.service_name,
            serviceurlint: service.url_int,
            serviceurlext: service.url_ext,
            serviceurlsec: service.url_sec,
            servicesecuritytype: service.security_type,
            serviceonlyintranet: service.only_intranet,
            serviceonlyinternet: service.only_internet,
            servicedatasource: service.datasource,
            servicemappingdescription: service.mapping_description,
            servicecached: service.cached,
            serviceinspirerelevant: service.inspire_relevant,
            serviceqa: service.qa,
            externalService: service.external
        });

        Ext.getStore("Layers").load({
            params: {service_id: service.service_id},
            callback: function (records) {
                if (records.length > 0) {
                    var layer_grid = Ext.getCmp("layers-grid");

                    if (DiensteManager.Config.getAnmerkungenModul() && (service.service_type === "WMS" || service.service_type === "WFS") && !window.read_only) {
                        Ext.getCmp("startservicequickcheck").setDisabled(false);
                    }

                    if (select_layer_id) {
                        Ext.getCmp("servicedetails-tabpanel").setActiveTab("servicedetails-layers");
                        for (var i = 0; i < records.length; i++) {
                            if (records[i].data.layer_id === select_layer_id) {
                                layer_grid.fireEvent("cellclick", layer_grid, null, 0, records[i]);
                                var row = layer_grid.store.indexOf(records[i]);

                                layer_grid.getSelectionModel().select(row);
                                layer_grid.ensureVisible(row);
                                break;
                            }
                        }
                    }
                    else {
                        layer_grid.getSelectionModel().select(0);
                        for (var j = 0; j < records.length; j++) {
                            if (records[j].data.layer_id === layer_grid.getSelectionModel().getSelection()[0].data.layer_id) {
                                layer_grid.fireEvent("cellclick", layer_grid, null, 0, records[j]);
                                break;
                            }
                        }
                    }
                }
            }
        });

        if (DiensteManager.Config.getMetadatenModul()) {
            if (service.service_md_id !== "") {
                Ext.getCmp("metadata_uuid").setValue(service.service_md_id);
                Ext.getCmp("servicesourcecswname").setValue(service.csw_name);
                Ext.getCmp("metadata_uuid_link").setValue("<a href=\"" + service.show_doc_url + service.service_md_id + "\" target=\"_blank\">Metadaten anzeigen</a>");
                Ext.getCmp("deleteMetadataCoupling").setDisabled(false);
            }
        }

        var versioncombo = Ext.getCmp("serviceversion"),
            versionstore = DiensteManager.Config.getDienstVersionen()[service.service_type];

        versioncombo.bindStore(versionstore);

        servicedetailswindow.show();

        if (DiensteManager.Config.getMetadatenModul()) {
            if ((service.software === "ESRI" && service.service_type !== "WFS") || (service.software === "deegree")) {
                Ext.getCmp("getINSPIRECaps").setDisabled(false);
            }
        }

        Ext.getCmp("servicedetails-dataupdatestatus-form").getForm().setValues({
            dataupdatestatuslastupdate: service.update_date,
            dataupdatestatusrefreshnumber: service.update_number,
            dataupdatestatusrefreshunit: service.update_unit,
            dataupdatestatusprocesstype: service.update_process_type,
            dataupdatestatusprocessname: service.update_process_name,
            dataupdatestatusprocesslocation: service.update_process_location,
            dataupdatestatusprocessdescription: service.update_process_description
        });

        if (service.update_type === "manuell") {
            Ext.getCmp("dataupdatestatusautomatically").setValue(false);
            Ext.getCmp("dataupdatestatusrealtime").setValue(false);
            Ext.getCmp("dataupdatestatusmanually").setValue(true);
        }
        else if (service.update_type === "automatisch") {
            Ext.getCmp("dataupdatestatusrealtime").setValue(false);
            Ext.getCmp("dataupdatestatusmanually").setValue(false);
            Ext.getCmp("dataupdatestatusautomatically").setValue(true);
        }
        else if (service.update_type === "echtzeit") {
            Ext.getCmp("dataupdatestatusautomatically").setValue(false);
            Ext.getCmp("dataupdatestatusmanually").setValue(false);
            Ext.getCmp("dataupdatestatusrealtime").setValue(true);
        }

        if (service.update_unit === "unregelmäßig") {
            Ext.getCmp("dataupdatestatusrefreshnumber").setDisabled(true);
        }
        if (service.update_number === 0) {
            Ext.getCmp("dataupdatestatusrefreshnumber").setValue("");
        }

        if (DiensteManager.Config.getZugriffsstatistikModul()) {
            var sprite_width = Ext.Element.getViewportWidth() - 100;

            if (sprite_width > 1470) {
                sprite_width = 1470;
            }
            Ext.getCmp("serviceVisitsChart").setSprites([{
                type: "text",
                text: "Zugriffe auf \"" + service.title + "\"",
                fontSize: 22,
                width: sprite_width,
                height: 30,
                x: 40,
                y: 20
            }]);
        }

        if (window.read_only) {
            Ext.getCmp("updatelayer").setDisabled(true);
            Ext.getCmp("getINSPIRECaps").setDisabled(true);
            Ext.getCmp("savelayerdetails").setDisabled(true);
            Ext.getCmp("linkdatasetbutton").setDisabled(true);
            Ext.getCmp("editGFIConfig").setDisabled(true);
            Ext.getCmp("layerdeleterequest").setDisabled(true);
        }

        Ext.getCmp("save_service_details_button").setDisabled(true);
    },

    xmlToString: function (xmlData) {
        var xmlString;

        if (window.ActiveXObject) {
            xmlString = xmlData.xml;
        }
        else {
            xmlString = new XMLSerializer().serializeToString(xmlData);
        }
        return xmlString;
    }
});
