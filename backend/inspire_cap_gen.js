var axios = require("axios"),
    https = require("https"),
    database = require("./database"),
    format = require("xml-formatter"),
    debug = require("debug")("DM:inspire_cap_gen"),
    fs = require("fs"),
    _ = require("underscore"),
    config = require("./../config.js"),
    xmldom = require("xmldom"),
    DOMParser = xmldom.DOMParser,
    XMLSerializer = xmldom.XMLSerializer,
    path = require("path"),
    inspire_cap_gen;

inspire_cap_gen = {
    updateDeegreeCapabilities: function (service, res) {
        var serverList = _.where(config.client_config.server, {name: service.server}),
            upload_urls = [];

        if (serverList[0].lb === true) {
            for (var i = 0; i < serverList[0].server.length; i++) {
                var server = serverList[0].server[i];

                upload_urls.push("http://" + server + "/" + service.folder + "/config/upload/services/" + service.service_name + "_metadata.xml");
            }
        }
        else {
            upload_urls.push("http://" + serverList[0].name + "." + serverList[0].domain + "/" + service.folder + "/config/upload/services/" + service.service_name + "_metadata.xml");
        }

        database.getLinkedDatasets(service.service_id).then(
            function (datasets) {
                database.getCapabilitiesKeywordList(service.service_id).then(
                    function (keywords) {
                        var template_data = {
                            service_md_id: service.service_md_id,
                            title: service.title,
                            description: service.description,
                            keywords: keywords,
                            fees: service.fees,
                            accessConstraints: service.access_constraints,
                            capabilitiesMetadata: config.capabilities_metadata,
                            linkedDatasets: datasets
                        };

                        fs.readFile(path.join(__dirname, "templates/deegree_service_" + service.service_type.toLowerCase() + "_metadata.xml"), "utf-8", function (error, source) {
                            var template = _.template(source),
                                promises_upload = [];

                            upload_urls.forEach(function (url) {
                                promises_upload.push(axios({
                                    method: "post",
                                    url: url,
                                    data: template(template_data),
                                    auth: config.tomcatUser,
                                    httpsAgent: new https.Agent({
                                        rejectUnauthorized: config.reject_unauthorized_SSL === null || config.reject_unauthorized_SSL === undefined ? false : config.reject_unauthorized_SSL
                                    })
                                }));
                            });

                            axios.all(promises_upload).then(function () {
                                if (res) {
                                    return res.json({deegree_upload_success: true});
                                }
                            }).catch(function (promise_error) {
                                debug("Save deegree Metadata", promise_error);
                                if (res) {
                                    return res.json({deegree_upload_success: false});
                                }
                            });
                        });
                    }
                );
            }
        );
    },

    createCapabilitiesDoc: function (service, res) {
        axios({
            method: "get",
            url: service.url_int + "?Service=WMS&Request=GetCapabilities",
            httpsAgent: new https.Agent({
                rejectUnauthorized: config.reject_unauthorized_SSL === null || config.reject_unauthorized_SSL === undefined ? false : config.reject_unauthorized_SSL
            })
        }).then(function (capabilities_response) {
            database.getLinkedDatasets(service.service_id).then(
                function (datasets) {
                    var xmlString = capabilities_response.data.replace(new RegExp("<!--.*?-->", "g"), ""),
                        tmp1 = xmlString.split("<WMS_Capabilities"),
                        tmp2 = tmp1[1].split("<Service>"),
                        newCaps = tmp1[0] + "<WMS_Capabilities version=\"1.3.0\" xmlns=\"http://www.opengis.net/wms\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" ";

                    if (tmp2[0].indexOf("xlink") > -1) {
                        newCaps += "xmlns:xlink=\"http://www.w3.org/1999/xlink\" ";
                    }
                    if (tmp2[0].indexOf("sld") > -1) {
                        newCaps += "xmlns:sld=\"http://www.opengis.net/sld\" ";
                    }

                    if (tmp2[0].indexOf("esri") > -1) {
                        if (tmp2[0].indexOf("sld") > -1) {
                            newCaps += "xmlns:esri_wms=\"http://www.esri.com/wms\" xsi:schemaLocation=\"http://inspire.ec.europa.eu/schemas/inspire_vs/1.0 http://inspire.ec.europa.eu/schemas/inspire_vs/1.0/inspire_vs.xsd http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/sld_capabilities.xsd http://www.esri.com/wms " + service.url_int + "?version=1.3.0&amp;service=WMS&amp;request=GetSchemaExtension\">" +
                                    "<Service>" + tmp2[1];
                        }
                        else {
                            newCaps += "xmlns:esri_wms=\"http://www.esri.com/wms\" xsi:schemaLocation=\"http://inspire.ec.europa.eu/schemas/inspire_vs/1.0 http://inspire.ec.europa.eu/schemas/inspire_vs/1.0/inspire_vs.xsd http://www.esri.com/wms " + service.url_int + "?version=1.3.0&amp;service=WMS&amp;request=GetSchemaExtension\">" +
                                    "<Service>" + tmp2[1];
                        }
                    }
                    else {
                        if (tmp2[0].indexOf("sld") > -1) {
                            newCaps += "xsi:schemaLocation=\"http://inspire.ec.europa.eu/schemas/inspire_vs/1.0 http://inspire.ec.europa.eu/schemas/inspire_vs/1.0/inspire_vs.xsd http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/sld_capabilities.xsd\">" +
                                    "<Service>" + tmp2[1];
                        }
                        else {
                            newCaps += "xsi:schemaLocation=\"http://inspire.ec.europa.eu/schemas/inspire_vs/1.0 http://inspire.ec.europa.eu/schemas/inspire_vs/1.0/inspire_vs.xsd\">" +
                                    "<Service>" + tmp2[1];
                        }
                    }

                    var tmp3 = newCaps.split("</Exception>");

                    newCaps = tmp3[0] +
                        "</Exception>\n" +
                        "\t\t<inspire_vs:ExtendedCapabilities xmlns:inspire_vs=\"http://inspire.ec.europa.eu/schemas/inspire_vs/1.0\" " +
                        "xmlns:inspire_common=\"http://inspire.ec.europa.eu/schemas/common/1.0\" " +
                        "xsi:schemaLocation=\"http://inspire.ec.europa.eu/schemas/common/1.0 http://inspire.ec.europa.eu/schemas/common/1.0/common.xsd " +
                        "http://inspire.ec.europa.eu/schemas/inspire_vs/1.0 http://inspire.ec.europa.eu/schemas/inspire_vs/1.0/inspire_vs.xsd\">\n" +
                        "\t\t\t<inspire_common:MetadataUrl>\n" +
                        "\t\t\t\t<inspire_common:URL>" + config.capabilities_metadata.metadataurl + "?Service=CSW&amp;Request=GetRecordById&amp;Version=2.0.2&amp;id=" + service.service_md_id + "&amp;outputSchema=http://www.isotc211.org/2005/gmd&amp;elementSetName=full</inspire_common:URL>\n" +
                        "\t\t\t\t<inspire_common:MediaType>application/vnd.ogc.csw.GetRecordByIdResponse_xml</inspire_common:MediaType>\n" +
                        "\t\t\t</inspire_common:MetadataUrl>\n" +
                        "\t\t\t<inspire_common:SupportedLanguages>\n" +
                        "\t\t\t\t<inspire_common:DefaultLanguage>\n" +
                        "\t\t\t\t\t<inspire_common:Language>ger</inspire_common:Language>\n" +
                        "\t\t\t\t</inspire_common:DefaultLanguage>\n" +
                        "\t\t\t</inspire_common:SupportedLanguages>\n" +
                        "\t\t\t<inspire_common:ResponseLanguage>\n" +
                        "\t\t\t\t<inspire_common:Language>ger</inspire_common:Language>\n" +
                        "\t\t\t</inspire_common:ResponseLanguage>\n" +
                        "\t\t</inspire_vs:ExtendedCapabilities>\n";

                    if (tmp3[1].indexOf("inspire_vs") > -1) {
                        var tmp4 = tmp3[1].split("ExtendedCapabilities>");

                        newCaps += tmp4[1];
                    }
                    else {
                        newCaps += tmp3[1];
                    }

                    var capabilities_doc = new DOMParser().parseFromString(newCaps, "text/xml");

                    capabilities_doc.getElementsByTagName("Title")[0].firstChild.data = service.title;
                    capabilities_doc.getElementsByTagName("Abstract")[0].firstChild.data = service.description;

                    if (capabilities_doc.getElementsByTagName("Fees")[0].firstChild) {
                        capabilities_doc.getElementsByTagName("Fees")[0].firstChild.data = service.fees;
                    }
                    else {
                        capabilities_doc.getElementsByTagName("Fees")[0].appendChild(capabilities_doc.createTextNode(service.fees));
                    }

                    if (capabilities_doc.getElementsByTagName("AccessConstraints")[0].firstChild) {
                        capabilities_doc.getElementsByTagName("AccessConstraints")[0].firstChild.data = service.access_constraints;
                    }
                    else {
                        capabilities_doc.getElementsByTagName("AccessConstraints")[0].appendChild(capabilities_doc.createTextNode(service.access_constraints));
                    }

                    var layers = capabilities_doc.getElementsByTagName("Layer");

                    for (var i = 0; i < layers.length; i++) {
                        if (layers[i].getElementsByTagName("AuthorityURL").length > 0) {
                            capabilities_doc.documentElement.removeChild(layers[i].getElementsByTagName("AuthorityURL")[0]);
                        }
                        if (layers[i].getElementsByTagName("Identifier").length > 0) {
                            capabilities_doc.documentElement.removeChild(layers[i].getElementsByTagName("Identifier")[0]);
                        }
                        if (layers[i].getElementsByTagName("MetadataURL").length > 0) {
                            capabilities_doc.documentElement.removeChild(layers[i].getElementsByTagName("MetadataURL")[0]);
                        }

                        if (layers[i].getElementsByTagName("Layer").length === 0) {
                            var layer_name = layers[i].getElementsByTagName("Name")[0].firstChild.nodeValue,
                                auth = "<AuthorityURL name=\"" + config.capabilities_metadata.authorityname + "\">\n" +
                                    "\t\t\t\t<OnlineResource xmlns:xlink=\"http://www.w3.org/1999/xlink\" xlink:href=\"" + config.capabilities_metadata.authorityurl + "\" xlink:type=\"simple\"/>\n" +
                                    "\t\t\t</AuthorityURL>\n",
                                style = layers[i].getElementsByTagName("Style")[0],
                                maxScaleDenominator = layers[i].getElementsByTagName("MaxScaleDenominator")[0],
                                minScaleDenominator = layers[i].getElementsByTagName("MinScaleDenominator")[0];

                            capabilities_doc.documentElement.removeChild(style);
                            if (maxScaleDenominator) {
                                capabilities_doc.documentElement.removeChild(maxScaleDenominator);
                            }
                            if (minScaleDenominator) {
                                capabilities_doc.documentElement.removeChild(minScaleDenominator);
                            }

                            layers[i].appendChild(new DOMParser().parseFromString(auth, "text/xml"));

                            var layer_datasets = _.where(datasets, {layer_name: layer_name});

                            for (var j = 0; j < layer_datasets.length; j++) {
                                var identifier = "\t\t\t<Identifier authority=\"" + config.capabilities_metadata.authorityname + "\">" + layer_datasets[j].rs_id + "</Identifier>\n",
                                    metaUrl = "\t\t\t<MetadataURL type=\"ISO19115:2003\">\n" +
                                    "\t\t\t\t<Format>application/xml</Format>\n" +
                                    "\t\t\t\t<OnlineResource xmlns:xlink=\"http://www.w3.org/1999/xlink\" xlink:href=\"" + config.capabilities_metadata.metadataurl + "?service=CSW&amp;request=GetRecordById&amp;version=2.0.2&amp;id=" + layer_datasets[j].md_id + "&amp;outputSchema=http://www.isotc211.org/2005/gmd&amp;elementSetName=full\" xlink:type=\"simple\"/>\n" +
                                    "\t\t\t</MetadataURL>\n\t\t\t";

                                layers[i].appendChild(new DOMParser().parseFromString(identifier, "text/xml"));
                                layers[i].appendChild(new DOMParser().parseFromString(metaUrl, "text/xml"));
                            }

                            layers[i].appendChild(style);
                            if (minScaleDenominator) {
                                layers[i].appendChild(minScaleDenominator);
                            }
                            if (maxScaleDenominator) {
                                layers[i].appendChild(maxScaleDenominator);
                            }
                        }
                    }

                    var cap_string = new XMLSerializer().serializeToString(capabilities_doc);

                    cap_string = cap_string.replace(/<!\[CDATA\[/g, "").replace(/\]\]>/g, "");
                    cap_string = cap_string.replace(new RegExp("xmlns=\"\" ", "g"), "");
                    cap_string = cap_string.replace(new RegExp("%26", "g"), "&amp;");
                    cap_string = cap_string.replace(new RegExp("\\s+", "g"), " ");
                    cap_string = cap_string.replace(new RegExp("<Name> ", "g"), "<Name>");
                    cap_string = cap_string.replace(new RegExp("<Title> ", "g"), "<Title>");
                    cap_string = cap_string.replace(new RegExp("<Abstract> ", "g"), "<Abstract>");
                    cap_string = cap_string.replace(new RegExp(" </Name>", "g"), "</Name>");
                    cap_string = cap_string.replace(new RegExp(" </Title>", "g"), "</Title>");
                    cap_string = cap_string.replace(new RegExp(" </Abstract>", "g"), "</Abstract>");

                    return res.send(format(cap_string, {collapseContent: true, lineSeparator: "\n"}));
                }
            );
        }).catch(function (error) {
            debug("Generate INSPIRE Capabilities Attributes", error);
            return res.json({error: true});
        });
    }
};

module.exports = inspire_cap_gen;
