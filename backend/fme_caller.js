var axios = require("axios"),
    config = require("./../config.js"),
    debug = require("debug")("DM:fme_caller"),
    fme_caller;

fme_caller = {
    createGeoproxyConf: function (res) {
        axios.get(config.fme.geoproxy_conf_update).then(function () {
            if (res !== undefined) {
                return res.json({success: true});
            }
        }).catch(function (error) {
            debug("Geoproxy conf update", error);
            if (res !== undefined) {
                return res.json({error: true});
            }
        });
    },

    updateFreigaben: function (res) {
        axios.get(config.fme.freigaben_update).then(function () {
            return res.json({success: true});
        }).catch(function (error) {
            debug("Freigaben update", error);
            return res.json({error: true});
        });
    }
};

module.exports = fme_caller;
