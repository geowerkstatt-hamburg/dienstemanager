var tunnel = require("tunnel"),
    https = require("https"),
    config = require("../config.js"),
    https_agent;

https_agent = {

    getHttpsAgent: function (proxy) {
        if (config.use_https_tunnel_proxy && proxy) {
            return tunnel.httpsOverHttp({
                proxy: proxy,
                rejectUnauthorized: false
            });
        }
        else {
            return new https.Agent({
                rejectUnauthorized: config.reject_unauthorized_SSL === null || config.reject_unauthorized_SSL === undefined ? false : config.reject_unauthorized_SSL
            });
        }
    }
};

module.exports = https_agent;
