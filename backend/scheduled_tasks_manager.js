var database = require("./database"),
    schedule = require("node-schedule"),
    debug = require("debug")("DM:scheduled_task_manager"),
    scheduled_tasks = {},
    scheduled_tasks_manager;

scheduled_tasks_manager = {
    init: function () {
        const me = this;

        database.getScheduledTasks().then(function (results) {
            const cronregex = new RegExp(/^(\*|([0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9])|\*\/([0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9])) (\*|([0-9]|1[0-9]|2[0-3])|\*\/([0-9]|1[0-9]|2[0-3])) (\*|([1-9]|1[0-9]|2[0-9]|3[0-1])|\*\/([1-9]|1[0-9]|2[0-9]|3[0-1])) (\*|([1-9]|1[0-2])|\*\/([1-9]|1[0-2])) (\*|([0-6])|\*\/([0-6]))$/);

            for (const scheduled_task of results) {
                if (scheduled_task.enabled && cronregex.test(scheduled_task.schedule)) {
                    me._scheduleTask(scheduled_task);
                }
            }
        });
    },

    reschedule: function (tasks) {
        const me = this;

        for (let i = 0; i < tasks.length; i++) {
            if (scheduled_tasks[tasks[i].name] && tasks[i].enabled) {
                scheduled_tasks[tasks[i].name].reschedule(tasks.schedule);
            }
            else if (tasks[i].enabled) {
                me._scheduleTask(tasks[i]);
            }
            else if (scheduled_tasks[tasks[i].name] && !tasks[i].enabled) {
                scheduled_tasks[tasks[i].name].cancel();
                delete scheduled_tasks[tasks[i].name];
                debug("Scheduled Task " + scheduled_tasks[tasks[i].name] + " canceled");
            }
        }
    },

    _scheduleTask: function (scheduled_task) {
        const scheduled_task_obj = require("./scheduled_tasks/" + scheduled_task.name);

        scheduled_tasks[scheduled_task.name] = schedule.scheduleJob(scheduled_task.schedule, function () {
            scheduled_task_obj.execute();
        });
        debug("Scheduled Task " + scheduled_task.name + " initialized");
    }
};

module.exports = scheduled_tasks_manager;
