var axios = require("axios"),
    config = require("./../config.js"),
    debug = require("debug")("DM:service_caller"),
    database = require("./database"),
    layer_json_gen = require("./layer_json_gen"),
    xmldom = require("xmldom"),
    https_agent = require("./https_agent"),
    DOMParser = xmldom.DOMParser,
    xpath = require("xpath"),
    capabilities_parser = require("./capabilities_parser");

var service_caller = {

    getGFIAttributes: function (url, version, type, layername, externalService, res) {
        var gfi_url,
            proxy_settings = externalService ? config.proxy : false,
            https_url = url.indexOf("https:") > -1,
            httpsAgent = https_url ? https_agent.getHttpsAgent(proxy_settings) : null;

        if (type === "WMS") {
            if (url.indexOf("?") > -1) {
                gfi_url = url + "&request=GetFeatureInfoSchema&version=" + version + "&layers=" + layername;
            }
            else {
                gfi_url = url + "?request=GetFeatureInfoSchema&version=" + version + "&layers=" + layername;
            }
        }

        if (type === "WFS") {
            if (url.indexOf("?") > -1) {
                gfi_url = url + "&service=WFS&request=DescribeFeatureType&version=" + version + "&typename=" + layername;
            }
            else {
                gfi_url = url + "?service=WFS&request=DescribeFeatureType&version=" + version + "&typename=" + layername;
            }
        }

        axios({
            method: "get",
            url: gfi_url,
            proxy: config.use_https_tunnel_proxy && https_url ? false : proxy_settings,
            httpsAgent: httpsAgent
        }).then(function (gml_schema_response) {
            var gml_schema_doc = new DOMParser().parseFromString(gml_schema_response.data, "text/xml"),
                gfi_attributes = [],
                select = xpath.useNamespaces({"x": "http://www.w3.org/2001/XMLSchema"}),
                elements = select("//x:element/x:complexType/x:complexContent/x:extension/x:sequence/x:element", gml_schema_doc);

            if (gfi_url.indexOf("service=WFS") > -1 && layername.split(":").length > 1) {
                elements = select("//x:element[@name='" + layername.split(":")[1] + "']/x:complexType/x:complexContent/x:extension/x:sequence/x:element", gml_schema_doc);
                if (elements.length === 0) {
                    elements = select("//x:complexType[@name='" + layername.split(":")[1] + "']/x:complexContent/x:extension/x:sequence/x:element", gml_schema_doc);
                }
            }

            for (var i = 0; i < elements.length; i++) {
                var featureName = select("string(@name)", elements[i]),
                    featureType = select("string(@type)", elements[i]);

                if (featureType.indexOf("gml") === -1) {
                    gfi_attributes.push(
                        {
                            attr_name: featureName,
                            map_name: featureName
                        }
                    );
                }
            }

            return res.json(gfi_attributes);
        }).catch(function (error) {
            debug("Get GFI Attributes", error);
            return res.json({error: true});
        });
    },

    updateServiceLayers: function (url, service_id, service_type, software, version, url_ext, is_mrh, externalService, res) {
        var headers = null,
            gc_url,
            proxy_settings = software === "extern" || externalService ? config.proxy : false,
            https_url = url.indexOf("https:") > -1,
            httpsAgent = https_url ? https_agent.getHttpsAgent(proxy_settings) : null;

        if (service_type === "WMS") {
            if (url.indexOf("?") > -1) {
                gc_url = url + "&Service=WMS&Request=GetCapabilities&Version=" + version;
            }
            else {
                gc_url = url + "?Service=WMS&Request=GetCapabilities&Version=" + version;
            }
        }

        if (service_type === "WMTS" && version === "1.0.0") {
            if (url.indexOf("?") > -1) {
                gc_url = url + "&Service=WMTS&Request=GetCapabilities&Version=" + version;
            }
            else {
                gc_url = url + "?Service=WMTS&Request=GetCapabilities&Version=" + version;
            }
        }

        if (service_type === "WFS") {
            if (url.indexOf("?") > -1) {
                gc_url = url + "&Service=WFS&Request=GetCapabilities&Version=" + version;
            }
            else {
                gc_url = url + "?Service=WFS&Request=GetCapabilities&Version=" + version;
            }
        }

        if (is_mrh) {
            headers = {"X-Alt-Referer": "http://geoportal.metropolregion.hamburg.de/mrhportal"};
        }

        axios({
            method: "get",
            url: gc_url,
            headers: headers,
            proxy: config.use_https_tunnel_proxy && https_url ? false : proxy_settings,
            httpsAgent: httpsAgent
        }).then(function (capabilities) {
            var cap_data;

            if (service_type === "WFS") {
                cap_data = capabilities_parser.parseWFSCapabilities(capabilities.data, true, version);
            }
            if (service_type === "WMS") {
                cap_data = capabilities_parser.parseWMSCapabilities(capabilities.data, true, version);
            }
            if (service_type === "WMTS") {
                cap_data = capabilities_parser.parseWMTSCapabilities(capabilities.data, true, version);
            }

            if (cap_data.version_check) {
                cap_data.layer_list.forEach(function (layer) {
                    layer.type = layer.group_layer ? "groupLayer" : "layer";
                    layer.service_id = service_id;

                    if (software === "ESRI" && config.esri_url_namespace) {
                        layer.namespace = url_ext;
                    }
                });

                return res.json({success: true, layers: cap_data.layer_list});
            }
            else {
                return res.json({success: false, error: "Dienst Version stimmt nicht überein."});
            }
        }).catch(function (error) {
            debug("Update Service Layers", error);

            var return_json;

            if (error.response) {
                // Request made and server responded
                return_json = {success: false, error: error.response.statusText};
            }
            else if (error.request) {
                // The request was made but no response was received
                return_json = {
                    success: false,
                    error: "Netzwerkfehler für " + error.address + ":" + error.port + ": " + error.code
                };
            }
            else {
                // Something happened in setting up the request that triggered an Error
                return_json = {success: false, error: error.message};
            }
            return res.json(return_json);
        });
    },

    addLayersToService: function (service_id, service_type, layer_list, last_edited_by, last_edit_date, res) {
        database.getServiceLayers(service_id).then(
            async function (service_layers) {
                if (service_layers.length === 0) {
                    for (let i = 0; i < layer_list.length; i++) {
                        if (service_type === "WMS" || service_type === "WMTS" || service_type === "WFS" || service_type === "Terrain3D" || service_type === "TileSet3D" || service_type === "Oblique" || service_type === "SensorThings" || service_type === "VectorTiles") {
                            await database.addLayerWithType(layer_list[i]).then(
                                function (resultObject) {
                                    layer_json_gen.prepare(service_id, resultObject[0].layer_id);
                                }
                            );
                        }
                        else {
                            await database.addLayer(layer_list[i]).then(
                                function (resultObject) {
                                    layer_json_gen.prepare(service_id, resultObject[0].layer_id);
                                }
                            );
                        }

                    }
                    database.setLastEditedBy([service_id, last_edited_by, last_edit_date]);

                    var return_json = {success: true, status: "layer_insert_complete"};

                    if (layer_list.length === 0) {
                        return_json = {success: true, status: "no_layers_found"};
                    }
                    return res.json(return_json);
                }
                else {
                    var ident = true,
                        msg = "Keine neuen Layer";

                    if (layer_list.length !== service_layers.length) {
                        ident = false;
                    }
                    else {
                        var num_inside = 0;

                        for (let i = 0; i < layer_list.length; i++) {
                            for (let j = 0; j < service_layers.length; j++) {
                                if (layer_list[i].layer_name === service_layers[j].layer_name && layer_list[i].title === service_layers[j].title && layer_list[i].namespace === service_layers[j].namespace && layer_list[i].scale_min === service_layers[j].scale_min && layer_list[i].scale_max === service_layers[j].scale_max) {
                                    num_inside++;
                                    break;
                                }
                                else if (layer_list[i].layer_name === service_layers[j].layer_name && (layer_list[i].title !== service_layers[j].title || layer_list[i].namespace !== service_layers[j].namespace || layer_list[i].scale_min !== service_layers[j].scale_min || layer_list[i].scale_max !== service_layers[j].scale_max)) {
                                    await database.updateLayer({
                                        layer_name: layer_list[i].layer_name,
                                        title: layer_list[i].title,
                                        scale_max: layer_list[i].scale_max,
                                        scale_min: layer_list[i].scale_min,
                                        layer_id: service_layers[j].layer_id
                                    }).then(
                                        function (resultObject) {
                                            layer_json_gen.prepare(service_id, resultObject[0].layer_id);
                                        }
                                    );

                                    if (service_type === "WFS") {
                                        await database.updateLayerNamespace({namespace: layer_list[i].namespace, layer_id: service_layers[j].layer_id}).then(
                                            function (resultObject) {
                                                layer_json_gen.prepare(service_id, resultObject[0].layer_id);
                                            }
                                        );
                                    }

                                    msg = "Keine neuen Layer, Parameter wurden aktualisiert!";
                                    num_inside++;
                                    break;
                                }
                            }
                        }
                        if (num_inside !== layer_list.length) {
                            ident = false;
                        }
                    }

                    if (!ident) {
                        return res.json({success: true, status: "layer_update_needed", new_layer: layer_list});
                    }
                    else {
                        return res.json({success: true, status: "layer_update_complete", msg: msg});
                    }
                }
            }
        ).catch(function (error) {
            debug("Add Service Layers", error);
            return res.json({success: false});
        });
    }
};

module.exports = service_caller;
