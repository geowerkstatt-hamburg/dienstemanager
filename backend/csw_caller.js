var axios = require("axios"),
    https_agent = require("./https_agent"),
    config = require("./../config.js"),
    database = require("./database"),
    metadata_parser = require("./metadata_parser"),
    layer_json_gen = require("./layer_json_gen"),
    _ = require("underscore"),
    debug = require("debug")("DM:csw_caller"),
    csw_caller;

csw_caller = {
    getServiceMetadata: function (title, source_csw_name, res) {
        var csw_request = `<csw:GetRecords xmlns:csw="http://www.opengis.net/cat/csw/2.0.2" xmlns:ogc="http://www.opengis.net/ogc" service="CSW" version="2.0.2" resultType="results" startPosition="1" maxRecords="20" outputFormat="application/xml" outputSchema="http://www.isotc211.org/2005/gmd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opengis.net/cat/csw/2.0.2 http://schemas.opengis.net/csw/2.0.2/CSW-discovery.xsd" xmlns:gmd="http://www.isotc211.org/2005/gmd" xmlns:apiso="http://www.opengis.net/cat/csw/apiso/1.0">
            <csw:Query typeNames="csw:Record">
            <csw:ElementSetName>full</csw:ElementSetName>
            <csw:Constraint version="1.1.0">
            <ogc:Filter>
            <ogc:And>
            <ogc:PropertyIsLike matchCase="false" wildCard="%" singleChar="_" escapeChar="\\">
            <ogc:PropertyName>apiso:AnyText</ogc:PropertyName>
            <ogc:Literal>%` + title + `%</ogc:Literal>
            </ogc:PropertyIsLike>
            <ogc:PropertyIsLike matchCase="false" wildCard="%" singleChar="_" escapeChar="\\">
            <ogc:PropertyName>HierarchyLevelName</ogc:PropertyName>
            <ogc:Literal>service</ogc:Literal>
            </ogc:PropertyIsLike>
            </ogc:And>
            </ogc:Filter>
            </csw:Constraint>
            </csw:Query>
            </csw:GetRecords>`,
            service_metadata = [],
            me = this;

        database.getConfigMetadataCatalogsName(source_csw_name).then(
            function (resultObject) {
                var source_csw = resultObject[0],
                    proxy_settings = source_csw.proxy ? config.proxy : false,
                    https_url = source_csw.csw_url.indexOf("https:") > -1,
                    httpsAgent = https_url ? https_agent.getHttpsAgent(proxy_settings) : null;

                axios({
                    method: "post",
                    url: source_csw.csw_url,
                    data: csw_request,
                    headers: {
                        "Content-Type": "application/xml"
                    },
                    proxy: config.use_https_tunnel_proxy && https_url ? false : proxy_settings,
                    httpsAgent: httpsAgent
                }).then(function (metadata_response) {
                    me.getServiceMetadata_rec(metadata_parser.getServiceMDID(metadata_response), source_csw, service_metadata, res, 0);
                }).catch(function (error) {
                    debug("Get Service Metadata List", error);
                    return res.json({error: true});
                });
            }
        );
    },

    getServiceMetadata_rec: function (uuids, source_csw, service_metadata, res, index, service_id) {
        var me = this;

        if (index < uuids.length) {
            var proxy_settings = source_csw.proxy ? config.proxy : false,
                https_url = source_csw.csw_url.indexOf("https:") > -1,
                httpsAgent = https_url ? https_agent.getHttpsAgent(proxy_settings) : null;

            axios({
                method: "get",
                url: source_csw.csw_url + "?REQUEST=GetRecordById&SERVICE=CSW&VERSION=2.0.2&outputSchema=http://www.isotc211.org/2005/gmd&id=" + uuids[index],
                proxy: config.use_https_tunnel_proxy && https_url ? false : proxy_settings,
                httpsAgent: httpsAgent
            }).then(function (metadata_response) {
                if (config.log_level === "debug") {
                    debug(metadata_response.config.url);
                    if (metadata_response.data.indexOf("MD_Metadata") === -1) {
                        debug("Metadatensatz nicht gefunden");
                    }
                }

                var service_params = metadata_parser.parseServiceMD(metadata_response),
                    is_inside = _.where(service_metadata, {md_uuid: service_params.md_uuid});

                service_params.service_id = service_id;

                if (is_inside.length === 0) {
                    service_metadata.push(service_params);
                }

                me.getServiceMetadata_rec(uuids, source_csw, service_metadata, res, index + 1, service_id);
            }).catch(function (error) {
                debug("Get Service Metadata", error);
                if (res) {
                    return res.json({error: true});
                }
            });
        }
        else {
            if (res) {
                res.json(service_metadata);
            }
            else {
                if (service_metadata.length > 0) {
                    database.updateServiceMetadata(service_metadata[0]);
                }
            }
        }
    },

    getDatasetMetadata: function (uuid, source_csw_name, res) {
        var me = this;

        database.getConfigMetadataCatalogsName(source_csw_name).then(
            function (resultObject) {
                var source_csw = resultObject[0],
                    proxy_settings = source_csw.proxy ? config.proxy : false,
                    https_url = source_csw.csw_url.indexOf("https:") > -1,
                    httpsAgent = https_url ? https_agent.getHttpsAgent(proxy_settings) : null;

                axios({
                    method: "get",
                    url: source_csw.csw_url + "?REQUEST=GetRecordById&SERVICE=CSW&VERSION=2.0.2&outputSchema=http://www.isotc211.org/2005/gmd&id=" + uuid,
                    proxy: config.use_https_tunnel_proxy && https_url ? false : proxy_settings,
                    httpsAgent: httpsAgent
                }).then(function (metadata_response) {
                    me._callCSWDatasetMetadata(source_csw, metadata_parser.getCoupledRessources(metadata_response), [], 0, false, res);
                }).catch(function (error) {
                    debug("Get Coupled Datasets", error);
                    return res.json({error: true});
                });
            }
        );
    },

    _callCSWDatasetMetadata: function (source_csw, dataset_uuids, dataset_metadata, index, md_seperate_layer, res, linkseparate_layer_id, linkseparate_service_id, linkalllayers) {
        var me = this;

        if (index < dataset_uuids.length) {
            var proxy_settings = source_csw.proxy ? config.proxy : false,
                https_url = source_csw.csw_url.indexOf("https:") > -1,
                httpsAgent = https_url ? https_agent.getHttpsAgent(proxy_settings) : null;

            axios({
                method: "get",
                url: source_csw.csw_url + "?REQUEST=GetRecordById&SERVICE=CSW&VERSION=2.0.2&outputSchema=http://www.isotc211.org/2005/gmd&id=" + dataset_uuids[index].md_uuid,
                proxy: config.use_https_tunnel_proxy && https_url ? false : proxy_settings,
                httpsAgent: httpsAgent
            }).then(function (metadata_response) {
                if (metadata_response.data.indexOf("MD_Metadata") === -1 && config.log_level === "debug") {
                    debug("Metadatensatz nicht gefunden");
                }
                else {
                    var dataset_params = metadata_parser.parseDatasetMD(metadata_response);

                    dataset_params.md_id = dataset_uuids[index].md_uuid;
                    dataset_params.config_metadata_catalog_id = source_csw.config_metadata_catalog_id;
                    dataset_metadata.push(dataset_params);
                }

                me._callCSWDatasetMetadata(source_csw, dataset_uuids, dataset_metadata, index + 1, md_seperate_layer, res, linkseparate_layer_id, linkseparate_service_id, linkalllayers);
            }).catch(function (error) {
                debug("Get Dataset Metadata", error);
                if (res) {
                    return res.json({error: true});
                }
            });
        }
        else {
            if (md_seperate_layer) {
                if (dataset_metadata.length > 0) {
                    if (dataset_metadata[0].csw === "dataset" || dataset_metadata[0].csw === "series") {
                        if (dataset_metadata[0].dataset_name.length !== 0) {
                            dataset_metadata[0].srs_metadata = source_csw.srs_metadata;
                            dataset_metadata[0].srs_services = config.srs_services;

                            database.upsertDataset(dataset_metadata[0]).then(
                                function () {
                                    if (config.log_level === "debug") {
                                        debug("Metadatensatz Hinzugefügt/aktualisert: " + dataset_metadata[0].md_id);
                                    }
                                    layer_json_gen.prepare(undefined, undefined, dataset_metadata[0].md_id);
                                    if (res) {
                                        if (linkalllayers) {
                                            database.getServiceLayerIds(linkseparate_service_id).then(
                                                async function (layer_ids) {
                                                    for (var i = 0; i < layer_ids.length; i++) {
                                                        await database.updateLayerLink({md_id: dataset_metadata[0].md_id, layer_id: layer_ids[i].layer_id, service_id: linkseparate_service_id});
                                                    }
                                                    layer_json_gen.prepare(linkseparate_service_id);
                                                    res.json({success: true});
                                                }
                                            );
                                        }
                                        else {
                                            database.updateLayerLink({md_id: dataset_metadata[0].md_id, layer_id: linkseparate_layer_id, service_id: linkseparate_service_id}).then(
                                                function () {
                                                    layer_json_gen.prepare(linkseparate_service_id, linkseparate_layer_id);
                                                    res.json({success: true});
                                                }
                                            );
                                        }
                                    }
                                }
                            );
                        }
                        else {
                            if (config.log_level === "debug") {
                                debug("Datensatzname ist leer.");
                            }
                            if (res) {
                                res.json({success: false});
                            }
                        }
                    }
                    else {
                        if (config.log_level === "debug") {
                            debug("Keine Datensatz-Metadatensätze zum aktualisieren");
                        }
                        if (res) {
                            res.json({success: false, csw: true});
                        }
                    }
                }
                else {
                    if (config.log_level === "debug") {
                        debug("Liste der Metadatensätze ist leer");
                    }
                    if (res) {
                        res.json({success: false});
                    }
                }
            }
            else {
                res.json(dataset_metadata);
            }
        }
    }
};

module.exports = csw_caller;
