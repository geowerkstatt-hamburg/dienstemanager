var axios = require("axios"),
    http = require("http"),
    _ = require("underscore"),
    axios_instances;

const instances = [];

axios_instances = {
    getInstance: function (domain) {
        var instance_obj = _.findWhere(instances, {domain: domain});

        if (!instance_obj) {
            var instance = axios.create({
                baseURL: "http://" + domain,
                headers: {"Content-Type": "application/json;charset=UTF-8"},
                timeout: 30000,
                httpAgent: new http.Agent({keepAlive: true})
            });

            instances.push(
                {
                    domain: domain,
                    instance: instance
                }
            );
            return instance;
        }
        else {
            return instance_obj.instance;
        }
    }
};

module.exports = axios_instances;
