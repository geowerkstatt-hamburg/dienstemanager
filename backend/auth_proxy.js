var axios = require("axios"),
    https = require("https"),
    config = require("./../config.js"),
    debug = require("debug")("DM:auth_proxy"),
    auth_proxy;

auth_proxy = {
    /* Führt einen GET-Request /config/reload auf die, in der Config hinterlegten, Auth-Proxy-URL aus, um die Liste der abgesicherten URLs im Auth-Proxy zu aktualisieren */
    reload: function () {
        axios({
            method: "get",
            url: config.auth_proxy_reload_url,
            httpsAgent: new https.Agent({
                rejectUnauthorized: config.reject_unauthorized_SSL === null || config.reject_unauthorized_SSL === undefined ? false : config.reject_unauthorized_SSL
            })
        }).then(function (response) {
            if (response.data === "reload error") {
                debug("Auth-Proxy Reload Error");
            }
        }).catch(function (error) {
            debug("Auth-Proxy Reload Error", error.syscall, error.code, error.address, error.port);
        });
    }
};

module.exports = auth_proxy;
