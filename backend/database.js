var pgpromise = require("pg-promise")(),
    config = require("./../config.js"),
    debug = require("debug")("DM:database"),
    sql = require("./sql"),
    db,
    connection,
    database,
    env = process.env.NODE_ENV ? process.env.NODE_ENV.trim() : "dev";

if (env === "prod") {
    connection = config.database.prod;
}
else {
    connection = config.database.dev;
}

db = pgpromise(connection);

database = {

    dbQuery: function (query, params) {
        var result = db.any(query, params)
            .then(data => {
                return data;
            })
            .catch(error => {
                debug(error, query);
            });

        return result;
    },

    getServiceList: function () {
        return this.dbQuery("SELECT * FROM service_list");
    },

    getContacts: function () {
        return this.dbQuery("SELECT * FROM contacts");
    },

    getDatasetList: function () {
        return this.dbQuery("SELECT * FROM dataset_list");
    },

    getDatasets: function () {
        return this.dbQuery("select * FROM datasets d JOIN config_metadata_catalogs c ON d.config_metadata_catalog_id = c.config_metadata_catalog_id");
    },

    getServices: function () {
        return this.dbQuery("select * FROM services s JOIN config_metadata_catalogs c ON s.config_metadata_catalog_id = c.config_metadata_catalog_id");
    },

    getConfigDMInstances: function () {
        return this.dbQuery("SELECT * FROM config_dm_instances");
    },

    addConfigDMInstance: function (params) {
        return this.dbQuery("INSERT INTO config_dm_instances (name, host, port, database, db_user, pw) VALUES (${name}, ${host}, ${port}, ${database}, ${db_user}, ${pw})", params);
    },

    updateConfigDMInstance: function (params) {
        return this.dbQuery("UPDATE config_dm_instances SET name=${name}, host=${host}, port=${port}, database=${database}, db_user=${db_user}, pw=${pw} WHERE config_dm_instance_id=${config_dm_instance_id}", params);
    },

    deleteConfigDMInstance: function (config_dm_instance_id) {
        return this.dbQuery("DELETE FROM config_dm_instances WHERE config_dm_instance_id = $1", [config_dm_instance_id]);
    },

    getConfigMetadataCatalogs: function () {
        return this.dbQuery("SELECT * FROM config_metadata_catalogs");
    },

    getConfigMetadataCatalogsName: function (source_csw_name) {
        return this.dbQuery("SELECT * FROM config_metadata_catalogs WHERE name = $1", [source_csw_name]);
    },

    getConfigMetadataCatalogsId: function (config_metadata_catalog_id) {
        return this.dbQuery("SELECT * FROM config_metadata_catalogs WHERE config_metadata_catalog_id = $1", [config_metadata_catalog_id]);
    },

    getDistinctKeywords: function () {
        return this.dbQuery("SELECT DISTINCT ON (text) keyword_id, service_id, text FROM keywords");
    },

    getServiceTestResults: function (service_id) {
        return this.dbQuery("SELECT * FROM service_test_results WHERE service_id = $1", [service_id]);
    },

    getServiceTestResultsChecked: function (service_id) {
        return this.dbQuery("SELECT * FROM service_test_results_checked WHERE service_id = $1", [service_id]);
    },

    getDeletedServices: function () {
        return this.dbQuery("SELECT * FROM services WHERE deleted = true");
    },

    addDataset: function (params) {
        return this.dbQuery(sql.datasets.add, params);
    },

    getLayerList: function () {
        return this.dbQuery("SELECT * FROM layer_list");
    },

    getLayers: function () {
        return this.dbQuery("SELECT * FROM service_layers sl LEFT JOIN (SELECT service_id, array_agg(text) AS keywords FROM keywords GROUP BY service_id) kw using (service_id)");
    },

    getPortalList: function () {
        return this.dbQuery("SELECT distinct on (url) url, title FROM public.portal_links ORDER BY url, title");
    },

    getPortalLayerList: function (PortalUrl) {
        return this.dbQuery("SELECT a.layer_id, b.layer_name, b.title, b.service_id FROM public.portal_links a JOIN layers b ON a.layer_id = b.layer_id WHERE url = $1", [PortalUrl]);
    },

    getServiceLayers: function (service_id) {
        return this.dbQuery("SELECT * FROM service_layers WHERE service_id = $1", [service_id]);
    },

    getServiceLayerIds: function (service_id) {
        return this.dbQuery("SELECT layer_id FROM layers WHERE service_id = $1 AND deleted = false", [service_id]);
    },

    getServiceLayerIdsAll: function (service_id) {
        return this.dbQuery("SELECT layer_id FROM layers WHERE service_id = $1", [service_id]);
    },

    addLayer: function (params) {
        return this.dbQuery(sql.layers.add, params);
    },

    addLayerWithType: function (params) {

        var service_type = params.service_type;

        if (service_type === "WMS" || service_type === "WMTS" || service_type === "WFS") {
            return this.dbQuery(sql.layers.add_ows, params);
        }
        else if (service_type === "Terrain3D" || service_type === "TileSet3D") {
            return this.dbQuery(sql.layers.add_3d, params);
        }
        else if (service_type === "Oblique") {
            return this.dbQuery(sql.layers.add_oblique, params);
        }
        else if (service_type === "SensorThings") {
            params.expand = "Datastreams($filter=properties/layerName%20eq%20%27" + params.layer_name + "%27),Locations";
            params.filter = "Datastreams/properties/serviceName%20eq%20%27" + params.service_name + "%27";
            return this.dbQuery(sql.layers.add_sensorthings, params);
        }
        else if (service_type === "VectorTiles") {
            return this.dbQuery(sql.layers.add_vectortiles, params);
        }
    },

    updateLayerDetails: function (params) {
        return this.dbQuery(sql.layers.update_details, params);
    },

    updateLayerWithType: function (params) {
        var service_type = params.service_type;

        if (service_type === "WMS" || service_type === "WMTS" || service_type === "WFS") {
            return this.dbQuery(sql.layers.update_ows_details, params);
        }
        else if (service_type === "Terrain3D" || service_type === "TileSet3D") {
            return this.dbQuery(sql.layers.update_3d_details, params);
        }
        else if (service_type === "Oblique") {
            return this.dbQuery(sql.layers.update_oblique_details, params);
        }
        else if (service_type === "SensorThings") {
            return this.dbQuery(sql.layers.update_sensorthings_details, params);
        }
        else if (service_type === "VectorTiles") {
            return this.dbQuery(sql.layers.update_vectortiles_details, params);
        }
    },

    updateLayer: function (params) {
        return this.dbQuery(sql.layers.update, params);
    },

    updateLayerNamespace: function (params) {
        return this.dbQuery("UPDATE layers_type_ows SET namespace = $1 WHERE layer_id = $2 RETURNING layer_id;", [params.namespace, params.layer_id]);
    },

    deleteLayer: function (layer_id) {
        return this.dbQuery(sql.layers.delete, [layer_id]);
    },

    deleteLayerFinal: function (layer_id) {
        return this.dbQuery(sql.layers.delete_final, [layer_id]);
    },

    deleteLayerJSON: function (layer_id) {
        return this.dbQuery(sql.layers.delete_json, [layer_id]);
    },

    getLayerLinks: function (layer_id) {
        return this.dbQuery("SELECT * FROM layer_links_dataset WHERE layer_id = $1", [layer_id]);
    },

    deleteLayerLink: function (layer_links_id) {
        return this.dbQuery("DELETE FROM layer_links WHERE layer_links_id=$1", [layer_links_id]);
    },

    addLayerLink: function (params) {
        return this.dbQuery("INSERT INTO layer_links (layer_id, md_id, service_id) VALUES ($1,$2,$3)", params);
    },

    updateService: function (params) {
        return this.dbQuery(sql.services.update, params);
    },

    updateServiceMetadata: function (params) {
        return this.dbQuery(sql.services.update_metadata, params);
    },

    setNamespace: function (params) {
        return this.dbQuery("UPDATE layers SET namespace=$1 WHERE layers.service_id=$2", params);
    },

    updateServiceDataupdate: function (params) {
        return this.dbQuery(sql.services.dataupdate, params);
    },

    updateServiceInspire: function (params) {
        return this.dbQuery("UPDATE services SET inspire_relevant=$1 WHERE service_id = $2", params);
    },

    addService: function (params) {
        return this.dbQuery(sql.services.add, params);
    },

    deleteService: function (service_id) {
        return this.dbQuery(sql.services.delete, [service_id]);
    },

    deleteServiceFinal: function (service_id) {
        return this.dbQuery(sql.services.delete_final, [service_id]);
    },

    rollbackDelete: function (service_id) {
        return this.dbQuery(sql.services.rollback_delete, [service_id]);
    },

    getNotLinkedLayers: function (service_id) {
        return this.dbQuery("SELECT layer_id FROM layers WHERE service_id = $1 AND layer_id NOT IN (SELECT layer_id FROM layer_links WHERE service_id = $1) AND deleted = false", [service_id]);
    },

    setAllLayersLinked: function (service_id, dsc_complete) {
        return this.dbQuery("UPDATE services SET dsc_complete=$2 WHERE service_id = $1", [service_id, dsc_complete]);
    },

    getLinkedDatasets: function (service_id) {
        return this.dbQuery("SELECT ll.md_id, d.rs_id, d.fees, d.access_constraints, l.layer_name, lto.namespace FROM layer_links ll INNER JOIN datasets d ON ll.md_id = d.md_id INNER JOIN layers l ON ll.layer_id = l.layer_id INNER JOIN layers_type_ows lto ON ll.layer_id = lto.layer_id WHERE ll.service_id=$1", [service_id]);
    },

    getLinkedLayers: function (service_id) {
        return this.dbQuery("SELECT (SELECT layer_id FROM layers WHERE layer_id = layer_links.layer_id AND layers.deleted = false) AS layer_id, (SELECT layer_name FROM layers WHERE layer_id = layer_links.layer_id AND layers.deleted = false) AS layer_name, (SELECT title FROM layers WHERE layer_id = layer_links.layer_id AND layers.deleted = false) AS titel, md_id, (SELECT rs_id FROM datasets WHERE md_id = layer_links.md_id) AS rs_id, (SELECT dataset_name FROM datasets WHERE md_id = layer_links.md_id) AS dataset_name FROM layer_links where service_id = $1", [service_id]);
    },

    getServiceStats: function () {
        return this.dbQuery("SELECT service_type, COUNT(*) as count FROM services WHERE deleted = false GROUP BY service_type ORDER BY service_type ASC");
    },

    getSoftwareStats: function () {
        return this.dbQuery("SELECT software, COUNT(*) as count FROM services WHERE deleted = false GROUP BY software ORDER BY software ASC");
    },

    getLayerStats: function () {
        return this.dbQuery("SELECT (SELECT service_type FROM services WHERE service_id=layers.service_id) as service_type, COUNT(*) as count FROM layers WHERE layers.deleted = false GROUP BY service_type ORDER BY service_type ASC");
    },

    getServiceStatsProd: function () {
        return this.dbQuery("SELECT service_type, COUNT(*) as count FROM services WHERE status = 'Produktiv' AND deleted = false GROUP BY service_type ORDER BY service_type ASC");
    },

    getSoftwareStatsProd: function () {
        return this.dbQuery("SELECT software, COUNT(*) as count FROM services WHERE status = 'Produktiv' AND deleted = false GROUP BY software ORDER BY software ASC");
    },

    getLayerStatsProd: function () {
        return this.dbQuery("SELECT (SELECT service_type FROM services WHERE service_id=layers.service_id) as service_type, COUNT(*) as count FROM layers WHERE (SELECT status FROM services WHERE service_id=layers.service_id AND services.deleted = false) = 'Produktiv' GROUP BY service_type ORDER BY service_type ASC");
    },

    getInternalServiceStats: function (external, prod) {
        if (prod) {
            return this.dbQuery("SELECT service_type, COUNT(*) as count FROM services WHERE external = $1::boolean AND status = 'Produktiv' GROUP BY service_type ORDER BY service_type ASC", external);
        }
        else {
            return this.dbQuery("SELECT service_type, COUNT(*) as count FROM services WHERE external = $1::boolean GROUP BY service_type ORDER BY service_type ASC", external);
        }
    },

    getInternalSoftwareStats: function (external, prod) {
        if (prod) {
            return this.dbQuery("SELECT software, COUNT(*) as count FROM services WHERE external = $1::boolean AND deleted = false AND status = 'Produktiv' GROUP BY software ORDER BY software ASC", external);
        }
        else {
            return this.dbQuery("SELECT software, COUNT(*) as count FROM services WHERE external = $1::boolean AND deleted = false GROUP BY software ORDER BY software ASC", external);
        }
    },

    getInternalLayerStats: function (external, prod) {
        if (prod) {
            return this.dbQuery("SELECT (SELECT service_type FROM services WHERE service_id=layers.service_id) as service_type, COUNT(*) as count FROM layers WHERE (SELECT external FROM services WHERE service_id=layers.service_id) = $1::boolean AND (SELECT status FROM services WHERE service_id=layers.service_id) = 'Produktiv' GROUP BY service_type ORDER BY service_type ASC", external);
        }
        else {
            return this.dbQuery("SELECT (SELECT service_type FROM services WHERE service_id=layers.service_id) as service_type, COUNT(*) as count FROM layers WHERE (SELECT external FROM services WHERE service_id=layers.service_id) = $1::boolean GROUP BY service_type ORDER BY service_type ASC", external);
        }
    },

    setLastEditedBy: function (params) {
        return this.dbQuery("UPDATE services SET last_edited_by=$2, last_edit_date=$3 WHERE service_id = $1::integer", params);
    },

    setCheckedStatus: function (service_id, status) {
        return this.dbQuery("UPDATE services SET service_checked_status=$2 WHERE service_id = $1::integer", [service_id, status]);
    },

    addServiceTestResult: function (params) {
        return this.dbQuery(sql.servicetestresults.add, params);
    },

    deleteAllServiceTestResults: function (service_id) {
        return this.dbQuery("DELETE FROM service_test_results WHERE service_id = $1", [service_id]);
    },

    deleteServiceTestResult: function (service_test_results_id) {
        return this.dbQuery("DELETE FROM service_test_results WHERE service_test_results_id = $1", [service_test_results_id]);
    },

    checkServiceTestResult: function (service_test_results_id) {
        return this.dbQuery(sql.servicetestresults.check, [service_test_results_id]);
    },

    deleteServiceTestResultChecked: function (service_test_results_id) {
        return this.dbQuery("DELETE FROM service_test_results_checked WHERE service_test_results_id = $1", [service_test_results_id]);
    },

    getFreigabenList: function () {
        return this.dbQuery("SELECT * FROM freigaben");
    },

    getLinkedFreigaben: function (service_id) {
        return this.dbQuery("SELECT (SELECT freigabe_id FROM freigaben WHERE freigabe_id = freigaben_links.freigabe_id) AS freigabe_id, (SELECT datenbezeichnung FROM freigaben WHERE freigabe_id = freigaben_links.freigabe_id) AS datenbezeichnung,(SELECT link FROM freigaben WHERE freigabe_id = freigaben_links.freigabe_id) AS link, freigaben_link_id AS freigaben_link_id FROM freigaben_links where service_id = $1", [service_id]);
    },

    addFreigabeLink: function (service_id, freigabe_id) {
        return this.dbQuery("INSERT INTO freigaben_links (service_id, freigabe_id) VALUES ($1,$2)", [service_id, freigabe_id]);
    },

    getKeywordList: function (service_id) {
        return this.dbQuery("SELECT * FROM keywords WHERE service_id=$1", [service_id]);
    },

    getCapabilitiesKeywordList: function (service_id) {
        return this.dbQuery("SELECT * FROM keywords WHERE service_id=$1 AND text != 'mrh' AND text != 'only_mrh' AND text != 'Processing'", [service_id]);
    },

    getKeywordsByText: function (text) {
        return this.dbQuery("SELECT service_id, COUNT(text) as count FROM keywords WHERE text IN ($1) GROUP BY service_id", [text]);
    },

    deleteKeywords: function (service_id) {
        return this.dbQuery("DELETE FROM keywords WHERE service_id=$1", [service_id]);
    },

    addKeyword: function (service_id, text) {
        return this.dbQuery("INSERT INTO keywords (service_id, text) VALUES ($1,$2)", [service_id, text]);
    },

    deleteFreigabeLink: function (freigaben_link_id) {
        return this.dbQuery("DELETE FROM freigaben_links WHERE freigaben_link_id=$1", [freigaben_link_id]);
    },

    getCommentsList: function (service_id) {
        return this.dbQuery("SELECT comment_id, author, text, to_char(post_date::timestamp, 'YYYY-MM-DD') AS post_date, service_id FROM comments WHERE service_id=$1", [service_id]);
    },

    addComment: function (params) {
        return this.dbQuery("INSERT INTO comments (service_id, author, post_date, text) VALUES (${service_id},${author},${post_date},${text})", params);
    },

    deleteComment: function (comment_id) {
        return this.dbQuery("DELETE FROM comments WHERE comment_id=$1", [comment_id]);
    },

    updateComment: function (params) {
        return this.dbQuery("UPDATE comments SET service_id=${service_id}, author=${author}, post_date=${post_date}, text=${text} WHERE comment_id=${comment_id}", params);
    },

    updateContact: function (params) {
        return this.dbQuery("UPDATE contacts SET given_name=${given_name}, surname=${surname}, name=${name}, company=${company}, ad_account=${ad_account}, email=${email}, tel=${tel} WHERE contact_id=${contact_id}", params);
    },

    updateGfiConfig: function (params) {
        return this.dbQuery("UPDATE layers SET gfi_config=$(gfi_config) WHERE layer_id=$(layer_id)", params);
    },

    deleteContact: function (params) {
        return this.dbQuery(sql.contacts.delete, params);
    },

    addContact: function (params) {
        return this.dbQuery(sql.contacts.add, params);
    },

    addConfigmetadatacatalog: function (params) {
        return this.dbQuery("INSERT INTO config_metadata_catalogs (name, csw_url, show_doc_url, proxy, srs_metadata) VALUES (${name}, ${csw_url}, ${show_doc_url}, ${proxy}, ${srs_metadata})", params);
    },

    updateConfigmetadatacatalog: function (params) {
        return this.dbQuery("UPDATE config_metadata_catalogs SET name=${name}, csw_url=${csw_url}, show_doc_url=${show_doc_url}, proxy=${proxy}, srs_metadata=${srs_metadata} WHERE config_metadata_catalog_id=${config_metadata_catalog_id}", params);
    },

    deleteConfigmetadatacatalog: function (config_metadata_catalog_id) {
        return this.dbQuery("DELETE FROM config_metadata_catalogs WHERE config_metadata_catalog_id = $1", [config_metadata_catalog_id]);
    },

    deleteContactLink: function (service_id) {
        return this.dbQuery("DELETE FROM contact_links WHERE service_id=$1", [service_id]);
    },

    addContactLink: function (service_id, contact_id) {
        return this.dbQuery("INSERT INTO contact_links(service_id,contact_id) VALUES ($1, $2)", [service_id, contact_id]);
    },

    getLinkedContacts: function (service_id) {
        return this.dbQuery("SELECT * FROM contacts WHERE contact_id IN (SELECT contact_id AS contact_id FROM contact_links WHERE service_id = $1)", [service_id]);
    },

    getVisits: function (service_id) {
        return this.dbQuery("SELECT * FROM visits WHERE service_id = $1", [service_id]);
    },

    getTopVisits: function (month) {
        return this.dbQuery("SELECT * FROM visits WHERE month = $1 ORDER BY visits_total DESC LIMIT 10", [month]);
    },

    getDistinctMonth: function () {
        return this.dbQuery("SELECT DISTINCT ON (month) month FROM visits");
    },

    getTotalVisits: function () {
        return this.dbQuery("SELECT 1 AS service_id, 'Alle Dienste' AS title, 'Geoproxy' AS name_ext, month, sum(visits_intranet) AS visits_intranet, sum(visits_internet) AS visits_internet, sum(visits_total) AS visits_total FROM visits GROUP BY month");
    },

    setGfiConfigAllLayers: function (layer_id, service_id) {
        return this.dbQuery("UPDATE layers SET gfi_config=(SELECT gfi_config FROM layers WHERE layer_id=$1) WHERE service_id=$2", [layer_id, service_id]);
    },

    getJsonDataService: function (service_id) {
        return this.dbQuery("SELECT * FROM services WHERE service_id=$1 AND deleted = false", [service_id]);
    },

    getJsonDataServiceLayer: function (service_id) {
        return this.dbQuery("SELECT * FROM service_layers LEFT JOIN (SELECT service_id, array_agg(text) AS keywords FROM keywords GROUP BY service_id) kw using (service_id) WHERE service_id =$1", [service_id, service_id]);
    },

    getJsonDataLayerDataset: function (layer_id) {
        return this.dbQuery("SELECT * FROM dataset_list WHERE md_id IN (SELECT md_id FROM layer_links WHERE layer_id=$1)", [layer_id]);
    },

    getJsonDataLayer: function (layer_id) {
        return this.dbQuery("SELECT * FROM service_layers LEFT JOIN (SELECT service_id, array_agg(text) AS keywords FROM keywords GROUP BY service_id) kw using (service_id) WHERE layer_id=$1", [layer_id]);
    },

    getJsonDataDataset: function (md_id) {
        return this.dbQuery("SELECT * FROM datasets WHERE md_id=$1", [md_id]);
    },

    getLinkedDatasetLayers: function (md_id) {
        return this.dbQuery("SELECT * FROM service_layers LEFT JOIN (SELECT service_id, array_agg(text) AS keywords FROM keywords GROUP BY service_id) kw using (service_id) WHERE layer_id IN (SELECT layer_id FROM layer_links WHERE md_id=$1)", [md_id]);
    },

    deleteUnusedLayerJSON: function () {
        return this.dbQuery("DELETE FROM layer_json WHERE layer_id NOT IN (SELECT layer_id FROM layers)");
    },

    updateLayerJSON: function (params) {
        return this.dbQuery(sql.layers.update_json, params);
    },

    getLayerJson: function (scope, filter, prod) {
        var scope_inv = "internet",
            json_scope = "intranet",
            scope_query = "",
            filter_query = "",
            prod_query = "";

        if (scope === "internet") {
            scope_inv = "intranet";
            json_scope = "internet";
        }

        if (scope !== "all") {
            scope_query = " WHERE only_" + scope_inv + " = false";
        }

        if (prod && scope_query.indexOf("WHERE") > -1) {
            prod_query = " AND status = 'Produktiv'";
        }
        else if (prod && scope_query.indexOf("WHERE") === -1) {
            prod_query = " WHERE status = 'Produktiv'";
        }

        if (filter !== undefined && (scope_query.indexOf("WHERE") > -1 || prod_query.indexOf("WHERE") > -1)) {
            filter_query = " AND service_id IN (SELECT service_id FROM keywords WHERE text = '" + filter + "')";
        }
        else if (filter !== undefined && scope_query.indexOf("WHERE") === -1 && prod_query.indexOf("WHERE") === -1) {
            filter_query = " WHERE service_id IN (SELECT service_id FROM keywords WHERE text = '" + filter + "')";
        }

        return this.dbQuery("SELECT array_to_json(array_agg(json_" + json_scope + ")) AS json_array from layer_json" + scope_query + prod_query + filter_query);
    },

    getListLayerJson: function (layers, scope) {
        var scope_inv = "internet",
            json_scope = "intranet",
            scope_query = "";

        if (scope === "internet") {
            scope_inv = "intranet";
            json_scope = "internet";
        }

        if (scope !== "all") {
            scope_query = " AND only_" + scope_inv + " = false";
        }

        return this.dbQuery("SELECT array_to_json(array_agg(json_" + json_scope + ")) AS json_array from layer_json WHERE layer_id IN (" + layers + ")" + scope_query);
    },

    upsertDataset: function (params) {
        return this.dbQuery(sql.datasets.upsert, params);
    },

    updateLayerLink: function (params) {
        return this.dbQuery(sql.layers.update_layerlink, params);
    },

    updateChangelog: function (params) {
        return this.dbQuery(sql.changelog.add, params);
    },

    updateAllowedGroups: function (service_id, allowed_groups) {
        return this.dbQuery("UPDATE services SET allowed_groups = $2::json WHERE service_id = $1", [service_id, allowed_groups]);
    },

    deleteUnusedDataset: function () {
        return this.dbQuery("DELETE FROM datasets WHERE md_id NOT in (SELECT DISTINCT md_id FROM layer_links)");
    },

    getServiceIdsByKeyword: function (keywords) {
        return this.dbQuery("SELECT service_id FROM keywords WHERE text IN (" + keywords + ")");
    },

    getMetadataValues: function (md_id) {
        return this.dbQuery("SELECT * FROM datasets WHERE md_id = $1", [md_id]);
    },

    setDeleteLayerRequest: function (layer_id, service_id, comment) {
        return this.dbQuery("INSERT INTO layer_delete_requests (layer_id, service_id, comment) VALUES ($1, $2, $3) ON CONFLICT (layer_id) DO UPDATE SET comment = $2;", [layer_id, service_id, comment]);
    },

    removeDeleteLayerRequest: function (layer_id) {
        return this.dbQuery("DELETE FROM layer_delete_requests WHERE layer_id = $1;", [layer_id]);
    },

    getDeleteLayerRequests: function () {
        return this.dbQuery("SELECT * FROM layer_delete_requests;");
    },

    getLinkedTickets: function (params) {
        return this.dbQuery("SELECT * FROM service_jira_tickets WHERE service_id = $1 AND project = $2", [params.service_id, params.project]);
    },

    getServiceIssues: function (params) {
        return this.dbQuery("SELECT * FROM service_jira_tickets WHERE service_id = $1", [params.service_id]);
    },

    // getServiceIssues: function (params) {
    //     if (params.project) {
    //         return this.dbQuery("SELECT * FROM service_jira_tickets WHERE service_id = $1 AND project = $2", [params.service_id, params.project]);
    //     }
    //     else {
    //         return this.dbQuery("SELECT * FROM service_jira_tickets WHERE service_id = $1", [params.service_id]);
    //     }
    // },

    deleteServiceIssue: function (params) {
        return this.dbQuery("DELETE FROM service_jira_tickets WHERE id=$1 AND service_id=$2", [params.id, params.service_id]);
    },

    addServiceIssue: function (params) {
        return this.dbQuery("INSERT INTO service_jira_tickets (id, service_id, key, creator, summary, assignee, project) VALUES ($1,$2,$3,$4,$5,$6,$7)", [params.id, params.service_id, params.key, params.creator, params.summary, params.assignee, params.project]);
    },

    getScheduledTasks: function () {
        return this.dbQuery("SELECT * FROM config_scheduled_tasks");
    },

    updateScheduledTaskStatus: function (params) {
        return this.dbQuery("UPDATE config_scheduled_tasks SET last_run = $2, message = $3, status = $4 WHERE name = $1;", params);
    },

    updateScheduledTasks: function (params) {
        var result = db.tx(async t1 => {
            const update = pgpromise.helpers.update(params, ["name", "schedule", "enabled"], "config_scheduled_tasks") + " WHERE v.name = t.name";

            await t1.none(update);
        }).then(data => {
            return data;
        }).catch(error => {
            console.log("DB importServiceWithNewId", error);
        });

        return result;
    },

    getPortalLinks: function (layer_id) {
        return this.dbQuery("SELECT layer_id, title, url FROM portal_links WHERE layer_id=$1", [layer_id]);
    },

    deletePortalLinks: function (url) {
        return this.dbQuery("DELETE FROM portal_links WHERE url LIKE '$1:value%'", [url]);
    },

    addPortalLinks: async function (portal_links) {
        const table = new pgpromise.helpers.TableName("portal_links", "public"),
            cs = new pgpromise.helpers.ColumnSet(["layer_id", "title", "url"], {table}),
            query = pgpromise.helpers.insert(portal_links, cs) + " ON CONFLICT ON CONSTRAINT portal_links_pkey DO NOTHING";

        await db.none(query);
    },

    getNextLayerId: function () {
        var result = db.one("SELECT nextval(pg_get_serial_sequence('layers', 'layer_id')) AS layer_id;")
            .then(data => {
                return data;
            })
            .catch(error => {
                debug(error);
            });

        return result;
    },

    checkIfLayerIdExists: function (id_list) {
        return this.dbQuery("SELECT COUNT(*) FROM layers WHERE layer_id IN ($1:raw)", [id_list]);
    },

    importServiceWithNewId: function (service) {
        var result = db.tx(async t => {

            service.title = "IMPORT " + service.title;

            const newServiceID = await t.one("SELECT nextval(pg_get_serial_sequence('services', 'service_id')) AS service_id;");

            service.service_id = newServiceID.service_id;

            const services_column_names = await t.many("SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'services';");
            var services_columns = [];

            services_column_names.forEach(c => {
                services_columns.push(c.column_name);
            });

            const service_query = pgpromise.helpers.insert(service, services_columns, "services") + "RETURNING service_id";
            const service_id = await t.one(service_query);

            if (service.comments.length > 0) {
                service.comments.forEach(c => {
                    c.service_id = service.service_id;
                });
                const comments_cs = new pgpromise.helpers.ColumnSet(["author", "text", "post_date", "service_id"]);
                const comments_query = pgpromise.helpers.insert(service.comments, comments_cs, "comments");

                await t.none(comments_query);
            }
            if (service.changelog.length > 0) {
                service.changelog.forEach(cl => {
                    cl.service_id = service.service_id;
                });
                const changelog_cs = new pgpromise.helpers.ColumnSet(["username", "service_id", "change_date", "change"]);
                const changelog_query = pgpromise.helpers.insert(service.changelog, changelog_cs, "changelog");

                await t.none(changelog_query);
            }
            if (service.contact_links.length > 0) {
                service.contact_links.forEach(cl => {
                    cl.service_id = service.service_id;
                });
                const contact_links_cs = new pgpromise.helpers.ColumnSet(["service_id", "contact_id"]);
                const contact_links_query = pgpromise.helpers.insert(service.contact_links, contact_links_cs, "contact_links");

                await t.none(contact_links_query);
            }
            if (service.keywords.length > 0) {
                service.keywords.forEach(k => {
                    k.service_id = service.service_id;
                });
                const keywords_cs = new pgpromise.helpers.ColumnSet(["service_id", "text"]);
                const keywords_query = pgpromise.helpers.insert(service.keywords, keywords_cs, "keywords");

                await t.none(keywords_query);
            }
            if (service.layers.length > 0) {
                const layers_column_names = await t.many("SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'layers';");
                var layers_columns = [];

                layers_column_names.forEach(c => {
                    if (c.column_name !== "deleted") {
                        layers_columns.push(c.column_name);
                    }
                });

                const layers_cs = new pgpromise.helpers.ColumnSet(layers_columns);

                for (var layer of service.layers) {
                    var new_layer_id = await t.one("SELECT nextval(pg_get_serial_sequence('layers', 'layer_id')) AS layer_id;");

                    new_layer_id = new_layer_id.layer_id;

                    var layer_links = [];

                    for (var layer_link of service.layer_links) {
                        if (layer_link.layer_id === layer.layer_id) {
                            layer_link.layer_id = new_layer_id;
                            layer_link.service_id = service.service_id;
                            layer_links.push(layer_link);
                        }
                    }

                    layer.layer_id = new_layer_id;
                    layer.service_id = service.service_id;

                    const layers_query = pgpromise.helpers.insert(layer, layers_cs, "layers");

                    await t.none(layers_query);

                    if (service.service_type === "WMS" || service.service_type === "WMTS" || service.service_type === "WFS") {
                        const column_names = await t.many("SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'layers_type_ows';");
                        var layers_type_ows_columns = [];

                        for (const column of column_names) {
                            layers_type_ows_columns.push(column.column_name);
                        }
                        const cs = new pgpromise.helpers.ColumnSet(layers_type_ows_columns);
                        const query = pgpromise.helpers.insert(layer, cs, "layers_type_ows");

                        await t.none(query);
                    }
                    else if (service.service_type === "Terrain3D" || service.service_type === "TileSet3D") {
                        const column_names = await t.many("SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'layers_type_3d';");
                        var layers_type_3d_columns = [];

                        for (const column of column_names) {
                            layers_type_3d_columns.push(column.column_name);
                        }
                        const cs = new pgpromise.helpers.ColumnSet(layers_type_3d_columns);
                        const query = pgpromise.helpers.insert(layer, cs, "layers_type_3d");

                        await t.none(query);
                    }
                    else if (service.service_type === "Oblique") {
                        const column_names = await t.many("SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'layers_type_oblique';");
                        var layers_type_oblique_columns = [];

                        for (const column of column_names) {
                            layers_type_oblique_columns.push(column.column_name);
                        }
                        const cs = new pgpromise.helpers.ColumnSet(layers_type_oblique_columns);
                        const query = pgpromise.helpers.insert(layer, cs, "layers_type_oblique");

                        await t.none(query);
                    }
                    else if (service.service_type === "SensorThings") {
                        const column_names = await t.many("SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'layers_type_sensorthings';");
                        var layers_type_sensorthings_columns = [];

                        for (const column of column_names) {
                            layers_type_sensorthings_columns.push(column.column_name);
                        }
                        const cs = new pgpromise.helpers.ColumnSet(layers_type_sensorthings_columns);
                        const query = pgpromise.helpers.insert(layer, cs, "layers_type_sensorthings");

                        await t.none(query);
                    }
                    else if (service.service_type === "VectorTiles") {
                        const column_names = await t.many("SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'layers_type_vectortiles';");
                        var layers_type_vectortiles_columns = [];

                        for (const column of column_names) {
                            layers_type_vectortiles_columns.push(column.column_name);
                        }
                        const cs = new pgpromise.helpers.ColumnSet(layers_type_vectortiles_columns);
                        const query = pgpromise.helpers.insert(layer, cs, "layers_type_vectortiles");

                        await t.none(query);
                    }
                    if (service.layer_links.length > 0) {
                        const layer_links_cs = new pgpromise.helpers.ColumnSet(["layer_id", "md_id", "service_id"]);
                        const layer_links_query = pgpromise.helpers.insert(layer_links, layer_links_cs, "layer_links");

                        await t.none(layer_links_query);
                    }
                    if (service.datasets.length > 0) {
                        const column_names = await t.many("SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'datasets';");
                        var datasets_columns = [];

                        for (const column of column_names) {
                            datasets_columns.push(column.column_name);
                        }
                        const cs = new pgpromise.helpers.ColumnSet(datasets_columns);
                        const query = pgpromise.helpers.insert(service.datasets, cs, "datasets") + "ON CONFLICT (md_id) DO NOTHING;";

                        await t.none(query);
                    }
                }
            }

            return service_id;
        }).then(data => {
            return data;
        }).catch(error => {
            console.log("DB importServiceWithNewId", error);
        });

        return result;
    },

    importService: function (service) {
        var result = db.tx(async t => {
            const services_column_names = await t.many("SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'services';");
            var services_columns = [];

            services_column_names.forEach(c => {
                services_columns.push(c.column_name);
            });

            const service_query = pgpromise.helpers.insert(service, services_columns, "services") + "RETURNING service_id";
            const service_id = await t.one(service_query);

            if (service.comments.length > 0) {
                const comments_cs = new pgpromise.helpers.ColumnSet(["author", "text", "post_date", "service_id"]);
                const comments_query = pgpromise.helpers.insert(service.comments, comments_cs, "comments");

                await t.none(comments_query);
            }
            if (service.changelog.length > 0) {
                const changelog_cs = new pgpromise.helpers.ColumnSet(["username", "service_id", "change_date", "change"]);
                const changelog_query = pgpromise.helpers.insert(service.changelog, changelog_cs, "changelog");

                await t.none(changelog_query);
            }
            if (service.contact_links.length > 0) {
                const contact_links_cs = new pgpromise.helpers.ColumnSet(["service_id", "contact_id"]);
                const contact_links_query = pgpromise.helpers.insert(service.contact_links, contact_links_cs, "contact_links");

                await t.none(contact_links_query);
            }
            if (service.keywords.length > 0) {
                const keywords_cs = new pgpromise.helpers.ColumnSet(["service_id", "text"]);
                const keywords_query = pgpromise.helpers.insert(service.keywords, keywords_cs, "keywords");

                await t.none(keywords_query);
            }
            if (service.layers.length > 0) {
                const layers_column_names = await t.many("SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'layers';");
                var layers_columns = [];

                layers_column_names.forEach(c => {
                    if (c.column_name !== "deleted") {
                        layers_columns.push(c.column_name);
                    }
                });

                const layers_cs = new pgpromise.helpers.ColumnSet(layers_columns);
                const layers_query = pgpromise.helpers.insert(service.layers, layers_cs, "layers");

                await t.none(layers_query);

                if (service.service_type === "WMS" || service.service_type === "WMTS" || service.service_type === "WFS") {
                    const column_names = await t.many("SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'layers_type_ows';");
                    var layers_type_ows_columns = [];

                    for (const column of column_names) {
                        layers_type_ows_columns.push(column.column_name);
                    }
                    const cs = new pgpromise.helpers.ColumnSet(layers_type_ows_columns);
                    const query = pgpromise.helpers.insert(service.layers, cs, "layers_type_ows");

                    await t.none(query);
                }
                else if (service.service_type === "Terrain3D" || service.service_type === "TileSet3D") {
                    const column_names = await t.many("SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'layers_type_3d';");
                    var layers_type_3d_columns = [];

                    for (const column of column_names) {
                        layers_type_3d_columns.push(column.column_name);
                    }
                    const cs = new pgpromise.helpers.ColumnSet(layers_type_3d_columns);
                    const query = pgpromise.helpers.insert(service.layers, cs, "layers_type_3d");

                    await t.none(query);
                }
                else if (service.service_type === "Oblique") {
                    const column_names = await t.many("SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'layers_type_oblique';");
                    var layers_type_oblique_columns = [];

                    for (const column of column_names) {
                        layers_type_oblique_columns.push(column.column_name);
                    }
                    const cs = new pgpromise.helpers.ColumnSet(layers_type_oblique_columns);
                    const query = pgpromise.helpers.insert(service.layers, cs, "layers_type_oblique");

                    await t.none(query);
                }
                else if (service.service_type === "SensorThings") {
                    const column_names = await t.many("SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'layers_type_sensorthings';");
                    var layers_type_sensorthings_columns = [];

                    for (const column of column_names) {
                        layers_type_sensorthings_columns.push(column.column_name);
                    }
                    const cs = new pgpromise.helpers.ColumnSet(layers_type_sensorthings_columns);
                    const query = pgpromise.helpers.insert(service.layers, cs, "layers_type_sensorthings");

                    await t.none(query);
                }
                else if (service.service_type === "VectorTiles") {
                    const column_names = await t.many("SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'layers_type_vectortiles';");
                    var layers_type_vectortiles_columns = [];

                    for (const column of column_names) {
                        layers_type_vectortiles_columns.push(column.column_name);
                    }
                    const cs = new pgpromise.helpers.ColumnSet(layers_type_vectortiles_columns);
                    const query = pgpromise.helpers.insert(service.layers, cs, "layers_type_vectortiles");

                    await t.none(query);
                }

                if (service.layer_links.length > 0) {
                    const cs = new pgpromise.helpers.ColumnSet(["layer_id", "md_id", "service_id"]);
                    const query = pgpromise.helpers.insert(service.layer_links, cs, "layer_links");

                    await t.none(query);
                }

                if (service.datasets.length > 0) {
                    const column_names = await t.many("SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'datasets';");
                    var datasets_columns = [];

                    for (const column of column_names) {
                        datasets_columns.push(column.column_name);
                    }
                    const cs = new pgpromise.helpers.ColumnSet(datasets_columns);
                    const query = pgpromise.helpers.insert(service.datasets, cs, "datasets") + "ON CONFLICT (md_id) DO NOTHING;";

                    await t.none(query);
                }
            }

            return service_id;
        }).then(data => {
            return data;
        }).catch(error => {
            console.log("DB importService", error);
        });

        return result;
    },

    setLayerIdSeq: function () {
        return this.dbQuery("SELECT setval('public.layers_layer_id_seq', (SELECT MAX(layer_id) FROM layers), true);");
    },

    setServiceIdSeq: function () {
        return this.dbQuery("SELECT setval('public.services_service_id_seq', (SELECT MAX(service_id) FROM services), true);");
    }
};

module.exports = database;
