var fs = require("fs"),
    moment = require("moment"),
    database = require("../database"),
    path = require("path"),
    util = require("util"),
    readdir = util.promisify(fs.readdir),
    unlink = util.promisify(fs.unlink),
    debug = require("debug")("DM:delete_tmp_files"),
    delete_tmp_files;

delete_tmp_files = {
    execute: async function () {
        try {
            const files = await readdir(path.join(__dirname, "/../../tmp"));
            const unlinkPromises = files.map(file => unlink(path.join(path.join(__dirname, "/../../tmp"), file)));

            return Promise.all(unlinkPromises);
        }
        catch (err) {
            debug(err);
            database.updateScheduledTaskStatus(["delete_tmp_files", moment().format("YYYY-MM-DD HH:mm:ss"), err, "Fehler"]);
        }
        finally {
            debug("Ordner /tmp geleert");
            database.updateScheduledTaskStatus(["delete_tmp_files", moment().format("YYYY-MM-DD HH:mm:ss"), "Ordner /tmp geleert", "Erfolgreich"]);
        }
    }
};

module.exports = delete_tmp_files;
