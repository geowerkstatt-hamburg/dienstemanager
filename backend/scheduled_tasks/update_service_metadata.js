var csw_caller = require("../csw_caller"),
    database = require("../database"),
    debug = require("debug")("DM:update_service_metadata"),
    moment = require("moment"),
    update_service_metadata;

update_service_metadata = {
    execute: function () {
        database.getServices().then(
            function (services) {
                var i = 0;

                // Alle Dienste mit gekoppelten Metadaten werden aktualisiert.
                // Zwischen jedem Aufruf wird 0,5 Sekunden pausiert, damit nicht zu viele parallele Anfragen geöffnet werden.
                function updateLoop () {
                    var valid_md_id = false;

                    if (services[i].service_md_id !== undefined) {
                        if (services[i].service_md_id.length > 0) {
                            valid_md_id = true;
                        }
                    }
                    if (valid_md_id) {
                        setTimeout(function () {
                            debug("update service " + services[i].service_id);
                            var source_csw = {
                                csw_url: services[i].csw_url,
                                proxy: services[i].proxy
                            };

                            csw_caller.getServiceMetadata_rec([services[i].service_md_id], source_csw, [], null, 0, services[i].service_id);
                            i++;
                            if (i < services.length) {
                                updateLoop();
                            }
                            else {
                                console.log(moment().format("YYYY-MM-DD HH:mm:ss") + " - Aktualisierung der Dienstmetadaten abgeschlossen.");
                                database.updateScheduledTaskStatus(["update_service_metadata", moment().format("YYYY-MM-DD HH:mm:ss"), "Alle Dienstmetadaten aktualisiert", "Erfolgreich"]);
                            }
                        }, 500);
                    }
                    else {
                        i++;
                        if (i < services.length) {
                            updateLoop();
                        }
                        else {
                            console.log(moment().format("YYYY-MM-DD HH:mm:ss") + " - Aktualisierung der Dienstmetadaten abgeschlossen.");
                            database.updateScheduledTaskStatus(["update_service_metadata", moment().format("YYYY-MM-DD HH:mm:ss"), "Alle Dienstmetadaten aktualisiert", "Erfolgreich"]);
                        }
                    }
                }
                if (services.length > 0) {
                    updateLoop();
                }
                else {
                    console.log(moment().format("YYYY-MM-DD HH:mm:ss") + " - Aktualisierung der Dienstmetadaten abgeschlossen.");
                    database.updateScheduledTaskStatus(["update_service_metadata", moment().format("YYYY-MM-DD HH:mm:ss"), "Alle Dienstmetadaten aktualisiert", "Erfolgreich"]);
                }
            }
        );
    }
};

module.exports = update_service_metadata;
