var csw_caller = require("../csw_caller"),
    database = require("../database"),
    debug = require("debug")("DM:update_datasets"),
    moment = require("moment"),
    update_datasets;

update_datasets = {
    execute: function () {
        var me = this;

        database.deleteUnusedDataset().then(
            function () {
                database.getDatasets().then(
                    function (datasets) {
                        if (datasets.length > 0) {
                            me._updateLoop(datasets, 0);
                        }
                        else {
                            console.log(moment().format("YYYY-MM-DD HH:mm:ss") + " - Aktualisierung der Datensatzmetadaten abgeschlossen.");
                            database.updateScheduledTaskStatus(["update_datasets", moment().format("YYYY-MM-DD HH:mm:ss"), "Keine Datensatzmetadaten vorhanden", "Erfolgreich"]);
                        }
                    }
                );
            }
        );
    },

    _updateLoop: function (datasets, i) {
        var me = this;

        // alle Datensätze werden aktualisiert. Zwischen jedem Aufruf wird 0,5 Sekunden pausiert, damit nicht zu viele parallele Anfragen geöffnet werden.
        setTimeout(function () {
            debug("updating " + datasets[i].md_id);
            var source_csw = {
                    csw_url: datasets[i].csw_url,
                    proxy: datasets[i].proxy,
                    config_metadata_catalog_id: datasets[i].config_metadata_catalog_id,
                    srs_metadata: datasets[i].srs_metadata
                },
                dataset_uuids = [
                    {
                        md_uuid: datasets[i].md_id
                    }
                ];

            csw_caller._callCSWDatasetMetadata(source_csw, dataset_uuids, [], 0, true);

            if (i < datasets.length - 1) {
                me._updateLoop(datasets, i + 1);
            }
            else {
                console.log(moment().format("YYYY-MM-DD HH:mm:ss") + " - Aktualisierung der Datensatzmetadaten abgeschlossen.");
                database.updateScheduledTaskStatus(["update_datasets", moment().format("YYYY-MM-DD HH:mm:ss"), "Alle Datensatzmetadaten aktualisiert", "Erfolgreich"]);
            }
        }, 500);
    }
};

module.exports = update_datasets;
