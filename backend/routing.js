var layer_json_gen = require("./layer_json_gen"),
    database = require("./database"),
    service_caller = require("./service_caller"),
    csw_caller = require("./csw_caller"),
    fme_caller = require("./fme_caller"),
    inspire_cap_gen = require("./inspire_cap_gen"),
    downloader = require("./downloader"),
    auth_proxy = require("./auth_proxy"),
    notifier = require("./notifier"),
    cryptor = require("./cryptor"),
    service_checker = require("./service_checker"),
    elasticsearch_connector = require("./elasticsearch_connector"),
    scheduled_tasks_manager = require("./scheduled_tasks_manager"),
    Router = require("express-promise-router"),
    router = new Router(),
    config = require("./../config.js"),
    debug = require("debug")("DM:routing"),
    uuid = require("uuid"),
    ad,
    securityProxyDatabase,
    jira,
    service_importer = require("./service_importer"),
    env = process.env.NODE_ENV ? process.env.NODE_ENV.trim() : "dev";

if (config.ldap) {
    ad = require("./ad_async_caller");
}

if (config.security_proxy_database) {
    securityProxyDatabase = require("./security_proxy");
}

if (config.jira) {
    jira = require("./jira");
}

router.get("/getclientconfig", function (req, res) {
    var client_config = config.client_config;

    client_config.use_websockets = config.use_websockets;
    client_config.version = process.env.npm_package_version;
    client_config.jira_browse_url = config.jira.browse_url;
    client_config.jira_projects = config.jira.projects;
    client_config.jira_labels = config.jira.labels;

    res.json({config: client_config});
});

router.get("/getservicelist", function (req, res) {
    database.getServiceList().then(
        function (resultObject) {
            res.json(resultObject);
        }
    );
});

router.get("/getcontacts", function (req, res) {
    database.getContacts().then(
        function (resultObject) {
            res.json(resultObject);
        }
    );
});

router.get("/getdatasetlist", function (req, res) {
    database.getDatasetList().then(
        function (resultObject) {
            res.json(resultObject);
        }
    );
});

router.get("/getdistinctkeywords", function (req, res) {
    database.getDistinctKeywords().then(
        function (resultObject) {
            res.json(resultObject);
        }
    );
});

router.get("/getconfigmetadatacatalogs", function (req, res) {
    database.getConfigMetadataCatalogs().then(
        function (resultObject) {
            res.json(resultObject);
        }
    );

});

router.get("/getconfigmetadatacatalogsname", function (req, res) {
    database.getConfigMetadataCatalogsName().then(
        function (resultObject) {
            res.json(resultObject);
        }
    );
});

router.post("/addservice", function (req, res) {
    database.addService(req.body).then(
        function (resultObject) {
            if (config.auth_proxy_reload_url && env !== "dev") {
                auth_proxy.reload();
            }
            if (req.body.status === "Produktiv" && env === "prod" && config.mail) {
                notifier.onProdService(resultObject[0].service_id, req.body.title, req.body.service_type);
            }
            if (env === "prod" && config.fme.geoproxy_conf_update) {
                fme_caller.createGeoproxyConf();
            }
            layer_json_gen.prepare(resultObject[0].service_id);
            database.updateChangelog({username: req.body.editor, service_id: resultObject[0].service_id, change_date: req.body.last_edit_date, change: "Dienst angelegt"});
            res.json(resultObject);
        }
    );
});

router.post("/updateservice", function (req, res) {
    database.updateService(req.body).then(
        function (resultObject) {
            if (config.auth_proxy_reload_url && env !== "dev") {
                auth_proxy.reload();
            }
            if (req.body.qa && !req.body.old_qa && env === "prod" && config.mail) {
                notifier.onQAService(req.body.title, req.body.last_edited_by);
            }
            if (req.body.status === "Produktiv" && req.body.old_status !== "Produktiv" && env === "prod" && config.mail) {
                notifier.onProdService(req.body.service_id, req.body.title, req.body.service_type);
            }
            if (env === "prod" && config.fme.geoproxy_conf_update) {
                fme_caller.createGeoproxyConf();
            }
            if (Array.isArray(resultObject)) {
                layer_json_gen.prepare(req.body.service_id);
                database.setLastEditedBy([req.body.service_id, req.body.last_edited_by, req.body.last_edit_date]);
                database.updateChangelog({username: req.body.last_edited_by, service_id: req.body.service_id, change_date: req.body.last_edit_date, change: "Dienstdetails aktualisiert"});
                res.json({status: "success"});
            }
            else {
                res.json(resultObject);
            }
        }
    );
});

router.get("/getservicetestresults", function (req, res) {
    database.getServiceTestResults(req.query.service_id).then(
        function (resultObject) {
            res.json(resultObject);
        }
    );
});

router.get("/getservicetestresultschecked", function (req, res) {
    database.getServiceTestResultsChecked(req.query.service_id).then(
        function (resultObject) {
            res.json(resultObject);
        }
    );
});

router.get("/getcomments", function (req, res) {
    database.getCommentsList(req.query.service_id).then(
        function (resultObject) {
            res.json(resultObject);
        }
    );
});

router.get("/getvisits", function (req, res) {
    database.getVisits(req.query.service_id).then(
        function (resultObject) {
            res.json(resultObject);
        }
    );
});

router.get("/getvisitstotal", function (req, res) {
    database.getTotalVisits().then(
        function (resultObject) {
            res.json(resultObject);
        }
    );
});

router.get("/getvisitstop", function (req, res) {
    database.getTopVisits(req.query.month).then(
        function (resultObject) {
            res.json(resultObject);
        }
    );
});

router.get("/getdistinctmonth", function (req, res) {
    database.getDistinctMonth().then(
        function (resultObject) {
            res.json(resultObject);
        }
    );
});

router.get("/getlinkedcontacts", function (req, res) {
    database.getLinkedContacts(req.query.service_id).then(
        function (resultObject) {
            res.json(resultObject);
        }
    );
});

router.get("/getlinkedfreigaben", function (req, res) {
    database.getLinkedFreigaben(req.query.service_id).then(
        function (resultObject) {
            res.json(resultObject);
        }
    );
});

router.get("/getservicelayers", function (req, res) {
    database.getServiceLayers(req.query.service_id).then(
        function (resultObject) {
            res.json(resultObject);
        }
    );
});

router.get("/getkeywords", function (req, res) {
    database.getKeywordList(req.query.service_id).then(
        function (resultObject) {
            res.json(resultObject);
        }
    );
});

router.get("/getnotlinkedlayer", function (req, res) {
    database.getNotLinkedLayers(req.query.service_id).then(
        function (resultObject) {
            res.json(resultObject);
        }
    );
});

router.post("/setalllayerslinked", function (req, res) {
    database.setAllLayersLinked(req.body.service_id, req.body.dsc_complete).then(
        function (resultObject) {
            res.json(resultObject);
        }
    );
});

router.post("/updatekeywords", function (req, res) {
    var keywords = req.body.keywords;

    database.deleteKeywords(req.body.service_id).then(
        async function () {
            if (keywords) {
                for (var i = 0; i < keywords.length; i++) {
                    await database.addKeyword(req.body.service_id, keywords[i]);
                }
            }
            res.json({status: "success"});
        }
    );
});

router.post("/updatecontactlinks", function (req, res) {
    var contacts = req.body.contacts;

    database.deleteContactLink(req.body.service_id).then(
        async function () {
            for (var i = 0; i < contacts.length; i++) {
                await database.addContactLink(req.body.service_id, contacts[i].contact_id);
            }
            res.json({status: "success"});
        }
    );
});

router.get("/getusergroups", async function (req, res) {
    if (req.query.username) {
        var grouplist = await ad.getGroupMembershipForUser(req.query.username);

        res.json(grouplist);
    }
    else {
        res.json("Fehler: Parameter username nicht definiert");
    }
});

router.get("/getusersbygroup", async function (req, res) {
    if (req.query.groupname) {
        var users = await ad.findUsersByGroup(req.query.groupname);

        res.json(users);
    }
    else {
        res.json("Fehler: Parameter groupname nicht definiert");
    }
});

router.get("/getgroupsbygroup", async function (req, res) {
    if (req.query.groupname) {
        var grouplist = await ad.getGroupMembershipForGroup(req.query.groupname, res);

        res.json(grouplist);
    }
    else {
        res.json("Fehler: Parameter groupname nicht definiert");
    }
});

router.get("/getadcontacts", async function (req, res) {
    var user = await ad.findUsers(req.query.surname, req.query.givenname, res);

    res.json(user);
});

router.post("/addcontact", function (req, res) {
    database.addContact(req.body).then(
        function (resultObject) {
            res.json(resultObject);
        }
    );
});

router.post("/updatecontact", function (req, res) {
    database.updateContact(req.body).then(
        function (resultObject) {
            res.json(resultObject);
        }
    );
});

router.post("/updateconfigmetadatacatalog", function (req, res) {
    database.updateConfigmetadatacatalog(req.body).then(
        function (resultObject) {
            res.json(resultObject);
        }
    );
});

router.post("/deleteconfigmetadatacatalog", function (req, res) {
    database.deleteConfigmetadatacatalog(req.body.config_metadata_catalog_id).then(
        function (resultObject) {
            res.json(resultObject);
        }
    );
});

router.post("/addconfigmetadatacatalog", function (req, res) {
    database.addConfigmetadatacatalog(req.body).then(
        function (resultObject) {
            res.json(resultObject);
        }
    );
});

router.post("/deletecontact", function (req, res) {
    database.deleteContact(req.body).then(
        function (resultObject) {
            res.json(resultObject);
        }
    );
});

router.post("/deletetestresult", function (req, res) {
    database.deleteServiceTestResult(req.body.service_test_results_id).then(
        function (resultObject) {
            res.json(resultObject);
        }
    );
});

router.post("/checktestresult", function (req, res) {
    database.checkServiceTestResult(req.body.service_test_results_id).then(
        function (resultObject) {
            res.json(resultObject);
        }
    );
});

router.post("/deletetestresultchecked", function (req, res) {
    database.deleteServiceTestResultChecked(req.body.service_test_results_id).then(
        function (resultObject) {
            res.json(resultObject);
        }
    );
});

router.post("/updatedataupdatestatus", function (req, res) {
    database.updateServiceDataupdate(req.body).then(
        function (resultObject) {
            database.setLastEditedBy([req.body.service_id, req.body.last_edited_by, req.body.last_edit_date]);
            database.updateChangelog({username: req.body.last_edited_by, service_id: req.body.service_id, change_date: req.body.last_edit_date, change: "Dienstdetails Datenaktualität aktualisiert"});
            res.json(resultObject);
        }
    );
});

router.post("/deletecomment", function (req, res) {
    database.deleteComment(req.body.comment_id).then(
        function (resultObject) {
            res.json(resultObject);
        }
    );
});

router.post("/addcomment", function (req, res) {
    database.addComment(req.body).then(
        function (resultObject) {
            res.json(resultObject);
        }
    );
});

router.post("/updatecomment", function (req, res) {
    database.updateComment(req.body).then(
        function (resultObject) {
            res.json(resultObject);
        }
    );
});

router.post("/addfreigabelink", function (req, res) {
    database.addFreigabeLink(req.body.service_id, req.body.freigabe_id).then(
        function (resultObject) {
            database.setLastEditedBy([req.body.service_id, req.body.last_edited_by, req.body.last_edit_date]);
            database.updateChangelog({username: req.body.last_edited_by, service_id: req.body.service_id, change_date: req.body.last_edit_date, change: "Freigabe verlinkt"});
            res.json(resultObject);
        }
    );
});

router.post("/deletefreigabelink", function (req, res) {
    database.deleteFreigabeLink(req.body.freigaben_link_id).then(
        function (resultObject) {
            database.setLastEditedBy([req.body.service_id, req.body.last_edited_by, req.body.last_edit_date]);
            database.updateChangelog({username: req.body.last_edited_by, service_id: req.body.service_id, change_date: req.body.last_edit_date, change: "Freigabelink gelöscht"});
            res.json(resultObject);
        }
    );
});

router.post("/getgfiattributes", function (req, res) {
    service_caller.getGFIAttributes(req.body.url, req.body.version, req.body.type, req.body.layername, req.body.externalService, res);
});

router.get("/getdeletedservicelist", function (req, res) {
    database.getDeletedServices().then(
        function (resultObject) {
            res.json(resultObject);
        }
    );
});

router.post("/updateservicelayers", function (req, res) {
    service_caller.updateServiceLayers(req.body.url, req.body.service_id, req.body.service_type, req.body.software, req.body.version, req.body.url_ext, req.body.is_mrh, req.body.externalService, res);
});

router.post("/addLayersToService", function (req, res) {
    service_caller.addLayersToService(req.body.service_id, req.body.service_type, req.body.layer_list, req.body.last_edited_by, req.body.last_edit_date, res);
});

router.post("/updatelayer", function (req, res) {
    database.updateLayer(req.body).then(
        function (resultObject) {
            if (req.body.service_type === "WFS") {
                database.updateLayerNamespace(req.body).then(
                    function (resultObject2) {
                        database.updateChangelog({username: req.body.last_edited_by, service_id: req.body.service_id, change_date: req.body.last_edit_date, change: "Layerdetails aktualisiert, Layer-ID: " + req.body.layer_id});
                        database.setLastEditedBy([req.body.service_id, req.body.last_edited_by, req.body.last_edit_date]);
                        layer_json_gen.prepare(req.body.service_id, req.body.layer_id);
                        res.json(resultObject2);
                    }
                );
            }
            else {
                database.updateChangelog({username: req.body.last_edited_by, service_id: req.body.service_id, change_date: req.body.last_edit_date, change: "Layerdetails aktualisiert, Layer-ID: " + req.body.layer_id});
                database.setLastEditedBy([req.body.service_id, req.body.last_edited_by, req.body.last_edit_date]);
                layer_json_gen.prepare(req.body.service_id, req.body.layer_id);
                res.json(resultObject);
            }
        }
    );
});

router.post("/updatelayerdetails", function (req, res) {
    database.updateLayerDetails(req.body).then(
        function () {
            var service_type = req.body.service_type;

            if (service_type === "WMS" || service_type === "WMTS" || service_type === "WFS" || service_type === "Terrain3D" || service_type === "TileSet3D" || service_type === "Oblique" || service_type === "SensorThings" || service_type === "VectorTiles") {
                database.updateLayerWithType(req.body).then(
                    function () {
                        database.updateChangelog({username: req.body.last_edited_by, service_id: req.body.service_id, change_date: req.body.last_edit_date, change: "Layerliste aktualisiert"});
                        database.setLastEditedBy([req.body.service_id, req.body.last_edited_by, req.body.last_edit_date]);
                        if (req.body.gfi_useforalllayers) {
                            database.setGfiConfigAllLayers(req.body.layer_id, req.body.service_id);
                            layer_json_gen.prepare(req.body.service_id);
                        }
                        else {
                            layer_json_gen.prepare(req.body.service_id, req.body.layer_id);
                        }
                        res.json({success: true});
                    }
                );
            }
            else {
                database.updateChangelog({username: req.body.last_edited_by, service_id: req.body.service_id, change_date: req.body.last_edit_date, change: "Layerliste aktualisiert"});
                database.setLastEditedBy([req.body.service_id, req.body.last_edited_by, req.body.last_edit_date]);
                if (req.body.gfi_useforalllayers) {
                    database.setGfiConfigAllLayers(req.body.layer_id, req.body.service_id);
                    layer_json_gen.prepare(req.body.service_id);
                }
                else {
                    layer_json_gen.prepare(req.body.service_id, req.body.layer_id);
                }
                res.json({success: true});
            }

        }
    );
});

router.post("/updategficonfig", function (req, res) {
    database.updateGfiConfig(req.body).then(
        function () {
            database.updateChangelog({username: req.body.last_edited_by, service_id: req.body.service_id, change_date: req.body.last_edit_date, change: "Anzeige der Attribute geändert"});
            database.setLastEditedBy([req.body.service_id, req.body.last_edited_by, req.body.last_edit_date]);
            if (req.body.gfi_useforalllayers) {
                database.setGfiConfigAllLayers(req.body.layer_id, req.body.service_id);
                layer_json_gen.prepare(req.body.service_id);
            }
            else {
                layer_json_gen.prepare(req.body.service_id, req.body.layer_id);
            }
            res.json({success: true});
        }
    );
});

router.get("/getportallinks", function (req, res) {
    database.getPortalLinks(req.query.layer_id).then(
        function (resultObject) {
            res.json(resultObject);
        }
    );
});

router.post("/addlayer", function (req, res) {

    var service_type = req.body.service_type;

    if (service_type === "WMS" || service_type === "WMTS" || service_type === "WFS" || service_type === "Terrain3D" || service_type === "TileSet3D" || service_type === "Oblique" || service_type === "SensorThings" || service_type === "VectorTiles") {
        database.addLayerWithType(req.body).then(
            function (resultObject) {
                database.updateChangelog({username: req.body.last_edited_by, service_id: req.body.service_id, change_date: req.body.last_edit_date, change: "Layer hinzugefügt, Layer-ID: " + resultObject[0].layer_id});
                database.setLastEditedBy([req.body.service_id, req.body.last_edited_by, req.body.last_edit_date]);
                layer_json_gen.prepare(req.body.service_id, resultObject[0].layer_id);
                res.json(resultObject);
            }
        );
    }
    else {
        database.addLayer(req.body).then(
            function (resultObject) {
                database.updateChangelog({username: req.body.last_edited_by, service_id: req.body.service_id, change_date: req.body.last_edit_date, change: "Layer hinzugefügt, Layer-ID: " + resultObject[0].layer_id});
                database.setLastEditedBy([req.body.service_id, req.body.last_edited_by, req.body.last_edit_date]);
                layer_json_gen.prepare(req.body.service_id, resultObject[0].layer_id);
                res.json(resultObject);
            }
        );
    }
});

router.post("/setalllayerlinked", function (req, res) {
    database.setAllLayersLinked(req.body.service_id, req.body.dsc_complete).then(
        function (resultObject) {
            res.json(resultObject);
        }
    );
});

router.get("/getnotlinkedlayer", function (req, res) {
    database.getNotLinkedLayers(req.query.service_id).then(
        function (resultObject) {
            res.json(resultObject);
        }
    );
});

router.post("/deletelayerfinal", function (req, res) {
    database.deleteLayerFinal(req.body.layer_id).then(
        function () {
            database.updateChangelog({username: req.body.last_edited_by, service_id: req.body.service_id, change_date: req.body.last_edit_date, change: "Layer gelöscht, Layer-ID: " + req.body.layer_id});
            database.setLastEditedBy([req.body.service_id, req.body.last_edited_by, req.body.last_edit_date]);
            database.deleteLayerJSON(req.body.layer_id);
            if (env === "prod") {
                elasticsearch_connector.deleteFromIndex(req.body.layer_id);
            }

            if (req.body.status === "Produktiv" && env === "prod" && config.mail) {
                notifier.onDeleteLayer(req.body.layer_id, req.body.title);
            }
            res.json({success: true});
        }
    );
});

router.post("/deletelayer", function (req, res) {
    database.deleteLayer(req.body.layer_id).then(
        function () {
            database.updateChangelog({username: req.body.last_edited_by, service_id: req.body.service_id, change_date: req.body.last_edit_date, change: "Layer gelöscht, Layer-ID: " + req.body.layer_id});
            database.setLastEditedBy([req.body.service_id, req.body.last_edited_by, req.body.last_edit_date]);

            if (req.body.service_id !== undefined && req.body.layer_id === undefined) {
                database.getServiceLayerIds(req.body.service_id).then(
                    function (layer_data) {
                        for (var i = 0; i < layer_data.length; i++) {
                            database.deleteLayerJSON(layer_data[i].layer_id);
                            if (env === "prod") {
                                elasticsearch_connector.deleteFromIndex(req.body.layer_id);
                            }
                        }
                    }
                );
            }
            else {
                database.deleteLayerJSON(req.body.layer_id);
                if (env === "prod") {
                    elasticsearch_connector.deleteFromIndex(req.body.layer_id);
                }
            }

            if (req.body.status === "Produktiv" && env === "prod" && config.mail) {
                notifier.onDeleteLayer(req.body.layer_id, req.body.title);
            }
            res.json({success: true});
        }
    );
});

router.get("/getlayerlinks", function (req, res) {
    database.getLayerLinks(req.query.layer_id).then(
        function (resultObject) {
            res.json(resultObject);
        }
    );
});

router.post("/getservicemetadata", function (req, res) {
    csw_caller.getServiceMetadata(req.body.title, req.body.source_csw, res);
});

router.post("/getdatasetmetadata", function (req, res) {
    csw_caller.getDatasetMetadata(req.body.uuid, req.body.source_csw, res);
});

router.post("/setlayerdatasetmetadata", function (req, res) {
    database.getConfigMetadataCatalogsName(req.body.source_csw).then(
        function (resultObject) {
            var source_csw = resultObject[0];

            csw_caller._callCSWDatasetMetadata(source_csw, [{md_uuid: req.body.uuid}], [], 0, req.body.md_seperate_layer, res, req.body.linkseparate_layer_id, req.body.linkseparate_service_id, req.body.linkalllayers);
        }
    );
});

router.get("/getmetadatavalues", function (req, res) {
    database.getMetadataValues(req.query.md_id).then(
        function (resultObject) {
            res.json(resultObject);
        }
    );
});

router.post("/setLayerManualDatasetMetadata", function (req, res) {
    var dataset = JSON.parse(JSON.stringify(req.body)),
        bbox = config.client_config.bbox_getmap[0].bbox[0].split(",");

    if (dataset.md_id.length === 0) {
        dataset.md_id = uuid.v4();
    }

    dataset.rs_id = "";
    dataset.cat_hmbtg = "";
    dataset.bbox = bbox[0] + " " + bbox[1] + "," + bbox[2] + " " + bbox[3];
    dataset.config_metadata_catalog_id = null;
    dataset.srs_metadata = 4326;
    dataset.srs_services = 4326;

    database.upsertDataset(dataset).then(
        function () {
            if (config.log_level === "debug") {
                debug("Manueller Metadatensatz Hinzugefügt/aktualisert: " + dataset.dataset_name);
            }
            if (dataset.linkalllayers) {
                database.getServiceLayerIds(dataset.service_id).then(
                    async function (layer_ids) {
                        for (var i = 0; i < layer_ids.length; i++) {
                            await database.updateLayerLink({md_id: dataset.md_id, layer_id: layer_ids[i].layer_id, service_id: dataset.service_id});
                        }
                        layer_json_gen.prepare(dataset.service_id);
                        res.json({success: true});
                    }
                );
            }
            else {
                database.updateLayerLink({md_id: dataset.md_id, layer_id: dataset.layer_id, service_id: dataset.service_id}).then(
                    function () {
                        layer_json_gen.prepare(dataset.service_id, dataset.layer_id);
                        res.json({success: true});
                    }
                );
            }
        }
    );
});

router.post("/setlayerlinks", function (req, res) {
    var params = req.body.dataset;

    params.srs_metadata = req.body.srs_metadata;
    params.srs_services = config.srs_services;
    params.descr = params.description.replace(new RegExp("'", "g"), "''").replace(new RegExp("\n", "g"), "").replace(new RegExp("\r", "g"), "");

    database.updateChangelog({username: req.body.last_edited_by, service_id: req.body.service_id, change_date: req.body.last_edit_date, change: "Layer mit Metadaten gekoppelt, Layer-ID: " + req.body.layer_id});
    database.setLastEditedBy([req.body.service_id, req.body.last_edited_by, req.body.last_edit_date]);

    database.upsertDataset(params).then(
        function () {
            layer_json_gen.prepare(undefined, undefined, params.md_id);
            if (req.body.linkalllayers) {
                database.getServiceLayerIds(req.body.service_id).then(
                    async function (layer_ids) {
                        for (var i = 0; i < layer_ids.length; i++) {
                            await database.updateLayerLink({md_id: req.body.dataset.md_id, layer_id: layer_ids[i].layer_id, service_id: parseInt(req.body.service_id)});
                        }
                        layer_json_gen.prepare(req.body.service_id);
                        res.json({success: true});
                    }
                );
            }
            else {
                database.updateLayerLink({md_id: req.body.dataset.md_id, layer_id: req.body.layer_id, service_id: parseInt(req.body.service_id)}).then(
                    function () {
                        layer_json_gen.prepare(req.body.service_id, req.body.layer_id);
                        res.json({success: true});
                    }
                );
            }
        }
    );
});

router.get("/setdscstatus", function (req, res) {
    database.getServiceLayerIds(req.query.service_id).then(
        function (layer_ids) {
            if (layer_ids.length === 0) {
                database.setAllLayersLinked(req.query.service_id, false).then(
                    function () {
                        res.json({dsc_complete: false});
                    }
                );
            }
            else {
                database.getNotLinkedLayers(req.query.service_id).then(
                    function (not_linked_layers) {
                        var dsc_complete = false;

                        if (not_linked_layers.length === 0) {
                            dsc_complete = true;
                        }
                        database.setAllLayersLinked(req.query.service_id, dsc_complete).then(
                            function () {
                                res.json({dsc_complete: dsc_complete});
                            }
                        );
                    }
                );
            }
        }
    );
});

router.post("/deletelayerlink", function (req, res) {
    database.deleteLayerLink(req.body.layer_links_id).then(
        function (resultObject) {
            database.updateChangelog({username: req.body.last_edited_by, service_id: req.body.service_id, change_date: req.body.last_edit_date, change: "Metadatenkopplung gelöscht, Layer-ID: " + req.body.layer_id});
            database.setLastEditedBy([req.body.service_id, req.body.last_edited_by, req.body.last_edit_date]);
            layer_json_gen.prepare(req.body.service_id, req.body.layer_id);
            res.json(resultObject);
        }
    );
});

router.get("/getlayerlist", function (req, res) {
    database.getLayerList().then(
        function (resultObject) {
            res.json(resultObject);
        }
    );
});

router.get("/getportallist", function (req, res) {
    database.getPortalList().then(
        function (resultObject) {
            res.json(resultObject);
        }
    );
});

router.get("/getportallayerlist", function (req, res) {
    database.getPortalLayerList(req.query.PortalUrl).then(
        function (resultObject) {
            res.json(resultObject);
        }
    );
});

router.get("/getfreigabenlist", function (req, res) {
    database.getFreigabenList().then(
        function (resultObject) {
            res.json(resultObject);
        }
    );
});

router.post("/downloadgml", function (req, res) {
    downloader.downloadToTmp(req.body.url, req.body.filename, req.body.size, res);
});

router.post("/estimategmlsize", function (req, res) {
    downloader.estimateGmlSize(req.body.selected_ft, req.body.base_url, res);
});

router.post("/createinspirecapabilities", function (req, res) {
    database.updateChangelog({username: req.body.last_edited_by, service_id: req.body.service.service_id, change_date: req.body.last_edit_date, change: "INSPIRE Capabilities erzeugen"});
    database.setLastEditedBy([req.body.service.service_id, req.body.last_edited_by, req.body.last_edit_date]);
    if (req.body.service.software === "deegree") {
        inspire_cap_gen.updateDeegreeCapabilities(req.body.service, res);
    }
    else {
        inspire_cap_gen.createCapabilitiesDoc(req.body.service, res);
    }
});

router.get("/getlinkedlayers", function (req, res) {
    database.getLinkedLayers(req.query.service_id).then(
        function (resultObject) {
            res.json(resultObject);
        }
    );
});

router.post("/deleteservice", function (req, res) {
    database.deleteService(req.body.service_id).then(
        function (resultObject) {
            if (config.fme.geoproxy_conf_update) {
                fme_caller.createGeoproxyConf();
            }
            database.getServiceLayerIdsAll(req.body.service_id).then(
                function (layer_data) {
                    for (var i = 0; i < layer_data.length; i++) {
                        if (env === "prod") {
                            elasticsearch_connector.deleteFromIndex(layer_data[i].layer_id);
                        }
                    }
                }
            );
            database.updateChangelog({username: req.body.last_edited_by, service_id: req.body.service_id, change_date: req.body.last_edit_date, change: "Dienst gelöscht"});
            res.json(resultObject);
        }
    );
});

router.post("/restoreservice", function (req, res) {
    database.rollbackDelete(req.body.service_id).then(
        function (resultObject) {
            if (config.fme.geoproxy_conf_update) {
                fme_caller.createGeoproxyConf();
            }
            layer_json_gen.prepare(req.body.service_id);
            database.updateChangelog({username: req.body.last_edited_by, service_id: req.body.service_id, change_date: req.body.last_edit_date, change: "Gelöschter Dienst wieder herrgestellt"});
            res.json(resultObject);
        }
    );
});

router.post("/deleteservicefinal", function (req, res) {
    database.deleteServiceFinal(req.body.service_id).then(
        function (resultObject) {
            res.json(resultObject);
        }
    );
});

router.get("/generatelayerjson", function (req, res) {
    layer_json_gen.prepare(undefined, undefined, undefined, res);
});

router.get("/updategeoproxyconf", function (req, res) {
    fme_caller.createGeoproxyConf(res);
});

router.get("/updatefreigaben", function (req, res) {
    fme_caller.updateFreigaben(res);
});

router.get("/getservicestats", function (req, res) {
    var scope = req.query.scope;

    if (scope === "prod") {
        database.getServiceStatsProd().then(
            function (resultObject) {
                res.json(resultObject);
            }
        );
    }
    else if (scope === "intern") {
        database.getInternalServiceStats(false, true).then(
            function (resultObject) {
                res.json(resultObject);
            }
        );
    }
    else if (scope === "extern") {
        database.getInternalServiceStats(true, false).then(
            function (resultObject) {
                res.json(resultObject);
            }
        );
    }
    else { // scope = all
        database.getServiceStats().then(
            function (resultObject) {
                res.json(resultObject);
            }
        );
    }
});

router.get("/getlayerstats", function (req, res) {
    var scope = req.query.scope;

    if (scope === "prod") {
        database.getLayerStatsProd().then(
            function (resultObject) {
                res.json(resultObject);
            }
        );
    }
    else if (scope === "intern") {
        database.getInternalLayerStats(false, true).then(
            function (resultObject) {
                res.json(resultObject);
            }
        );
    }
    else if (scope === "extern") {
        database.getInternalLayerStats(true, false).then(
            function (resultObject) {
                res.json(resultObject);
            }
        );
    }
    else { // scope = all
        database.getLayerStats().then(
            function (resultObject) {
                res.json(resultObject);
            }
        );
    }
});

router.get("/getsoftwarestats", function (req, res) {
    var scope = req.query.scope;

    if (scope === "prod") {
        database.getSoftwareStatsProd().then(
            function (resultObject) {
                res.json(resultObject);
            }
        );
    }
    else if (scope === "intern") {
        database.getInternalSoftwareStats(false, true).then(
            function (resultObject) {
                res.json(resultObject);
            }
        );
    }
    else if (scope === "extern") {
        database.getInternalSoftwareStats(true, false).then(
            function (resultObject) {
                res.json(resultObject);
            }
        );
    }
    else { // scope = all
        database.getSoftwareStats().then(
            function (resultObject) {
                res.json(resultObject);
            }
        );
    }
});

router.get("/getadgroups", async function (req, res) {
    var groups = await ad.findGroups(req.query.groupname);

    res.json(groups);
});

router.post("/updateAllowedGroups", function (req, res) {
    database.updateAllowedGroups(req.body.service_id, JSON.stringify(req.body.allowed_groups)).then(
        function (resultObject) {
            if (config.auth_proxy_reload_url && env !== "dev") {
                auth_proxy.reload();
            }

            database.updateChangelog({username: req.body.last_edited_by, service_id: req.body.service_id, change_date: req.body.last_edit_date, change: "AD Gruppen Berechtigung gespeichert"});
            res.json(resultObject);
        }
    );
});

router.post("/startservicequickcheck", function (req, res) {
    service_checker.start(req.body.service_id, res);
});

router.post("/getservicesbykeyword", function (req, res) {
    database.getServiceIdsByKeyword(req.body.text, res).then(
        function (resultObject) {
            res.json(resultObject);
        }
    );
});

router.post("/addSecurityProxyConfiguration", function (request, result) {
    var params = request.body;

    securityProxyDatabase.addSecurityProxyUsers(params).then(function (user_ids) {
        for (let i = 0; i < user_ids.length; i++) {
            params.users[i].user_id = user_ids[i].user_id;
        }
        securityProxyDatabase.addSecurityProxyGroups(params).then(function (group_ids) {
            for (let i = 0; i < group_ids.length; i++) {
                params.users[i].group_id = group_ids[i].group_id;
            }
            securityProxyDatabase.addSecurityProxyRoles(params).then(function (role_ids) {
                for (let i = 0; i < role_ids.length; i++) {
                    params.users[i].role_id = role_ids[i].role_id;
                }
                securityProxyDatabase.assignUsersToGroupRoles(params).then(function () {
                    securityProxyDatabase.addEndpoint(params).then(function (endpoint_result) {
                        params.endpoint_id = endpoint_result[0].endpoint_id;
                        securityProxyDatabase.addEndpointURLs(params).then(function () {
                            if (typeof params.objects === "string") {
                                params.objects = JSON.parse(params.objects);
                            }
                            params.objects.forEach(function (objects) {
                                objects.endpoint_id = params.endpoint_id;
                            });
                            securityProxyDatabase.addEndpointObjects(params).then(function (obj) {
                                var objects = obj;

                                objects.forEach(function (object) {
                                    object.grant_right = params.grant_right;
                                });
                                if (params.grant_right === "WFS-T:*") {
                                    // für jedes wfs-t object auch eine wfs Kopie anlegen, damit beide grant_rights in security proxy config benutzt werden können
                                    var wfsObjects = JSON.parse(JSON.stringify(objects));

                                    wfsObjects.forEach(function (object) {
                                        object.grant_right = "WFS:*";
                                    });
                                    objects = objects.concat(wfsObjects);
                                }
                                objects.role_ids = role_ids;
                                // assign role to objects
                                securityProxyDatabase.assignRolesToObjects(objects).then(function (resultObject) {
                                    result.json(resultObject);
                                }).catch(error => {
                                    debug(error, "Assign Security Proxy Role To Objects");
                                });
                            }).catch(error => {
                                debug(error, "Add Security Proxy Endpoint Objects");
                            });
                        }).catch(error => {
                            debug(error, "Add Security Proxy Endpoint URLs");
                        });
                    }).catch(error => {
                        debug(error, "Add Security Proxy Endpoint");
                    });
                }).catch(error => {
                    debug(error, "Assign Security Proxy Users to Group Roles");
                });
            }).catch(error => {
                debug(error, "Add Security Proxy Roles");
            });
        }).catch(error => {
            debug(error, "Add Security Proxy Groups");
        });
    }).catch(error => {
        debug(error, "Add Security Proxy Users");
    });
});

router.post("/getSecurityProxyConfiguration", function (request, result) {
    var params = request.body;

    securityProxyDatabase.getEndpointId(params).then(function (endpoint) {
        if (endpoint.length > 0) {
            params.endpoint_id = endpoint[0].endpoint_id;
            params.endpoint_url = endpoint[0].endpoint_url;
            securityProxyDatabase.getObjects(params).then(function (objects) {
                if (objects.length > 0) {
                    params.objects = objects;
                    params.object_id = objects[0].object_id;
                    params.grant_right = objects[0].grant_right;
                    objects.forEach(function (object) {
                        // if one of the objects has grant_right "WFS-T:*" always use that for later
                        if (object.grant_right === "WFS-T:*") {
                            params.grant_right = object.grant_right;
                        }
                    });
                    securityProxyDatabase.getUserGroupRole(params).then(function (users) {
                        users.forEach(function (user) {
                            user.username = user.username.replace("[UP-ext]", "");
                        });
                        params.users = users;
                        result.json(params);
                    });
                }
                else { // no WMS layer or WFS feature recorded for the role
                    result.json({status: "Kein WMS Layer bzw. WFS feature für die Rolle im SecureAdmin eingetragen"});
                }
            });
        }
        else { // no SecurityProxy Configuration found for this service
            result.json({status: "noSecurityProxyConfiguration"});
        }
    });
});

router.post("/deleteSecurityProxyConfiguration", function (request, result) {
    var params = request.body;

    securityProxyDatabase.deleteUsers(params).then(function () {
        securityProxyDatabase.deleteEndpointObjects(params).then(function () {
            securityProxyDatabase.deleteEndpoint(params).then(function (resultObject) {
                result.json(resultObject);
            });
        });
    });
});

router.post("/usernameExistsInSecurityProxy", function (request, result) {
    var params = request.body;

    securityProxyDatabase.usernameExists(params).then(
        function (existingUsers) {
            var usernames = [];

            existingUsers.forEach(function (user) {
                usernames.push(user.username.replace("[UP-ext]", ""));
            });
            result.json(usernames);
        }
    );
});

router.post("/updateEndpoint", function (request, result) {
    var params = request.body;

    securityProxyDatabase.updateEndpoint(params).then(
        function (resultObject) {
            result.json(resultObject);
        }
    );
});

router.post("/setdeletelayerrequest", function (req, res) {
    database.setDeleteLayerRequest(req.body.layer_id, req.body.service_id, req.body.comment).then(
        function () {
            if (config.mail) {
                notifier.onDeleteLayerRequest(req.body.layer_id, req.body.title, req.body.comment, req.body.linked_portals);
            }
            database.updateChangelog({username: req.body.last_edited_by, service_id: req.body.service_id, change_date: req.body.last_edit_date, change: "Löschvermerk eingetragen"});
            res.json({success: true});
        }
    );
});

router.post("/removedeletelayerrequest", function (req, res) {
    database.removeDeleteLayerRequest(req.body.layer_id).then(
        function () {
            database.updateChangelog({username: req.body.last_edited_by, service_id: req.body.service_id, change_date: req.body.last_edit_date, change: "Löschvermerk entfernt"});
            res.json({success: true});
        }
    );
});

router.post("/searchJiraIssues", function (req, res) {
    jira.searchJiraIssues(res);
});

router.get("/getLinkedTickets", function (req, res) {
    database.getLinkedTickets(req.query).then(results => {
        res.json(results);
    });
});

router.get("/getServiceIssues", function (req, res) {
    database.getServiceIssues(req.query).then(results => {
        res.json(results);
    });
});

router.post("/addServiceIssue", function (req, res) {
    database.addServiceIssue(req.body).then(result => {
        res.json(result);
    });
});

router.post("/deleteServiceIssue", function (req, res) {
    database.deleteServiceIssue(req.body).then(result => {
        res.json(result);
    });
});

router.get("/getscheduledtasks", function (req, res) {
    database.getScheduledTasks(req.body).then(result => {
        res.json(result);
    });
});

router.get("/getconfigdminstances", function (req, res) {
    database.getConfigDMInstances().then(
        function (resultObject) {
            resultObject.forEach(element => {
                if (element.pw) {
                    if (element.pw.indexOf("__") > 0) {
                        element.pw = cryptor.decrypt(element.pw);
                    }
                }
            });

            res.json(resultObject);
        }
    ).catch(error => {
        debug("getconfigdminstances", error);
    });
});

router.post("/updateconfigdminstance", function (req, res) {
    req.body.pw = cryptor.encrypt(req.body.pw);

    database.updateConfigDMInstance(req.body).then(
        function (resultObject) {
            res.json(resultObject);
        }
    ).catch(error => {
        debug("updateconfigdminstance", error);
    });
});

router.post("/deleteconfigdminstance", function (req, res) {
    database.deleteConfigDMInstance(req.body.config_dm_instance_id).then(
        function (resultObject) {
            res.json(resultObject);
        }
    ).catch(error => {
        debug("deleteconfigdminstance", error);
    });
});

router.post("/addconfigdminstance", function (req, res) {
    req.body.pw = cryptor.encrypt(req.body.pw);

    database.addConfigDMInstance(req.body).then(resultObject => {
        res.json(resultObject);
    }).catch(error => {
        debug("addconfigdminstance", error);
    });
});

router.post("/resetexternaldminstanceconnection", function (req, res) {
    var reset = service_importer.resetConnection(req.body.host);

    res.json(reset);
});

router.post("/searchServicesInExternalDMInstance", function (req, res) {
    service_importer.searchService(req.body).then(resultObject => {
        res.json(resultObject);
    }).catch(error => {
        debug("searchServicesInExternalDMInstance", error);
    });
});

router.post("/importservice", function (req, res) {
    service_importer.getServiceData(req.body.service_id).then(async function (service) {
        if (!service.error) {
            var layer_ids = [],
                layer_count = 0;

            if (service.layers.length > 0) {
                for (var i = 0; i < service.layers.length; i++) {
                    layer_ids.push(service.layers[i].layer_id);
                }

                var resp = await database.checkIfLayerIdExists(layer_ids.toString());

                layer_count = parseInt(resp[0].count);
            }

            service.last_edit_date = req.body.last_edit_date;
            service.last_edited_by = req.body.last_edited_by;
            if (req.body.createNewIds || layer_count > 0) {
                database.importServiceWithNewId(service).then(data => {
                    if (config.auth_proxy_reload_url && env !== "dev") {
                        auth_proxy.reload();
                    }
                    if (env === "prod" && config.fme.geoproxy_conf_update) {
                        fme_caller.createGeoproxyConf();
                    }
                    if (service.layers.length > 0) {
                        layer_json_gen.prepare(data.service_id);
                    }
                    database.updateChangelog({username: req.body.last_edited_by, service_id: data.service_id, change_date: req.body.last_edit_date, change: "Dienst Importiert"});
                    res.json(data);
                }).catch(error => {
                    debug("importServiceWithNewId", error);
                    res.json({error: true});
                });
            }
            else {
                database.importService(service).then(data => {
                    if (config.auth_proxy_reload_url && env !== "dev") {
                        auth_proxy.reload();
                    }
                    if (env === "prod" && config.fme.geoproxy_conf_update) {
                        fme_caller.createGeoproxyConf();
                    }
                    if (service.layers.length > 0) {
                        layer_json_gen.prepare(data.service_id);
                    }
                    database.updateChangelog({username: req.body.last_edited_by, service_id: data.service_id, change_date: req.body.last_edit_date, change: "Dienst Importiert"});
                    database.setLayerIdSeq();
                    database.setServiceIdSeq();
                    res.json(data);
                }).catch(error => {
                    debug("importservice", error);
                    res.json({error: true});
                });
            }
        }
        else {
            res.json({error: true});
        }
    }).catch(error => {
        debug("getServiceData", error);
        res.json({error: true});
    });
});

router.post("/updatewithimportedservice", function (req, res) {
    database.deleteServiceFinal(req.body.service_id).then(function () {
        service_importer.getServiceData(req.body.service_id).then(service => {
            service.last_edit_date = req.body.last_edit_date;
            service.last_edited_by = req.body.last_edited_by;
            database.importService(service).then(resultObject => {
                res.json(resultObject);
            }).catch(error => {
                debug("updatewithimportedservice", error);
                res.json({error: true});
            });
        }).catch(error => {
            debug("getServiceData", error);
            res.json({error: true});
        });
    }).catch(error => {
        debug("deleteServiceFinal", error);
        res.json({error: true});
    });
});

router.post("/startscheduledtask", function (req, res) {
    var scheduled_task = require("./scheduled_tasks/" + req.body.task_name);

    database.updateScheduledTaskStatus([req.body.task_name, "", "", "in Ausführung"]).then(function () {
        scheduled_task.execute();
    });

    res.json({status: "Running"});
});

router.post("/updatescheduledtasks", function (req, res) {
    database.updateScheduledTasks(req.body.tasks).then(
        function () {
            scheduled_tasks_manager.reschedule(req.body.tasks);
            res.json({success: true});
        }
    );
});

router.post("/cancelscheduledtask", function (req, res) {
    database.updateScheduledTaskStatus([req.body.task_name, "", "", "Zurückgesetzt"]).then(function () {
        res.json({success: true});
    });
});

router.get("/updatelayerjson", function (req, res) {
    if (req.query.layerid) {
        layer_json_gen.prepare(req.query.serviceid, req.query.layerid, undefined, res);
    }
    else {
        layer_json_gen.prepare(req.query.serviceid, undefined, undefined, res);
    }
});

router.post("/createjiraticket", function (req, res) {
    jira.createJiraTicket(req.body, res);
});

module.exports = router;
