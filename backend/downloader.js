var fs = require("fs"),
    path = require("path"),
    http = require("http"),
    request = require("request"),
    replaceStream = require("replacestream"),
    archiver = require("archiver"),
    config = require("./../config.js"),
    axios = require("axios"),
    https = require("https"),
    debug = require("debug")("DM:downloader"),
    xmldom = require("xmldom"),
    DOMParser = xmldom.DOMParser,
    xpath = require("xpath"),
    downloader;

downloader = {
    downloadToTmp: function (url, filename, size, res) {
        var tmpDirectory = "tmp",
            gml_file = fs.createWriteStream(path.join(__dirname, "../" + tmpDirectory + "/" + filename + ".gml")),
            xsd_file = fs.createWriteStream(path.join(__dirname, "../" + tmpDirectory + "/" + filename + ".xsd"));

        request(url).pipe(replaceStream(/\b[A-Za-z0-9\.,_\&;:\/\?%\+\-=\(\)]+DescribeFeatureType[A-Za-z0-9\.,_\&;:\/\?%\+\-=\(\)]+/i, filename + ".xsd")).pipe(gml_file);

        gml_file.on("finish", function () {
            http.get(url.replace("GetFeature", "DescribeFeatureType"), function (xsd_response) {
                xsd_response.pipe(xsd_file);
                xsd_file.on("finish", function () {
                    if (parseInt(size) < 150) {
                        var output = fs.createWriteStream(path.join(__dirname, "../" + tmpDirectory + "/" + filename + ".zip")),
                            archive = archiver("zip", {
                                zlib: {level: 9}
                            });

                        output.on("finish", function () {
                            return res.json({link_zip: tmpDirectory + "/" + filename + ".zip"});
                        });

                        archive.pipe(output);

                        archive.file(path.join(__dirname, "../" + tmpDirectory + "/" + filename + ".gml"), {name: filename + ".gml"});
                        archive.file(path.join(__dirname, "../" + tmpDirectory + "/" + filename + ".xsd"), {name: filename + ".xsd"});

                        archive.finalize();
                    }
                    else {
                        return res.json({link_gml: tmpDirectory + "/" + filename + ".gml", link_xsd: "tmp/" + filename + ".xsd"});
                    }
                });
            });
        });
    },

    estimateGmlSize: function (selected_ft, base_url, res) {
        this._downloadFt(selected_ft, base_url, 0, 0, res);
    },

    _downloadFt: function (selected_ft, base_url, estimated_size, index, res) {
        var me = this;

        if (index < selected_ft.length) {
            axios({
                method: "get",
                url: base_url + "&resultType=hits&typename=" + selected_ft[index],
                httpsAgent: new https.Agent({
                    rejectUnauthorized: config.reject_unauthorized_SSL === null || config.reject_unauthorized_SSL === undefined ? false : config.reject_unauthorized_SSL
                })
            }).then(function (get_count_response) {
                var gml_doc = new DOMParser().parseFromString(get_count_response.data, "text/xml"),
                    numberOfFeatures = 0,
                    gf_url = base_url + "&typename=" + selected_ft[index];

                if (base_url.toLowerCase().indexOf("version=1.1.0") > -1) {
                    var select_wfs_11 = xpath.useNamespaces({"wfs": "http://www.opengis.net/wfs"});

                    numberOfFeatures = select_wfs_11("string(//wfs:FeatureCollection/@numberOfFeatures)", gml_doc);
                    gf_url += "&maxFeatures=1";
                }
                else if (base_url.toLowerCase().indexOf("version=2.0.0") > -1) {
                    var select_wfs_20 = xpath.useNamespaces({"wfs": "http://www.opengis.net/wfs/2.0"});

                    numberOfFeatures = select_wfs_20("string(//wfs:FeatureCollection/@numberMatched)", gml_doc);
                    gf_url += "&count=1";
                }

                axios({
                    method: "get",
                    url: gf_url,
                    httpsAgent: new https.Agent({
                        rejectUnauthorized: config.reject_unauthorized_SSL === null || config.reject_unauthorized_SSL === undefined ? false : config.reject_unauthorized_SSL
                    })
                }).then(function (get_size_response) {
                    var ft_size = ((get_size_response.data.length - 660) * parseInt(numberOfFeatures)) / 1000000,
                        new_size = estimated_size + ft_size;

                    me._downloadFt(selected_ft, base_url, new_size, index + 1, res);
                }).catch(function (error) {
                    debug("Downloader Estimate Size get size", error);
                });
            }).catch(function (error) {
                debug("Downloader Estimate Size get count", error);
                return res.json({error: true});
            });
        }
        else {
            return res.json({estimated_size: Math.ceil(estimated_size)});
        }
    }
};

module.exports = downloader;
