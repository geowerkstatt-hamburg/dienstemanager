const config = require("./../config.js"),
    axios = require("axios"),
    https_agent = require("./https_agent"),
    database = require("./database"),
    moment = require("moment"),
    _ = require("underscore"),
    mailer = require("./mailer"),
    debug = require("debug")("DM:service_checker"),
    metadata_parser = require("./metadata_parser"),
    capabilities_parser = require("./capabilities_parser");

module.exports = {
    /**
     * Start test for single service
     * @param {Integer} service_id - service_id of the service to be tested
     * @param {Object} res - response Object from backend call
     * @returns {void}
     */
    start: function (service_id, res) {
        this._prepareData(service_id, res);
    },

    /**
     * Start test for given array of services through scheduled task
     * @param {Array} services_to_check - array of services to be checked
     * @param {Integer} check_index - up counted index for service array through recursive call
     * @param {Object} check_results - object with test overview
     * @returns {void}
     */
    start_rec: function (services_to_check, check_index, check_results) {
        if (check_index < services_to_check.length) {
            if (config.log_level === "debug") {
                debug("Dienst testen: " + services_to_check[check_index].service_id);
            }
            this._prepareData(services_to_check[check_index].service_id, null, services_to_check, check_index, check_results);
        }
        else {
            var text = "Zusammenfassung der letzten Prüfung: Für " + check_results.num_cancelled + " Dienste konnte die Prüfung nicht durchgeführt werden. " + check_results.num_check + " Dienste ohne Fehler, " + check_results.num_warn + " Dienste mit Warnungen, " + check_results.num_error + " Dienste mit Fehlern.";

            if (config.mail) {
                var subject = "Dienstemanager: Prüfbericht";

                console.log(moment().format("YYYY-MM-DD HH:mm:ss") + " - Prüfung der Dienste abgeschlossen. " + check_results.num_cancelled + " Dienste konnte die Prüfung nicht durchgeführt werden. " + check_results.num_check + " Dienste ohne Fehler, " + check_results.num_warn + " Dienste mit Warnungen, " + check_results.num_error + " Dienste mit Fehlern.");
                mailer.sendMail(config.mail.service_checker_to, subject, text);
            }
            database.updateScheduledTaskStatus(["check_all_services", moment().format("YYYY-MM-DD HH:mm:ss"), text, "Erfolgreich"]);
        }
    },

    /**
     * before running actual tests, all required data will be selected
     * @param {Integer} service_id - service ID of service to be tested
     * @param {Object} res - response Object from backend call
     * @param {Array} services_to_check - array of services to be checked (needed for recursive call)
     * @param {Integer} check_index - up counted index for service array through recursive call
     * @param {Object} check_results - object with test overview
     * @returns {void}
     */
    _prepareData: async function (service_id, res, services_to_check, check_index, check_results) {
        var service_data_arr = await database.getJsonDataService(service_id),
            service_data = service_data_arr[0],
            metadata_catalogs_data,
            params_jira = {service_id: service_id},
            linked_datasets;

        service_data.jira_data = await database.getServiceIssues(params_jira);
        service_data.layer_data = await database.getJsonDataServiceLayer(service_id);

        if (service_data.service_md_id) {
            metadata_catalogs_data = await database.getConfigMetadataCatalogsId(service_data.config_metadata_catalog_id);
            linked_datasets = await database.getLinkedDatasets(service_id);
            this._getCapabilities(service_data, metadata_catalogs_data, linked_datasets, res, services_to_check, check_index, check_results);
        }
        else {
            this._getCapabilities(service_data, null, null, res, services_to_check, check_index, check_results);
        }
    },

    /**
     * run getCapabilities Request for service to be tested
     * @param {Object} service_data - service attributes
     * @param {Object} metadata_catalog_data - metadata catalog attributes
     * @param {Array} linked_datasets - coupled dataset attributes
     * @param {Object} res - response Object from backend call
     * @param {Array} services_to_check - array of services to be checked (needed for recursive call)
     * @param {Integer} check_index - up counted index for service array through recursive call
     * @param {Object} check_results - object with test overview
     * @returns {void}
     */
    _getCapabilities: function (service_data, metadata_catalog_data, linked_datasets, res, services_to_check, check_index, check_results) {
        var me = this,
            proxy_settings = service_data.external ? config.proxy : false,
            https_url = service_data.url_int.indexOf("https:") > -1,
            httpsAgent = https_url ? https_agent.getHttpsAgent(proxy_settings) : null;

        axios({
            method: "get",
            url: service_data.url_int + "?REQUEST=GetCapabilities&SERVICE=" + service_data.service_type,
            proxy: config.use_https_tunnel_proxy && https_url ? false : proxy_settings,
            httpsAgent: httpsAgent
        }).then(function (capabilities) {
            if (capabilities.data.indexOf("WFS_Capabilities") > 0 || capabilities.data.indexOf("WMS_Capabilities") > 0 || capabilities.data.indexOf("WMT_MS_Capabilities") > 0) {
                var capabilities_data = null;

                if (service_data.service_type === "WMS") {
                    capabilities_data = capabilities_parser.parseWMSCapabilities(capabilities.data, false, service_data.version);
                }
                else if (service_data.service_type === "WFS") {
                    capabilities_data = capabilities_parser.parseWFSCapabilities(capabilities.data, false, service_data.version);
                }

                if (!service_data.service_md_id) {
                    me._check_service(service_data, capabilities_data, {error: "no metadata"}, linked_datasets, res, services_to_check, check_index, check_results);
                }
                else {
                    me._getMetadata(service_data, metadata_catalog_data, linked_datasets, capabilities_data, res, services_to_check, check_index, check_results);
                }
            }
            else {
                me._check_service(service_data, {error: "not found"}, null, linked_datasets, res, services_to_check, check_index, check_results);
            }
        }).catch(function (error) {
            console.log(error);
            me._check_service(service_data, {error: "not available"}, null, linked_datasets, res, services_to_check, check_index, check_results);
        });
    },

    /**
     * call GetRecordByID Request on metadata cataloque csw to get service metadata
     * @param {Object} service_data - service attributes
     * @param {Object} metadata_catalog_data - metadata catalog attributes
     * @param {Array} linked_datasets - coupled dataset attributes
     * @param {Object} capabilities_data - service capabilities attributes
     * @param {Object} res - response Object from backend call
     * @param {Array} services_to_check - array of services to be checked (needed for recursive call)
     * @param {Integer} check_index - up counted index for service array through recursive call
     * @param {Object} check_results - object with test overview
     * @returns {void}
     */
    _getMetadata: function (service_data, metadata_catalog_data, linked_datasets, capabilities_data, res, services_to_check, check_index, check_results) {
        var me = this,
            source_csw = metadata_catalog_data[0],
            proxy_settings = source_csw.proxy ? config.proxy : false,
            https_url = source_csw.csw_url.indexOf("https:") > -1,
            httpsAgent = https_url ? https_agent.getHttpsAgent(proxy_settings) : null;

        axios({
            method: "get",
            url: source_csw.csw_url + "?REQUEST=GetRecordById&SERVICE=CSW&VERSION=2.0.2&outputSchema=http://www.isotc211.org/2005/gmd&id=" + service_data.service_md_id,
            proxy: config.use_https_tunnel_proxy && https_url ? false : proxy_settings,
            httpsAgent: httpsAgent
        }).then(function (metadata_response) {
            if (metadata_response.data.indexOf("gmd:MD_Metadata") > 0) {
                var metadata_data = metadata_parser.parseServiceMD(metadata_response);

                metadata_data.dataset_uuids = metadata_parser.getCoupledRessources(metadata_response);

                me._check_service(service_data, capabilities_data, metadata_data, linked_datasets, res, services_to_check, check_index, check_results);
            }
            else if (metadata_response.data.indexOf("ExceptionReport") > 0) {
                me._check_service(service_data, capabilities_data, {error: "CSW error"}, linked_datasets, res, services_to_check, check_index, check_results);
            }
            else {
                me._check_service(service_data, capabilities_data, {error: "not found"}, linked_datasets, res, services_to_check, check_index, check_results);
            }
        }).catch(function (error) {
            console.log(error);
            me._check_service(service_data, capabilities_data, {error: "not available"}, linked_datasets, res, services_to_check, check_index, check_results);
        });
    },

    /**
     * call GetRecordByID Request on metadata cataloque csw to get service metadata
     * @param {Object} service_data - service attributes
     * @param {Object} capabilities_data - service capabilities attributes
     * @param {Object} metadata_data - service metadata attributes
     * @param {Array} linked_datasets - coupled dataset attributes
     * @param {Object} res - response Object from backend call
     * @param {Array} services_to_check - array of services to be checked (needed for recursive call)
     * @param {Ineteger} check_index - up counted index for service array through recursive call
     * @param {Object} check_results - object with test overview
     * @returns {void}
     */
    _check_service: function (service_data, capabilities_data, metadata_data, linked_datasets, res, services_to_check, check_index, check_results) {
        var me = this,
            layers_check = false,
            ddk_check = false,
            ddk_checked = 0,
            layer_checked = [],
            title_check = false,
            fees_check = false,
            accessconstraints_check = false,
            inspire_check = false,
            epsg_check = false,
            service_url_check = false,
            epsg_codes = JSON.parse(JSON.stringify(config.mandatory_epsg_codes)),
            error_messages = [];

        if (capabilities_data.error === "not found") {
            const error_message = {type: "error", topic: "capabilities not found", message: "Die Dienst-URL antwortet, aber der Dienst ist nicht abrufbar!"};

            if (config.log_level === "debug") {
                debug("cancelled: " + service_data.service_id + ", " + error_message.message);
            }
            this._check_cancelled(error_message, res, services_to_check, check_index, check_results);
        }
        else if (capabilities_data.error === "not available") {
            const error_message = {type: "error", topic: "capabilities not available", message: "Der Dienst antwortet nicht!"};

            debug("cancelled: " + service_data.service_id + ", " + error_message.message);
            this._check_cancelled(error_message, res, services_to_check, check_index, check_results);
        }
        else {
            if (metadata_data.error === "not found") {
                const error_message = {type: "error", topic: "metadata not found", message: "Dienstmetadaten nicht im Metadatenkatalog zu finden!"};

                if (config.log_level === "debug") {
                    debug("cancelled: " + service_data.service_id + ", " + error_message.message);
                }
                this._check_cancelled(error_message, res, services_to_check, check_index, check_results);
            }
            else if (metadata_data.error === "not available") {
                const error_message = {type: "error", topic: "metadata not available", message: "Metadatenkatalog nicht erreichbar!"};

                if (config.log_level === "debug") {
                    debug("cancelled: " + service_data.service_id + ", " + error_message.message);
                }
                this._check_cancelled(error_message, res, services_to_check, check_index, check_results);
            }
            else if (metadata_data.error === "CSW error") {
                const error_message = {type: "error", topic: "CSW not available", message: "CSW liefert keine Ergebnisse!"};

                if (config.log_level === "debug") {
                    debug("cancelled: " + service_data.service_id + ", " + error_message.message);
                }
                this._check_cancelled(error_message, res, services_to_check, check_index, check_results);
            }
            else {
                var data_layer_length = _.where(capabilities_data.layer_list, {group_layer: false}).length,
                    all_layer_length = capabilities_data.layer_list.length,
                    capabilities_layer_names = _.pluck(capabilities_data.layer_list, "layer_name"),
                    capabilities_layer_names_uniq = _.uniq(capabilities_layer_names);

                // checks if layers in capabilties and dienstemanager DB are consistent
                for (let j = 0; j < all_layer_length; j++) {
                    var is_layer = false;

                    for (let i = 0; i < service_data.layer_data.length; i++) {
                        if (service_data.layer_data[i].layer_name === capabilities_data.layer_list[j].layer_name) {
                            is_layer = true;
                        }
                    }
                    if (is_layer) {
                        layer_checked.push(true);
                    }
                }

                if (((service_data.layer_data.length === all_layer_length || service_data.layer_data.length === data_layer_length) && service_data.layer_data.length === layer_checked.length) && capabilities_layer_names_uniq.length === capabilities_layer_names.length) {
                    layers_check = true;
                }
                else {
                    if (capabilities_layer_names_uniq.length < capabilities_layer_names.length) {
                        error_messages.push({type: "error", topic: "layers", message: "Es existieren mehrere Layer mit identischen Layer-Namen!"});
                    }
                    if ((service_data.layer_data.length !== all_layer_length && service_data.layer_data.length !== data_layer_length) || service_data.layer_data.length !== layer_checked.length) {
                        error_messages.push({type: "error", topic: "layers", message: "Die Layer in der Dienstemanager-Datenbank und den Capabilities stimmen nicht überein!"});
                    }
                }

                // metadata related checks
                if (service_data.service_md_id) {
                    for (let j = 0; j < capabilities_data.layer_list.length; j++) {
                        if (!capabilities_data.layer_list[j].group_layer) {
                            var found = _.where(linked_datasets, {layer_name: capabilities_data.layer_list[j].layer_name, md_id: capabilities_data.layer_list[j].metadata_uuid});

                            if (found.length > 0) {
                                ddk_checked++;
                            }
                        }
                    }

                    if (ddk_checked === data_layer_length) {
                        ddk_check = true;
                    }
                    else {
                        error_messages.push({type: "error", topic: "ddk", message: "Die Daten-Dienste-Kopplung in den Capabilities ist fehlerhaft!"});
                    }
                    if (service_data.title === capabilities_data.title && service_data.title === metadata_data.title) {
                        title_check = true;
                    }
                    else {
                        error_messages.push({type: "warning", topic: "title", message: "Die Dienst-Titel in den Metadaten (" + metadata_data.title + ") , den Capabilities (" + capabilities_data.title + ") und dem Dienstemanager (" + service_data.title + ") sind nicht identisch!"});
                    }

                    if (service_data.fees.trim() === capabilities_data.fees.trim() && service_data.fees.trim() === metadata_data.fees.trim()) {
                        fees_check = true;
                    }
                    else {
                        error_messages.push({type: "warning", topic: "fees", message: "Die Angaben zu Nutzungsbedingungen (Fees) in den Dienst-Metadaten, den Capabilities und der Dienstemanager-Datenbank sind nicht identisch!"});
                    }

                    if (service_data.access_constraints.trim() === capabilities_data.access_constraints.trim() && service_data.access_constraints.trim() === metadata_data.access_constraints.trim()) {
                        accessconstraints_check = true;
                    }
                    else {
                        error_messages.push({type: "warning", topic: "accessconstraints", message: "Die Angaben zu Zugriffsbeschränkungen (Access Constraints) in den Dienst-Metadaten, den Capabilities und der Dienstemanager-Datenbank sind nicht identisch!"});
                    }

                    inspire_check = capabilities_data.inspire_ext_cap;

                    if (!inspire_check) {
                        error_messages.push({type: "warning", topic: "inspire", message: "Der INSPIRE extended Capabilities Block fehlt!"});
                    }

                    if (service_data.url_ext === metadata_data.service_url) {
                        service_url_check = true;
                    }
                    else if (!config.verbose_url_check && service_data.url_ext.replace("qs-", "").replace("test-", "") === metadata_data.service_url.replace("qs-", "").replace("test-", "")) {
                        service_url_check = true;
                    }
                    else {
                        error_messages.push({type: "warning", topic: "service url", message: "Die Dienst-URL im Metadatenkatalog (" + metadata_data.service_url + ") und Dienstemanager (" + service_data.url_ext + ") sind nicht identisch!"});
                    }
                }

                // data layer related checks
                if (data_layer_length > 0) {
                    if (service_data.service_type === "WMS") {
                        epsg_codes.push("CRS:84");
                    }

                    var epsg_diff = _.difference(epsg_codes, capabilities_data.epsg_list);

                    if (epsg_diff.length === 0) {
                        epsg_check = true;
                    }
                    else {
                        error_messages.push({type: "warning", topic: "epsg", message: "Folgende geforderten EPSG Codes werden nicht von diesem Dienst unterstützt: " + epsg_diff.sort().toString()});
                    }
                }

                // check if fields for servive dataset update info are filled
                if (!service_data.update_type || !service_data.update_process_type || !service_data.update_process_name) {
                    error_messages.push({type: "warning", topic: "service_update_info", message: "Die Felder unter \"Datenaktualität\" sollten gefüllt sein!"});
                }

                // check if JIRA Tickets are linked
                if (config.client_config.jira_modul && service_data.jira_data.length === 0) {
                    error_messages.push({type: "warning", topic: "service_linked_jira_tickets", message: "Es gibt keine verknüpften JIRA Tickets!"});
                }

                // check if fees are filled
                if (!metadata_data.error && !service_data.fees) {
                    error_messages.push({type: "warning", topic: "service_no_fees_access_constraints", message: "Achtung, es sind keine Nutzungsbedingungen eingetragen!"});
                }

                database.deleteAllServiceTestResults(service_data.service_id).then(
                    function () {
                        if (layers_check && ddk_check && title_check && fees_check && accessconstraints_check && inspire_check && epsg_check && service_url_check) {
                            database.setCheckedStatus(service_data.service_id, "Geprüft");
                            if (res) {
                                res.json({status: "Geprüft"});
                            }
                            else {
                                check_results.num_check = check_results.num_check + 1;
                                var check_index_new = check_index + 1;

                                me.start_rec(services_to_check, check_index_new, check_results);
                            }
                        }
                        else {
                            me._addServiceTestResults(service_data.service_id, error_messages, 0, res, services_to_check, check_index, check_results);
                        }
                    }
                );
            }
        }
    },

    /**
     * recursifly save service test resuluts to DB
     * @param {Integer} service_id - service ID of tested service
     * @param {Array} error_messages - test results
     * @param {Integer} index - index of test results array position
     * @param {Object} res - response object from backend cal
     * @param {Array} services_to_check - array with services to test from scheduled task call
     * @param {Integer} check_index - index of tested service in services_to_check array
     * @param {Object} check_results - object with test overview
     * @returns {void}
     */
    _addServiceTestResults: function (service_id, error_messages, index, res, services_to_check, check_index, check_results) {
        var me = this,
            status = "Hinweis",
            errors = _.where(error_messages, {type: "error"});

        if (errors.length > 0) {
            status = "Fehler";
        }

        if (index < error_messages.length) {
            database.addServiceTestResult({service_id: service_id, text: error_messages[index].message, status: status}).then(
                function () {
                    var index_new = index + 1;

                    me._addServiceTestResults(service_id, error_messages, index_new, res, services_to_check, check_index, check_results);
                }
            );
        }
        else {
            if (res) {
                res.json({status: status, error_messages: error_messages});
            }
            else {
                if (status === "Fehler") {
                    check_results.num_error = check_results.num_error + 1;
                }
                if (status === "Hinweis") {
                    check_results.num_warn = check_results.num_warn + 1;
                }
                var check_index_new = check_index + 1;

                me.start_rec(services_to_check, check_index_new, check_results);
            }
        }
    },

    /**
     * send check canceld message / start test for next service
     * @param {Object} error_message - test results
     * @param {Object} res - response object from backend cal
     * @param {Array} services_to_check - array with services to test from scheduled task call
     * @param {Integer} check_index - index of tested service in services_to_check array
     * @param {Object} check_results - object with test overview
     * @returns {void}
     */
    _check_cancelled: function (error_message, res, services_to_check, check_index, check_results) {
        if (res) {
            res.json({status: "canceled", error_message: error_message});
        }
        else {
            check_results.num_cancelled = check_results.num_cancelled + 1;
            var check_index_new = check_index + 1;

            this.start_rec(services_to_check, check_index_new, check_results);
        }
    }
};
