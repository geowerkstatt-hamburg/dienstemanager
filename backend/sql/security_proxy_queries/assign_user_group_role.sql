BEGIN TRANSACTION;
INSERT INTO deeuser.saii_user_group (
    user_id,
    group_id
)
VALUES (
    ${user_id},
    ${group_id}
);
INSERT INTO deeuser.saii_group_role (
    group_id,
    role_id
)
VALUES (
    ${group_id},
    ${role_id}
);
END TRANSACTION;