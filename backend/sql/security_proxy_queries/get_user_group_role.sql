SELECT DISTINCT user_id, group_id, role_id, username, password, firstname, lastname, email, g.name AS group_name, r.name AS role_name
FROM deeuser.saii_role_object ro
JOIN deeuser.saii_group_role gr USING (role_id)
JOIN deeuser.saii_user_group ug USING (group_id)
JOIN deeuser.saii_user u USING (user_id)
JOIN deeuser.saii_group g USING (group_id)
JOIN deeuser.saii_role r USING (role_id)
WHERE ro.object_id = ${object_id}