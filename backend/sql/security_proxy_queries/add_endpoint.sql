INSERT INTO deeuser.saii_endpoint (
    endpoint_name, 
    proxy_url, 
    endpoint_id
)
VALUES (
    ${endpoint_name}, 
    ${proxy_url}, 
    nextval('deeuser.saii_endpoint_seq')
)
RETURNING deeuser.saii_endpoint.endpoint_id;