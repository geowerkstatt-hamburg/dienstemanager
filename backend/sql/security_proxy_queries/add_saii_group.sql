INSERT INTO deeuser.saii_group (
    group_id,
    name
)
VALUES (
    nextval('deeuser.saii_group_seq'),
    ${group_name}
) 
RETURNING group_id;