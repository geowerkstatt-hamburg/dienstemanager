INSERT INTO deeuser.saii_role (
    role_id,
    name,
    sort_order
)
VALUES (
    nextval('deeuser.saii_role_seq'),
    ${role_name},
    0
) 
RETURNING role_id;