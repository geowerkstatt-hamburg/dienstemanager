UPDATE datasets SET
    dataset_name=${dataset_name},
    rs_id=${rs_id},
    bbox=(SELECT replace(substring(ST_EXTENT(ST_Transform(ST_GeomFromText('MULTIPOINT(${bbox})',${srs_metadatenkatalog}),${srs_dienste}))::text FROM 5 for char_length(ST_EXTENT(ST_Transform(ST_GeomFromText('MULTIPOINT(${bbox})',4326),25832))::text)-5),' ', ',')),
    cat_opendata=${cat_opendata},
    cat_inspire=${cat_inspire},
    cat_hmbtg=${cat_hmbtg},
    cat_org=${cat_org},
    fees=${fees},
    access_constraints=${access_constraints}
WHERE md_id_operates_on = ${md_id}
