UPDATE layers_type_3d SET
    request_vertex_normals = ${request_vertex_normals},
    maximum_screen_space_error = ${maximum_screen_space_error}
WHERE layer_id = ${layer_id};