UPDATE layers_type_sensorthings SET
    epsg = ${epsg},
    rootel = ${rootel},
    filter = ${filter},
    expand = ${expand},
    related_wms_layers = ${related_wms_layers},
    style_id = ${style_id},
    cluster_distance = ${cluster_distance},
    load_things_only_in_current_extent = ${load_things_only_in_current_extent},
    mouse_hover_field = ${mouse_hover_field},
    sta_test_url = ${sta_test_url}
WHERE layer_id = ${layer_id};
