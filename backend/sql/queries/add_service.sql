INSERT INTO services
(
  service_checked_status, 
  service_name,
  title, 
  description, 
  fees, 
  access_constraints, 
  editor, 
  responsible_party,
  status,
  service_type, 
  software, 
  server, 
  folder,
  url_ext, 
  url_int, 
  url_sec, 
  security_type, 
  only_intranet, 
  only_internet,
  datasource, 
  mapping_description, 
  cached, 
  service_md_id, 
  config_metadata_catalog_id,
  inspire_relevant, 
  last_edited_by, 
  last_edit_date, 
  version, 
  qa,
  external
)
VALUES (
  ${service_checked_status}, 
  ${service_name},
  ${title}, 
  ${description}, 
  ${fees}, 
  ${access_constraints}, 
  ${editor},
  ${responsible_party},
  ${status}, 
  ${service_type}, 
  ${software}, 
  ${server}, 
  ${folder},
  ${url_ext}, 
  ${url_int}, 
  ${url_sec}, 
  ${security_type}, 
  ${only_intranet}, 
  ${only_internet},
  ${datasource}, 
  ${mapping_description}, 
  ${cached},
  ${service_md_id}, 
  ${config_metadata_catalog_id},
  ${inspire_relevant}, 
  ${last_edited_by}, 
  ${last_edit_date}, 
  ${version}, 
  ${qa},
  ${external}
) 
RETURNING service_id
