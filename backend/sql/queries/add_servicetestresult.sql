INSERT INTO service_test_results(service_id, text)
	VALUES (${service_id}, ${text});
DELETE FROM service_test_results WHERE service_test_results.service_test_results_id NOT IN
(SELECT s.service_test_results_id FROM (
    SELECT DISTINCT ON (service_id, text) service_test_results_id
  FROM service_test_results) AS s);
UPDATE services SET service_checked_status = ${status} WHERE service_id = ${service_id};
DELETE FROM service_test_results WHERE service_id::text || text IN (SELECT service_id::text || text FROM service_test_results_checked);
UPDATE services set service_checked_status='Geprüft' WHERE (service_checked_status='Fehler' OR service_checked_status='Hinweis') AND service_id NOT IN (SELECT service_id FROM service_test_results);
