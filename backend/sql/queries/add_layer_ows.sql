WITH new_layer AS (
    INSERT INTO layers (
        layer_name,
        service_id,
        title,
        scale_min,
        scale_max,
        output_format
    )
    VALUES (
        ${layer_name},
        ${service_id}::integer,
        ${title},
        ${scale_min},
        ${scale_max},
        ${output_format}
    )
    RETURNING layers.layer_id
)
INSERT INTO layers_type_ows (
    layer_id,
    namespace
)
VALUES (
    (SELECT new_layer.layer_id FROM new_layer),
    ${namespace}
)
RETURNING layer_id;
