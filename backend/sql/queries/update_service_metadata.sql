UPDATE services SET
    title=${title},
    description=${description},
    fees=${fees},
    access_constraints=${access_constraints},
    inspire_relevant=${inspire}
WHERE service_id = ${service_id}
