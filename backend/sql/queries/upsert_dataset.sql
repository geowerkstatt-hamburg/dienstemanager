SELECT upsert_dataset(
    ${dataset_name},
    ${md_id},
    ${rs_id},
    ${cat_opendata},
    ${cat_inspire},
    ${cat_hmbtg},
    ${cat_org},
    ${bbox},
    ${fees},
    ${access_constraints},
    ${config_metadata_catalog_id},
    ${descr},
    ${keywords},
    ${srs_metadata},
    ${srs_services}
);