UPDATE layers SET
    layer_name=${layer_name},
    title=${title},
    scale_min=${scale_min},
    scale_max=${scale_max}
WHERE layer_id = ${layer_id}
RETURNING layer_id;
