BEGIN TRANSACTION;

UPDATE services SET deleted = false
WHERE service_id = $1;

UPDATE layers SET deleted = false
WHERE service_id = $1;

END TRANSACTION;