BEGIN TRANSACTION;

UPDATE services SET deleted = true WHERE service_id = $1;

UPDATE layers SET deleted = true WHERE service_id = $1;

DELETE FROM layer_json WHERE service_id=$1;

DELETE FROM layer_delete_requests WHERE service_id=$1;

END TRANSACTION;
