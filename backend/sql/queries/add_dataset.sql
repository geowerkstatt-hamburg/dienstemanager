INSERT INTO datasets (
    dataset_name,
    md_id,
    rs_id,
    cat_opendata,
    cat_inspire,
    cat_hmbtg,
    cat_org,
    fees,
    access_constraints,
    config_metadata_catalog_id,
    bbox) SELECT ${dataset_name},${md_id},${rs_id},${cat_opendata},${cat_inspire},${cat_hmbtg},${cat_org},${fees},${access_constraints},${config_metadata_catalog_id},
    (SELECT replace(substring(ST_EXTENT(ST_Transform(ST_GeomFromText('MULTIPOINT(${bbox})',${srs_metadatenkatalog}),${srs_dienste}))::text FROM 5 for char_length(ST_EXTENT(ST_Transform(ST_GeomFromText('MULTIPOINT(${bbox})',4326),25832))::text)-5),' ', ',')) WHERE NOT EXISTS (SELECT md_id FROM datasets WHERE md_id = ${md_id})
