var config = require("./../config.js"),
    mailer = require("./mailer"),
    database = require("./database"),
    debug = require("debug")("DM:notifier"),
    notifier,
    env = process.env.NODE_ENV ? process.env.NODE_ENV.trim() : "dev";

notifier = {
    onDeleteLayer: function (layer_id, title) {
        var subject = "Dienstemanager: Layer (" + layer_id + ") gelöscht",
            text = "Der produktive Layer \"" + title + "\" mit der ID " + layer_id + " wurde gelöscht.";

        mailer.sendMail(config.mail.delete_layer_to, subject, text);
    },

    onDeleteLayerRequest: function (layer_id, title, comment, linked_portals) {
        var subject = "Dienstemanager: Layer (" + layer_id + ") zum Löschen markiert",
            text = "Der Layer \"" + title + "\" mit der ID " + layer_id + " wurde zum Löschen markiert.<br/>",
            linked_portals_str = "";

        for (let i = 0; i < linked_portals.length; i++) {
            linked_portals_str = linked_portals_str + "\"" + linked_portals[i].title + "\", ";
        }

        text += "<br/>Folgende Portale sind betroffen:<br/>";
        text += linked_portals_str.substring(0, linked_portals_str.length - 2);

        if (comment.length > 0) {
            text += "Kommentar:<br/>";
            text += comment;
        }

        if (env === "prod") {
            mailer.sendMail(config.mail.delete_layer_to, subject, text);
        }
        else {
            debug("In Prod env Mail versendet:");
            debug(config.mail.delete_layer_to);
            debug(subject);
            debug(text);
        }
    },

    onNoMoreLinkedPortals: function (layer_id) {
        var subject = "Dienstemanager: zum löschen vorgemerkter Layer (" + layer_id + ") aus allen Portalen entfernt",
            text = "Der Layer mit der ID " + layer_id + " wurde aus allen Portalen entfernt und kann nun gelöscht werden.";

        if (env === "prod") {
            mailer.sendMail(config.mail.deletable_layer_to, subject, text);
        }
        else {
            debug("In Prod env Mail versendet:");
            debug(config.mail.deletable_layer_to);
            debug(subject);
            debug(text);
        }
    },

    onProdService: function (service_id, title, service_type) {
        var subject = "Neuer produktiver Dienst: " + title,
            text = "Der Dienst \"" + title + "\" ist jetzt produktiv verfügbar.";

        if (service_type === "WMS") {
            database.getServiceLayers(service_id).then(
                function (resultObject) {
                    if (resultObject.length > 0 && config.preview_portal_url) {
                        var layerList = "";

                        resultObject.each(function (record) {
                            layerList += record.layer_id + ",";
                        });
                        var previewURL = "<a href=\"" + config.preview_portal_url + "?Map/layerids=" + layerList.substring(0, layerList.length - 1) + "\">Im Vorschauportal betrachten</a>";

                        text += " " + previewURL;

                        mailer.sendMail(config.mail.prod_to, subject, text);
                    }
                    else {
                        mailer.sendMail(config.mail.prod_to, subject, text);
                    }
                }
            );
        }
        else {
            mailer.sendMail(config.mail.prod_to, subject, text);
        }
    },

    onQAService: function (title, user) {
        var subject = "[QS] bitte Dienst prüfen: " + title,
            text = "Die Bearbeitung des Dienstes " + title + " wurde durch " + user + " abgeschlossen.";

        mailer.sendMail(config.mail.qa_to, subject, text);
    }
};

module.exports = notifier;
