var config = require("../config.js"),
    _ = require("underscore"),
    debug = require("debug")("DM:ad_async_caller"),
    ActiveDirectory = require("activedirectory"),
    ad = new ActiveDirectory(config.ldap);

/**
 * Promise wrapper for activedirectory module functions
 */
module.exports = {
    /**
     * search AD for User by sAMAccountName. Used for Authorization.
     * @param {String} accountname - sAMAccountName
     * @returns {JSON} user details
     */
    findUser: (accountname) => new Promise((resolve, reject) => {
        ad.findUser(accountname, function (err, user) {
            if (err) {
                reject(err);
            }

            if (!user) {
                if (config.log_level === "debug") {
                    debug("Benutzer " + accountname + " nicht gefunden.");
                }
                reject("Benutzer " + accountname + " nicht gefunden.");
            }
            else {
                resolve(user);
            }
        });
    }),

    /**
     * search AD for User by surname/givenname
     * @param {String} surname - surname, must be supplied
     * @param {String} givenname - givenname or "*" Wildcard
     * @returns {Array} alle found users
     */
    findUsers: (surname, givenname) => new Promise((resolve, reject) => {
        var query = "CN=" + surname + " " + givenname;

        ad.findUsers(query, function (err, users) {
            if (err) {
                reject(err);
            }

            if (!users) {
                if (config.log_level === "debug") {
                    debug("Benutzer " + givenname + " " + surname + " nicht gefunden.");
                }
                reject("Benutzer " + givenname + " " + surname + " nicht gefunden.");
            }
            else {
                resolve(users);
            }
        });
    }),

    /**
     * Find all groups supplied User is member of
     * @param {String} accountname - sAMAccountName
     * @returns {Array} all found groups
     */
    getGroupMembershipForUser: (accountname) => new Promise((resolve, reject) => {
        ad.getGroupMembershipForUser(accountname, function (err, groupsfound) {
            if (err) {
                reject(err);
            }

            if (!groupsfound) {
                if (config.log_level === "debug") {
                    debug("Keine Gruppen für " + accountname + " gefunden.");
                }
                reject("Keine Gruppen für " + accountname + " gefunden.");
            }
            else {
                var grouplist = _.map(groupsfound, function (ug) {
                    return {name: ug.cn};
                });

                resolve(grouplist);
            }
        });
    }),

    /**
     * Find all groups, where supplied Name ist part of groupname.
     * @param {String} groupname - search phrase
     * @returns {Array} all found groups
     */
    findGroups: (groupname) => new Promise((resolve, reject) => {
        var query = "CN=*" + groupname + "*";

        ad.findGroups(query, function (err, groups) {
            if (err) {
                if (config.log_level === "debug") {
                    debug("Find Groups" + JSON.stringify(err));
                }
                reject(err);
            }
            else {
                var groups_list = [];

                for (var i = 0; i < groups.length; i++) {
                    groups_list.push({name: groups[i].sAMAccountName});
                }

                resolve(groups_list);
            }
        });
    }),

    /**
     * get all users for given group name
     * @param {*} groupname - search phrase
     * @returns {Aray} all found users
     */
    findUsersByGroup: (groupname) => new Promise((resolve, reject) => {
        ad.getUsersForGroup(groupname, function (err, users) {
            if (err) {
                if (config.log_level === "debug") {
                    debug("Find Users by Group" + JSON.stringify(err));
                }
                reject(err);
            }

            if (!users) {
                reject("Keine User für " + groupname + " gefunden.");
            }
            else {
                resolve(users);
            }
        });
    }),

    /**
     * get all sub-groups for given group name
     * @param {*} groupname - search phrase
     * @returns {Aray} all found groups
     */
    getGroupMembershipForGroup: (groupname) => new Promise((resolve, reject) => {
        ad.getGroupMembershipForGroup(groupname, function (err, groups) {
            if (err) {
                if (config.log_level === "debug") {
                    debug("Find Groups by Group" + JSON.stringify(err));
                }
                reject(err);
            }

            if (!groups) {
                reject("Keine Gruppen für " + groupname + " gefunden.");
            }
            else {
                resolve(groups);
            }
        });
    })
};
