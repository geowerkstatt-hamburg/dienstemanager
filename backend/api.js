var database = require("./database"),
    Router = require("express-promise-router"),
    router = new Router();

router.get("/", function (req, res) {
    var layers = req.query.layers,
        scope = req.query.scope,
        style = req.query.style,
        filter = req.query.filter;

    res.set("Content-Type", "application/json; charset=UTF-8");

    if ((layers === "all" || layers === "prod") && (scope === "internet" || scope === "intranet" || scope === "all")) {
        var prod_query = false;

        if (layers === "prod") {
            prod_query = true;
        }
        database.getLayerJson(scope, filter, prod_query).then(
            function (resultObject) {
                var res_array = [];

                if (resultObject[0].json_array !== null && resultObject[0].json_array !== undefined) {
                    res_array = resultObject[0].json_array;
                }
                if (style === "pretty") {
                    res.send(JSON.stringify(res_array, null, 2));
                }
                else {
                    res.json(res_array);
                }
            }
        );
    }
    else if (layers.match("^[0-9,]+$") && (scope === "internet" || scope === "intranet" || scope === "all")) {
        database.getListLayerJson(layers, scope).then(
            function (resultObject) {
                var res_array = [];

                if (resultObject[0].json_array !== null && resultObject[0].json_array !== undefined) {
                    res_array = resultObject[0].json_array;
                }
                if (style === "pretty") {
                    res.send(JSON.stringify(res_array, null, 2));
                }
                else {
                    res.json(res_array);
                }
            }
        );
    }
    else {
        res.json({status: "error", message: "Parameter 'layers' [all, prod, list of layer IDs] and 'scope' [internet,intranet,all] must be set."});
    }
});

module.exports = router;
