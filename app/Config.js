Ext.define("DiensteManager.Config", {
    singleton: true,

    config: {
        version: null,
        endpointer: null,
        mapPreview: null,
        mapPreviewBaseLayerId: null,
        mapPreviewComplexAppend: null,
        srsDienste: null,
        esriUrlNamespace: null,
        geoportalUrl: null,
        geoportalUrlExt: null,
        bearbeiter: null,
        responsibleParty: null,
        bboxGetmap_name: null,
        bboxGetmap_extent: null,
        dienstTypen: null,
        dienstVersionen: null,
        software: null,
        server: null,
        applyDpiFactor: false,
        useWebsockets: false,
        externeUrlPrefix: null,
        securityType: null,
        securityProxyUrlSso: null,
        securityProxyUrlAuth: null,
        fmeJsonModul: false,
        freigabenModul: false,
        metadatenModul: false,
        gekoppelteportaleModul: false,
        proxyupdateModul: false,
        deegreeModul: false,
        anmerkungenModul: false,
        verwaltungModul: false,
        zugriffsstatistikModul: false,
        ldapModul: false,
        securityModul: false,
        jiraModul: false,
        jiraBrowseUrl: null,
        jiraProjects: null,
        jiraLabels: null
    },

    constructor: function (config) {
        this.initConfig(config);
    },

    bboxFilterListeGenerieren: function () {
        var items = [{
            text: "Alle",
            type: "typ_alle",
            itemId: "typ_alle",
            checked: true
        }];

        for (var i = 0; i < this.getBboxGetmap_name().length; ++i) {

            items.push({
                text: this.getBboxGetmap_name()[i],
                type: "typ_" + this.getBboxGetmap_name()[i].toLowerCase(),
                itemId: "typ_" + this.getBboxGetmap_name()[i].toLowerCase()
            });
        }
        return items;
    },

    dienstTypFilterListeGenerieren: function () {
        var items = [{
            text: "Alle",
            type: "typ_alle",
            itemId: "typ_alle",
            checked: true
        }];

        for (var i = 0; i < this.getDienstTypen().length; ++i) {
            items.push({
                text: this.getDienstTypen()[i],
                type: "typ_" + this.getDienstTypen()[i].toLowerCase(),
                itemId: "typ_" + this.getDienstTypen()[i].toLowerCase()
            });
        }
        return items;
    },

    softwareFilterListeGenerieren: function () {
        var items = [{
            text: "Alle",
            type: "software_alle",
            itemId: "software_alle",
            checked: true
        }];

        for (var i = 0; i < this.getSoftware().length; ++i) {
            items.push({
                text: this.getSoftware()[i],
                type: "software_" + this.getSoftware()[i].toLowerCase(),
                itemId: "software_" + this.getSoftware()[i].toLowerCase()
            });
        }

        return items;
    },

    responsiblePartyFilterListeGenerieren: function () {
        var items = [{
            text: "Alle",
            type: "responsible_party_alle",
            itemId: "responsible_party_alle",
            checked: true
        }];

        for (var i = 0; i < this.getResponsibleParty().length; i++) {
            items.push({
                text: this.getResponsibleParty()[i],
                type: "responsible_party_" + this.getResponsibleParty()[i].toLowerCase().replace(/\s/g, "_"),
                itemId: "responsible_party_" + this.getResponsibleParty()[i].toLowerCase().replace(/\s/g, "_")
            });
        }

        return items;
    },

    getServerListe: function () {
        var items = [];

        for (var i = 0; i < this.getServer().length; ++i) {
            items.push(this.getServer()[i].name);
        }
        return items;
    },

    getJiraProjectNames: function () {
        var items = [];

        for (var i = 0; i < this.getJiraProjects().length; ++i) {
            items.push(this.getJiraProjects()[i].name);
        }
        return items;
    }
});
