Ext.define("DiensteManager.store.PortalList", {
    extend: "Ext.data.Store",
    alias: "store.portallist",
    storeId: "PortalList",
    model: "DiensteManager.model.PortalList",
    sorters: [{
        property: "title",
        direction: "ASC"
    }],
    proxy: {
        type: "ajax",
        actionMethods: {
            create: "GET",
            read: "GET",
            update: "GET",
            destroy: "GET"
        },
        url: "backend/getportallist",
        method: "GET",
        reader: {type: "json", rootProperty: "results", totalProperty: "total"}
    }
});
