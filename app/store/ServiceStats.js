Ext.define("DiensteManager.store.ServiceStats", {
    extend: "Ext.data.Store",
    alias: "store.servicestats",
    storeId: "ServiceStats",
    model: "DiensteManager.model.Statistic",
    proxy: {
        type: "ajax",
        actionMethods: {
            create: "GET",
            read: "GET",
            update: "GET",
            destroy: "GET"
        },
        url: "backend/getservicestats",
        method: "GET",
        reader: {type: "json", rootProperty: "results", totalProperty: "total"}
    }
});
