Ext.define("DiensteManager.store.NavigationTree", {
    extend: "Ext.data.TreeStore",

    storeId: "NavigationTree",
    root: {
        expanded: true,
        children: [
            {
                text: "Dienste",
                iconCls: "right-icon x-fa fa-list",
                expanded: true,
                selectable: false,
                children: [
                    {
                        text: "aktiv",
                        view: "services.ServiceList",
                        leaf: true,
                        routeId: "service_list"
                    },

                    {
                        text: "gelöscht",
                        view: "services.DeletedServiceList",
                        leaf: true,
                        routeId: "deleted_service_list"
                    }
                ]
            },
            {
                text: "Layer",
                view: "layer.LayerList",
                leaf: true,
                iconCls: "right-icon x-fa fa-list-ol",
                routeId: "layer_list"

            },
            {
                text: "Portale",
                view: "portals.PortalList",
                leaf: true,
                iconCls: "right-icon pictos pictos-browser",
                routeId: "portal_list"

            },
            {
                text: "Datensätze",
                view: "datasets.DatasetList",
                iconCls: "right-icon x-fa fa-list-alt",
                leaf: true,
                routeId: "datasets_list"
            },
            {
                text: "Freigaben",
                view: "freigaben.FreigabenList",
                iconCls: "right-icon pictos pictos-news",
                leaf: true,
                routeId: "freigaben_list"
            },
            // {
            //     text: "Dienste testen",
            //     view: "tests.Tests",
            //     iconCls: "right-icon pictos pictos-speedometer",
            //     leaf: true,
            //     routeId: "tests"
            // },
            {
                text: "Statistiken",
                view: "statistics.Statistics",
                iconCls: "right-icon pictos pictos-chart2",
                leaf: true,
                routeId: "statistics"
            },
            {
                text: "Kontakte",
                view: "contacts.ContactList",
                iconCls: "right-icon pictos pictos-user",
                leaf: true,
                routeId: "contacts"
            },
            {
                text: "Verwaltung",
                view: "management.Management",
                iconCls: "right-icon x-fa fa-cog",
                leaf: true,
                routeId: "management"
            }
        ]
    },
    fields: [
        {
            name: "text"
        }
    ]
});
