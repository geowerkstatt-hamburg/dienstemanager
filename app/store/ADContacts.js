Ext.define("DiensteManager.store.ADContacts", {
    extend: "Ext.data.Store",
    alias: "store.adcontacts",
    storeId: "ADContacts",
    model: "DiensteManager.model.ADContact",
    proxy: {
        type: "ajax",
        actionMethods: {
            create: "GET",
            read: "GET",
            update: "GET",
            destroy: "GET"
        },
        url: "backend/getadcontacts",
        method: "GET",
        reader: {type: "json", rootProperty: "results", totalProperty: "total"}
    }
});
