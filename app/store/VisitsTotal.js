Ext.define("DiensteManager.store.VisitsTotal", {
    extend: "Ext.data.Store",
    alias: "store.visitstotal",
    storeId: "VisitsTotal",
    model: "DiensteManager.model.Visits",
    sorters: [{
        property: "month",
        direction: "ASC"
    }, {
        property: "visits_total",
        direction: "ASC"
    }],
    proxy: {
        type: "ajax",
        actionMethods: {
            create: "GET",
            read: "GET",
            update: "GET",
            destroy: "GET"
        },
        url: "backend/getvisitstotal",
        method: "GET",
        reader: {type: "json", rootProperty: "results", totalProperty: "total"}
    }
});
