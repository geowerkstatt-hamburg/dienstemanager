Ext.define("DiensteManager.store.ServiceTestResultsChecked", {
    extend: "Ext.data.Store",
    alias: "store.servicetestresultschecked",
    storeId: "ServiceTestResultsChecked",
    model: "DiensteManager.model.ServiceTestResult",
    proxy: {
        type: "ajax",
        useDefaultXhrHeader: false,
        actionMethods: {
            create: "GET",
            read: "GET",
            update: "GET",
            destroy: "GET"
        },
        url: "backend/getservicetestresultschecked",
        method: "GET",
        reader: {type: "json", rootProperty: "results", totalProperty: "total"}
    }
});
