Ext.define("DiensteManager.store.Portals", {
    extend: "Ext.data.Store",
    alias: "store.portals",
    storeId: "Portals",
    model: "DiensteManager.model.Portal",
    sorters: [{
        property: "title",
        direction: "ASC"
    }],
    proxy: {
        type: "ajax",
        actionMethods: {
            create: "GET",
            read: "GET",
            update: "GET",
            destroy: "GET"
        },
        url: "backend/getportallinks",
        method: "GET",
        reader: {type: "json", rootProperty: "results", totalProperty: "total"}
    }
});
