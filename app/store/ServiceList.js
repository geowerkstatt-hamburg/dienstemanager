Ext.define("DiensteManager.store.ServiceList", {
    extend: "Ext.data.Store",
    alias: "store.servicelist",
    storeId: "ServiceList",
    model: "DiensteManager.model.Service",
    autoLoad: true,
    sorters: [{
        property: "last_edit_date",
        direction: "DESC"
    }],
    proxy: {
        type: "ajax",
        actionMethods: {
            create: "POST",
            read: "GET",
            update: "POST",
            destroy: "POST"
        },
        url: "backend/getservicelist",
        method: "GET",
        reader: {
            type: "json",
            rootProperty: "results"
        }
    }
});
