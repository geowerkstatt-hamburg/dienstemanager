Ext.define("DiensteManager.store.VisitsTop", {
    extend: "Ext.data.Store",
    alias: "store.visitstop",
    storeId: "VisitsTop",
    model: "DiensteManager.model.Visits",
    sorters: [{
        property: "month",
        direction: "ASC"
    }, {
        property: "visits_total",
        direction: "ASC"
    }],
    proxy: {
        type: "ajax",
        actionMethods: {
            create: "GET",
            read: "GET",
            update: "GET",
            destroy: "GET"
        },
        url: "backend/getvisitstop",
        method: "GET",
        reader: {type: "json", rootProperty: "results", totalProperty: "total"}
    }
});
