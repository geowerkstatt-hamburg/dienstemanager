Ext.define("DiensteManager.store.JiraTickets", {
    extend: "Ext.data.Store",
    alias: "store.jiraTickets",
    storeId: "JiraTickets",
    model: "DiensteManager.model.JiraTicket",
    proxy: {
        type: "ajax",
        useDefaultXhrHeader: false,
        actionMethods: {
            create: "GET",
            read: "GET",
            update: "GET",
            destroy: "GET"
        },
        url: "backend/getServiceIssues",
        method: "GET",
        reader: {type: "json", rootProperty: "results", totalProperty: "total"}
    }
});
