Ext.define("DiensteManager.store.LinkedContacts", {
    extend: "Ext.data.Store",
    alias: "store.linkedcontacts",
    storeId: "LinkedContacts",
    model: "DiensteManager.model.Contact",
    proxy: {
        type: "ajax",
        actionMethods: {
            create: "GET",
            read: "GET",
            update: "GET",
            destroy: "GET"
        },
        url: "backend/getlinkedcontacts",
        method: "GET",
        reader: {type: "json", rootProperty: "results", totalProperty: "total"}
    }
});
