Ext.define("DiensteManager.store.DeletedServiceList", {
    extend: "Ext.data.Store",
    alias: "store.deletedservicelist",
    storeId: "DeletedServiceList",
    model: "DiensteManager.model.Service",
    sorters: [{
        property: "title",
        direction: "ASC"
    }],
    proxy: {
        type: "ajax",
        actionMethods: {
            create: "GET",
            read: "GET",
            update: "GET",
            destroy: "GET"
        },
        url: "backend/getdeletedservicelist",
        method: "GET",
        reader: {type: "json", rootProperty: "results", totalProperty: "total"}
    }
});
