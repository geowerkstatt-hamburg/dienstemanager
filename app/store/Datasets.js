Ext.define("DiensteManager.store.Datasets", {
    extend: "Ext.data.Store",
    alias: "store.datasets",
    storeId: "Datasets",
    model: "DiensteManager.model.Dataset",
    proxy: {
        type: "memory",
        reader: {
            type: "json",
            rootProperty: "datasets"
        }
    }
});
