Ext.define("DiensteManager.store.LayerList", {
    extend: "Ext.data.Store",
    alias: "store.layerlist",
    storeId: "LayerList",
    model: "DiensteManager.model.LayerList",
    sorters: [{
        property: "layer_id",
        direction: "ASC"
    }],
    proxy: {
        type: "ajax",
        actionMethods: {
            create: "GET",
            read: "GET",
            update: "GET",
            destroy: "GET"
        },
        url: "backend/getlayerlist",
        method: "GET",
        reader: {type: "json", rootProperty: "results", totalProperty: "total"}
    }
});
