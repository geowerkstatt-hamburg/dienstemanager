Ext.define("DiensteManager.store.SecurityProxyConfiguration", {
    extend: "Ext.data.Store",
    alias: "store.SecurityProxyConfiguration",
    storeId: "SecurityProxyConfiguration",
    model: "SecurityProxyConfigurationModel",
    autoLoad: false
});
