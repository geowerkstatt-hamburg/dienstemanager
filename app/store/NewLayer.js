Ext.define("DiensteManager.store.NewLayer", {
    extend: "Ext.data.Store",
    alias: "store.newlayer",
    storeId: "NewLayer",
    model: "DiensteManager.model.NewLayer",
    sorters: [{
        property: "name",
        direction: "ASC"
    }],
    proxy: {
        type: "memory",
        reader: {
            type: "json",
            rootProperty: "layers"
        }
    }
});
