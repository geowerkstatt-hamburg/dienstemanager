Ext.define("DiensteManager.store.ConfigMetadataCatalogs", {
    extend: "Ext.data.Store",
    alias: "store.configmetadatacatalogs",
    storeId: "ConfigMetadataCatalogs",
    model: "DiensteManager.model.ConfigMetadataCatalog",
    autoLoad: true,
    proxy: {
        type: "ajax",
        actionMethods: {
            create: "GET",
            read: "GET",
            update: "GET",
            destroy: "GET"
        },
        url: "backend/getconfigmetadatacatalogs",
        method: "GET",
        reader: {
            type: "json",
            rootProperty: "results"
        }
    }
});
