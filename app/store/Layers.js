Ext.define("DiensteManager.store.Layers", {
    extend: "Ext.data.Store",
    alias: "store.layers",
    storeId: "Layers",
    model: "DiensteManager.model.Layer",
    sorters: [{
        property: "layer_name",
        direction: "ASC"
    }],
    proxy: {
        type: "ajax",
        actionMethods: {
            create: "GET",
            read: "GET",
            update: "GET",
            destroy: "GET"
        },
        url: "backend/getservicelayers",
        method: "GET",
        reader: {type: "json", rootProperty: "results", totalProperty: "total"}
    }
});
