Ext.define("DiensteManager.model.ServiceTestResult", {
    extend: "Ext.data.Model",
    fields: [
        {name: "service_test_results_id", type: "integer"},
        {name: "service_id", type: "integer"},
        {name: "text", type: "string"}
    ]
});
