Ext.define("DiensteManager.model.Layer", {
    extend: "Ext.data.Model",
    fields: [
        {name: "layer_id", type: "int"},
        {name: "service_id", type: "int"},
        {name: "layer_name", type: "string"},
        {name: "title", type: "string"},
        {name: "titel_alt", type: "string"},
        {name: "layerattribution", type: "string"},
        {name: "additional_categories", type: "string"},
        {name: "gutter", type: "integer"},
        {name: "tilesize", type: "integer"},
        {name: "legend_url_intranet", type: "string"},
        {name: "legend_url_internet", type: "string"},
        {name: "singletile", type: "boolean"},
        {name: "transparent", type: "boolean"},
        {name: "transparency", type: "integer"},
        {name: "namespace", type: "string"},
        {name: "datetime_column", type: "string"},
        {name: "output_format", type: "string"},
        {name: "scale_min", type: "string"},
        {name: "scale_max", type: "string"},
        {name: "legend_type", type: "string"},
        {name: "legend_path", type: "string"},
        {name: "featurecount", type: "integer"},
        {name: "gfi_format", type: "string"},
        {name: "gfi_theme", type: "string"},
        {name: "gfi_config", type: "string"},
        {name: "gfi_complex", type: "boolean"},
        {name: "gfi_asnewwindow", type: "boolean"},
        {name: "gfi_windowspecs", type: "string"},
        {name: "request_vertex_normals", type: "boolean"},
        {name: "maximum_screen_space_error", type: "integer"},
        {name: "hidelevels", type: "integer"},
        {name: "minzoom", type: "integer"},
        {name: "resolution", type: "integer"},
        {name: "terrainurl", type: "string"},
        {name: "projection", type: "string"},
        {name: "hittolerance", type: "integer"},
        {name: "layer_delete_request", type: "boolean"},
        {name: "layer_delete_request_comment", type: "string"},
        {name: "service_url_is_visible", type: "boolean"},
        {name: "epsg", type: "string"},
        {name: "rootel", type: "string"},
        {name: "expand", type: "string"},
        {name: "filter", type: "string"},
        {name: "related_wms_layers", type: "string"},
        {name: "style_id", type: "string"},
        {name: "cluster_distance", type: "integer"},
        {name: "load_things_only_in_current_extent", type: "boolean"},
        {name: "mouse_hover_field", type: "string"},
        {name: "sta_test_url", type: "string"},
        {name: "extent", type: "string"},
        {name: "origin", type: "string"},
        {name: "resolutions", type: "string"},
        {name: "visibility", type: "boolean"},
        {name: "vtstyles", type: "string"},
        {name: "gfi_beautifykeys", type: "boolean"},
        {name: "time_series", type: "boolean"}
    ]
});
