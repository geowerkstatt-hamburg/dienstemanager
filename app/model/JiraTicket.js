Ext.define("DiensteManager.model.JiraTicket", {
    extend: "Ext.data.Model",
    fields: [
        {name: "id", type: "integer"},
        {name: "service_id", type: "integer"},
        {name: "key", type: "string"},
        {name: "creator", type: "string"},
        {name: "assignee", type: "string"},
        {name: "project", type: "string"},
        {name: "summary", type: "string"}
    ]
});
