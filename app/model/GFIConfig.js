Ext.define("DiensteManager.model.GFIConfig", {
    extend: "Ext.data.Model",
    fields: [
        {name: "attr_name", type: "string"},
        {name: "map_name", type: "string"}
    ]
});
