Ext.define("DiensteManager.model.PortalList", {
    extend: "Ext.data.Model",
    fields: [
        {name: "title", type: "string"},
        {name: "url", type: "string"}
    ]
});
