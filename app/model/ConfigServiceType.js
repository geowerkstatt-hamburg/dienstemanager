Ext.define("DiensteManager.model.ConfigServiceType", {
    extend: "Ext.data.Model",
    fields: [
        {name: "name", type: "string"},
        {name: "versions", type: "auto"}
    ],
    belongsTo: "Config"
});
