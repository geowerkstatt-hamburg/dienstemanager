Ext.define("DiensteManager.model.ServiceMDSet", {
    extend: "Ext.data.Model",
    fields: [
        {name: "title", type: "string"},
        {name: "description", type: "string"},
        {name: "md_uuid", type: "string"},
        {name: "inspire", type: "boolean"},
        {name: "fees", type: "string"},
        {name: "access_constraints", type: "string"},
        {name: "keywords", type: "auto"}
    ]
});
