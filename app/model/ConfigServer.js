Ext.define("DiensteManager.model.ConfigServer", {
    extend: "Ext.data.Model",
    fields: [
        {name: "name", type: "string"},
        {name: "domain", type: "string"},
        {name: "lb", type: "boolean"},
        {name: "server", type: "auto"}
    ],
    belongsTo: "Config"
});
