Ext.define("DiensteManager.model.Contact", {
    extend: "Ext.data.Model",
    fields: [
        {name: "contact_id", type: "integer"},
        {name: "name", type: "string"},
        {name: "given_name", type: "string"},
        {name: "surname", type: "string"},
        {name: "company", type: "string"},
        {name: "ad_account", type: "string"},
        {name: "email", type: "string"},
        {name: "tel", type: "string"}
    ]
});
