Ext.define("DiensteManager.model.Portal", {
    extend: "Ext.data.Model",
    fields: [
        {name: "layer_id", type: "string"},
        {name: "title", type: "string"},
        {name: "url", type: "string"}
    ]
});
