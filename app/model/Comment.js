Ext.define("DiensteManager.model.Comment", {
    extend: "Ext.data.Model",
    fields: [
        {name: "comment_id", type: "integer"},
        {name: "service_id", type: "integer"},
        {name: "text", type: "string"},
        {name: "author", type: "string"},
        {name: "post_date", type: "string"}
    ]
});
