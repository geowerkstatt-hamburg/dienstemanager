Ext.define("DiensteManager.model.ConfigDMInstance", {
    extend: "Ext.data.Model",
    fields: [
        {name: "config_dm_instance_id", type: "integer"},
        {name: "name", type: "string"},
        {name: "host", type: "string"},
        {name: "port", type: "integer"},
        {name: "database", type: "string"},
        {name: "db_user", type: "string"},
        {name: "pw", type: "string"}
    ],
    belongsTo: "Config"
});
